!
! Copyright (C) 2001-2014 Quantum-ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!

!-----------------------------------------------------------------------
SUBROUTINE update_hamiltonian(istep)
  !-----------------------------------------------------------------------
  !
  ! ... Update the hamiltonian
  !
  USE kinds,         ONLY : dp
  USE ldaU,          ONLY : lda_plus_U
  USE scf,           ONLY : rho, rho_core, rhog_core, vltot, v, kedtau, vrs
  USE fft_base,      ONLY : dfftp
  USE gvecs,         ONLY : doublegrid
  USE io_global,     ONLY : stdout
  USE lsda_mod,      ONLY : nspin
  USE uspp,          ONLY : okvan, nkb
  USE dfunct,        ONLY : newd
  USE tddft_module,  ONLY : nupdate_Dnm, iverbosity
  USE becmod,        ONLY : becp, allocate_bec_type, deallocate_bec_type
  USE wvfct,         ONLY : nbnd
  USE ldaU,          ONLY : eth
  USE extfield,      ONLY : etotefield
  USE ener,          ONLY : etot, eband, etxc, ehart, etxcc, vtxc, ewld  
  USE cell_base,     ONLY : at, bg, alat, omega, tpiba2
  USE ions_base,     ONLY : zv, nat, nsp, ityp, tau
  USE gvect,         ONLY : ngm, gstart, nl, nlm, g, gg, gcutm
  USE control_flags, ONLY : gamma_only
  USE vlocal,        ONLY : strf
  implicit none
  integer, intent(in) :: istep
  real(dp) :: charge
  real(dp), external :: ewald

  call start_clock('updateH')
  
  ! calculate total charge density
  call deallocate_bec_type(becp)
  call sum_band()
  call allocate_bec_type(nkb, nbnd, becp)

  if (lda_plus_U) then
    call new_ns
    if (iverbosity > 10) call write_ns()
  end if
    
  ! calculate HXC-potential
  call v_of_rho( rho, rho_core, rhog_core, ehart, etxc, vtxc, eth, etotefield, charge, v )
    
  ! calculate total local potential (external + scf)
  call set_vrs(vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid)    
 
  ! calculate eband and etot
  call calculate_eband     
  ewld = ewald( alat, nat, nsp, ityp, zv, at, bg, tau, &
                omega, g, gg, ngm, gcutm, gstart, gamma_only, strf )
  etot = eband + ( etxc - etxcc ) + ewld + ehart

  ! calculate new D_nm matrix for ultrasoft pseudopotential
  if (okvan) then
    if (istep == -1 .or. ( (nupdate_Dnm /= 0 .and. mod(istep,nupdate_Dnm) == 0) ) ) then
      call newd()
      if (iverbosity > 10) write(stdout,'(5X,''call newd'')')
    endif
  endif
    
  call deallocate_bec_type(becp)
  call stop_clock('updateH')
    
END SUBROUTINE update_hamiltonian


!-----------------------------------------------------------------------
SUBROUTINE calculate_eband
  !-----------------------------------------------------------------------
  !
  ! ... Calculate band energy
  !
  USE kinds,                       ONLY : dp
  USE wavefunctions_module,        ONLY : evc
  USE io_global,                   ONLY : stdout, ionode
  USE io_files,                    ONLY : nwordwfc, iunwfc, iunigk
  USE klist,                       ONLY : nks
  USE wvfct,                       ONLY : nbnd, npwx, npw, wg, current_k
  USE lsda_mod,                    ONLY : current_spin, lsda, isk, nspin
  USE becmod,                      ONLY : becp, calbec
  USE mp,                          ONLY : mp_sum, mp_barrier
  USE mp_pools,                    ONLY : intra_pool_comm, inter_pool_comm
  USE buffers,                     ONLY : get_buffer, save_buffer
  USE uspp,                        ONLY : nkb, vkb, deeq
  USE pwcom
  USE tddft_module

  implicit none
  complex(dp), allocatable :: hpsi(:,:)
  integer :: ik, ibnd
  complex(dp), external :: zdotc
 
  allocate(hpsi(npwx,nbnd))
  eband = 0.d0
    
  ! loop over k-points     
  if (nks > 1) rewind (iunigk)
  do ik = 1, nks
    current_k = ik
    current_spin = isk(ik)

    ! initialize at k-point k 
    call gk_sort(xk(1,ik), ngm, g, ecutwfc/tpiba2, npw, igk, g2kin)
    g2kin = g2kin * tpiba2
    call init_us_2(npw, igk, xk(1,ik), vkb)

    ! read wfcs from file and compute becp
    evc = (0.d0, 0.d0)
    call get_buffer(evc, nwordwfc, iunevcn, ik)
    !call calbec(npw, vkb, evc, becp)
 
    ! calculate H |psi_current>
    call h_psi(npwx, npw, nbnd_occ(ik), evc, hpsi)
    
    ! add to eband
    do ibnd = 1, nbnd
        eband = eband + wg(ibnd,ik)*real(zdotc(npw,hpsi(1,ibnd),1,evc(1,ibnd),1),dp)
    enddo
  enddo
  call mp_sum(eband, intra_pool_comm)
  call mp_sum(eband, inter_pool_comm)
  
  eband = eband + delta_e()
  
  deallocate(hpsi)
  
  CONTAINS
  
   !-----------------------------------------------------------------------
   FUNCTION delta_e()
     !-----------------------------------------------------------------------
     ! ... delta_e = - \int rho%of_r(r)  v%of_r(r)
     !               - \int rho%kin_r(r) v%kin_r(r) [for Meta-GGA]
     !               - \sum rho%ns       v%ns       [for LDA+U]
     !               - \sum becsum       D1_Hxc     [for PAW]
     USE scf,              ONLY : scf_type, rho, rho_core, rhog_core, v, &
                                  vltot, vrs, kedtau, vnew
     USE funct,            ONLY : dft_is_meta
     USE fft_base,         ONLY : dfftp
     USE noncollin_module, ONLY : noncolin
     USE mp_bands,         ONLY : intra_bgrp_comm
     USE paw_variables,    ONLY : okpaw, ddd_paw
     IMPLICIT NONE
     REAL(DP) :: delta_e, delta_e_hub
     !
     delta_e = - SUM( rho%of_r(:,:)*v%of_r(:,:) )
     !
     IF ( dft_is_meta() ) &
        delta_e = delta_e - SUM( rho%kin_r(:,:)*v%kin_r(:,:) )
     !
     delta_e = omega * delta_e / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
     !
     CALL mp_sum( delta_e, intra_bgrp_comm )
     !
     if (lda_plus_u) then
       if (noncolin) then
         delta_e_hub = - SUM (rho%ns_nc(:,:,:,:)*v%ns_nc(:,:,:,:))
         delta_e = delta_e + delta_e_hub
       else
         delta_e_hub = - SUM (rho%ns(:,:,:,:)*v%ns(:,:,:,:))
         if (nspin==1) delta_e_hub = 2.d0 * delta_e_hub
         delta_e = delta_e + delta_e_hub
       endif
     end if
     !
     IF (okpaw) delta_e = delta_e - SUM(ddd_paw(:,:,:)*rho%bec(:,:,:))
     !
     RETURN
     !
   END FUNCTION delta_e

END SUBROUTINE calculate_eband
   
