#!/bin/bash
work=$( pwd )
for system in AIMD nonlocal_NAKE thiophene-dimer beginner mGGA-L_xc nonlocal_xc use_gaussians
do 
  cd ${work}/CI/${system}
  echo "Tesing the ${system} ... "
  bash run.bash
  echo "DONE ${system}"
done


