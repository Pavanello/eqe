#!/bin/sh
# compute dependencies for the PWscf directory tree

# make sure there is no locale setting creating unneeded differences.
export LC_ALL=C

QE_SOURCE=/cache/home/wm268/Hackthon/mCI

if [ "$QE_SOURCE"=="../" ]; then
 QE_SOURCE="$( echo $( pwd ) | sed 's/\/TDDFT//' )"
fi

if [ "$QE_SOURCE"==".." ]; then
 QE_SOURCE="$( echo $( pwd ) | sed 's/\/TDDFT//' )"
fi

DEPENDS="${QE_SOURCE}/include ${QE_SOURCE}/iotk/src
         ${QE_SOURCE}/Modules ${QE_SOURCE}/PW/src"

cd src

${QE_SOURCE}/install/moduledep.sh $DEPENDS > make.depend
${QE_SOURCE}/install/includedep.sh $DEPENDS >> make.depend

# handle special cases
# cumbersome - but otherwise it doesn't work on MacOS
sed '/@\/cineca\/prod\/hpm\/include\/f_hpm.h@/d' make.depend > make.depend.tmp
sed '/@iso_c_binding@/d;/@ifcore@/d' make.depend.tmp > make.depend
sed '/@iotk_module@/d' make.depend > make.depend.tmp
sed '/@either@/d' make.depend.tmp > make.depend
rm make.depend.tmp

# check for missing dependencies 
if grep @ make.depend
then
  echo WARNING: dependencies not found in directory 'src'
  exit 1
fi
