MODULE current

  USE tddft_module
  use lsda_mod,             only : nspin
  USE kinds,                ONLY : DP
  USE cell_base,            ONLY : tpiba, omega, alat, tpiba2
  USE fft_base,             only : dffts 
  USE mp_global,            only : intra_bgrp_comm
  USE mp,                   ONLY : mp_sum, mp_bcast
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE constants,            ONLY : tpi

  PUBLIC :: calc_current, print_current

contains
!----------------------------------------------------------------------------
SUBROUTINE calc_current_old(current,divergence)
  !----------------------------------------------------------------------------
  !
  ! ... Calculates the current -i<psi_i|\nabla_j|psi_i> with j being
  ! e_direction. Based on sum_band_k.
  !
  USE control_flags,        ONLY : gamma_only
  USE gvect,                ONLY : nlm, g, gg, ngm
  USE gvecs,                only : nls
  USE wvfct,                ONLY : igk, g2kin, ecutwfc,&
                                   wg, nbnd
  USE wavefunctions_module, ONLY : evc
  USE klist,                ONLY : nks, wk, xk, ngk
  USE buffers,              ONLY : get_buffer, save_buffer
  USE io_files,             only : nwordwfc
  USE fft_interfaces,       only : fwfft, invfft

  !
  IMPLICIT NONE
  !
  !
  real(dp), intent(inout) :: current(dffts%nnr)
  real(dp), intent(inout) :: divergence(dffts%nnr)
  !
  !
  ! local vars 
  integer :: ik, ibnd, nvec, ipol, ig
  real(dp) :: gcurrent(size(g,2))
  real(dp) :: myg(size(g,2)),mygc(size(g,2))
  !
  ! buffers
  complex(dp), allocatable :: maux1(:)
  !

  ! checking that we do not do something that we currently cannot
  if (gamma_only) call errore('calc_current','J not available for gamma_only',1)
  if (nspin==2)   call errore('calc_current','J only for closed-shell subsystems',1)


  current  = 0._DP
  divergence  = 0._DP

  ! allocate tmp buffers
  allocate(maux1(dffts%nnr))

  k_loop: DO ik = 1, nks

      ! find appropriate g-vectors for this k-point
      gcurrent = 0._DP
      call gk_sort_current(xk(:,ik), ngm, g, ecutwfc/tpiba2, nvec, igk, gcurrent)
      gcurrent = gcurrent * tpiba
      myg(1:nvec)      = dsqrt((xk(1,ik)+g(1,igk(1:nvec)))**2 &
                              +(xk(2,ik)+g(2,igk(1:nvec)))**2 &
                              +(xk(3,ik)+g(3,igk(1:nvec)))**2)*tpiba

      call get_buffer (evc, nwordwfc, iunevcn, ik)

     ! sum over all bands - to be weighted by wg
     ! could use openmp parallelization here
     DO ibnd = 1, nbnd
         maux1(:) = 0.0_DP

         ! maux1 =   psi(G)^* \hat{p} * psi(G)
         ! we use a screened p because of numerical inaccuracies for large Gs.
         maux1(nls(igk(1:nvec)))=conjg(evc(1:nvec,ibnd))*                    &
                                 gcurrent(1:nvec)*                           &
                                 exp(-0.5*(myg(1:nvec)*current_screen)**2) * &
                                 evc(1:nvec,ibnd)


         maux1 = cmplx(dble(maux1),0.0_DP,kind=DP)

         ! maux1 = J(r)
         call invfft('Wave',maux1,dffts)
         ! this is the current density
         current = current + dble(maux1) * wg(ibnd,ik)
         !
         !
         if (l_divergence) then
         ! now div(J)=grad dot J
         !
         !
         maux1(:) = 0.0_DP
         !
         do ipol = 1, 3
              !      
              gcurrent(1:nvec) = (xk(ipol,ik) + g(ipol,igk(1:nvec))) * tpiba
              !mygc(1:nvec)     = g(ipol,igk(1:nvec)) * tpiba 
              mygc(1:nvec)     = gcurrent(1:nvec)
              !      
              do ig=1,nvec
                maux1(nls(igk(ig)))= maux1(nls(igk(ig))) + dble(conjg(evc(ig,ibnd))* &
                                        gcurrent(ig)*mygc(ig)*                       &
                                        exp(-(myg(ig)*current_screen)**2) *          &
                                        evc(ig,ibnd))
              enddo
              !      
         enddo
         !
         !
         maux1 = cmplx(0.0_DP,dble(maux1),kind=DP)
         ! maux1 = div(J(r))
         call invfft('Wave',maux1,dffts)
         ! this is the divergence of the current density
         divergence = divergence + dble(maux1) * wg(ibnd,ik)
         endif ! l_divergence
     enddo

  END DO k_loop

  deallocate(maux1)

  !
  ! current is now a vector which is already distributed 
  !     across the pools of procs
  !
  RETURN
  !
END SUBROUTINE calc_current_old
!
!----------------------------------------------------------------------------
!----------------------------------------------------------------------------
SUBROUTINE calc_current(current,divergence)
  !----------------------------------------------------------------------------
  !
  ! ... Calculates the current -i<psi_i|\nabla_j|psi_i> with j being
  ! e_direction. Based on sum_band_k.
  !
  USE control_flags,        ONLY : gamma_only
  USE gvect,                ONLY : nlm, g, gg, ngm
  USE gvecs,                only : nls
  USE wvfct,                ONLY : igk, g2kin, ecutwfc,&
                                   wg, nbnd
  USE wavefunctions_module, ONLY : evc
  USE klist,                ONLY : nks, wk, xk, ngk
  USE buffers,              ONLY : get_buffer, save_buffer
  USE io_files,             only : nwordwfc
  USE fft_interfaces,       only : fwfft, invfft
  USE mp_bands,             ONLY : intra_bgrp_comm

  !
  IMPLICIT NONE
  !
  !
  real(dp), intent(inout) :: current(dffts%nnr)
  real(dp), intent(inout) :: divergence(dffts%nnr)
  !
  !
  ! local vars 
  integer :: ik, ibnd, nvec, ipol, ig
  real(dp) :: gcurrent(size(g,2))
  real(dp) :: myg(size(g,2)),mygc(size(g,2))
  real(dp) :: mycurrent, mydiv
  real(dp) :: domega
  !
  !

  ! checking that we do not do something that we currently cannot
  if (gamma_only) call errore('calc_current','J not available for gamma_only',1)
  if (nspin==2)   call errore('calc_current','J only for closed-shell subsystems',1)

  mycurrent = 0.0_DP
  mydiv     = 0.0_DP

  k_loop: DO ik = 1, nks

      ! find appropriate g-vectors for this k-point
      call gk_sort_current(xk(:,ik), ngm, g, ecutwfc/tpiba2, nvec, igk, gcurrent)
      gcurrent = gcurrent * tpiba
      myg(1:nvec)      = dsqrt((xk(1,ik)+g(1,igk(1:nvec)))**2 &
                              +(xk(2,ik)+g(2,igk(1:nvec)))**2 &
                              +(xk(3,ik)+g(3,igk(1:nvec)))**2)*tpiba

      call get_buffer (evc, nwordwfc, iunevcn, ik)

     ! sum over all bands - to be weighted by wg
     ! could use openmp parallelization here
    
     DO ibnd = 1, nbnd
         ! maux1 =   psi(G)^* \hat{p} * psi(G)
         ! we use a screened p because of numerical inaccuracies for large Gs.
         mycurrent=mycurrent+wg(ibnd,ik)*sum(dble(conjg(evc(1:nvec,ibnd))*   &
                   gcurrent(1:nvec)*                                         &
                   exp(-0.5*(myg(1:nvec)*current_screen)**2) *               &
                   evc(1:nvec,ibnd)))

         mydiv=mydiv+wg(ibnd,ik)*sum(dble(conjg(evc(1:nvec,ibnd))*           &
                   gcurrent(1:nvec)*gcurrent(1:nvec)*                        &
                   exp(-0.5*(myg(1:nvec)*current_screen)**2) *               &
                   evc(1:nvec,ibnd)))
     enddo ! ibnd

  END DO k_loop

  ! all is carried out in the smooth cell (both real and reciprocal space)
    !domega = tpi**3 / omega / dble(dffts%nr1x * dffts%nr2x * dffts%nr3x)

    domega = 0.5_DP ! get them to a.u.

  ! now print the current
    call mp_sum(mycurrent,intra_bgrp_comm)
    call mp_sum(mydiv,intra_bgrp_comm)
    !if (ionode) write(stdout,'(1x,a,1x,E20.15)') "Avg Current =", mycurrent*domega
    !if (ionode) write(stdout,'(1x,a,1x,E20.15)') "Avg KE      =", mydiv*domega
    if (ionode) write(stdout,*) "Current =", mycurrent*domega
    if (ionode) write(stdout,*) "KE      =", mydiv*domega
  !
END SUBROUTINE calc_current
!
!----------------------------------------------------------------------------
!----------------------------------------------------------------------------
SUBROUTINE print_current(current,divergence)
  !----------------------------------------------------------------------------
  !
  ! Print current density 
  !   - across a given plane specified by the perpendicular lattice vector "pvec" 
  !   - plane has given thickness "thkns"   (in alat units)
  !   - plane is positioned in "p_position" (in alat units)
  ! 
  !
  use fft_base, only : gather_smooth
  USE tddft_module
  USE ions_base,                ONLY : ntyp => nsp, atm, zv, tau, ityp, nat
  USE gvect,                    ONLY : gcutm
  USE gvecs,                    ONLY : dual
  USE wvfct,                    ONLY : ecutwfc
  USE io_global,                ONLY : ionode, stdout
  USE io_files,                 ONLY : prefix
  USE cell_base,                ONLY : celldm, at, ibrav
  !
  !
  implicit none
  !
  !
  real(dp), intent(in)  :: current(dffts%nnr)
  real(dp), intent(in)  :: divergence(dffts%nnr)
  !
  !
  !
  real(dp), allocatable :: current_gathered(:),     &
                           mask(:,:,:), &
                           current_reshaped(:,:,:)
  real(dp), allocatable :: divergence_gathered(:)
  real(dp)              :: p_initial, p_final, domega
  integer               :: i_initial, i_final, ipnt
  integer               :: p_vec
  character(20)         :: cfile
  !
  RETURN
  !
  p_vec = e_direction
  !
  ! all is carried out in the smooth cell (both real and reciprocal space)
  domega = omega / dble(dffts%nr1x * dffts%nr2x * dffts%nr3x)
  !
  !
  !
  !
  if (ionode) then
   allocate (current_gathered(dffts%nr1x*dffts%nr2x*dffts%nr3x))
   current_gathered = 0.0_DP
   !
   if (l_divergence) then 
     allocate (divergence_gathered(dffts%nr1x*dffts%nr2x*dffts%nr3x))
     divergence_gathered = 0.0_DP
   endif
   !
   allocate (current_reshaped(dffts%nr1x,dffts%nr2x,dffts%nr3x))
   allocate (mask(dffts%nr1x,dffts%nr2x,dffts%nr3x))
  endif !ionode
  !
  !
  call gather_smooth(current,current_gathered)
  if (l_divergence) call gather_smooth(divergence,divergence_gathered)
  !
  !
  if (ionode) then
     !
     !
     !write(stdout,'(1x,a,1x,f12.7)') 'Max Current =', maxval(abs(current_gathered))
     !write(stdout,'(1x,a,1x,f12.7)') 'Max Divergence =', maxval(abs(divergence_gathered))
     !if (l_divergence) write(stdout,'(1x,a,1x,f12.7)') "Total Divergence =", sum(divergence_gathered)*domega
     current_reshaped = RESHAPE(current_gathered,(/dffts%nr1x, dffts%nr2x, dffts%nr3x/))
     !
     !
     ! now sum current_gathered across a plane
     
     ! generate a mask, such that it is 1.0 in the small region of 
     ! space where we want to trace the current
   
     p_initial =  (p_position - thkns*0.5_DP) 
     p_final   =  (p_position + thkns*0.5_DP) 
   
     mask = 0.0_DP
     if (p_vec == 1) then
         i_initial = int( p_initial * dffts%nr1x ) + 1
         i_final   = int( p_final * dffts%nr1x )
         mask(i_initial:i_final,1:dffts%nr2x,1:dffts%nr3x)=1.0_DP
     elseif (p_vec ==2) then
         i_initial = int( p_initial * dffts%nr2x ) + 1
         i_final   = int( p_final * dffts%nr2x )
         mask(1:dffts%nr1x,i_initial:i_final,1:dffts%nr3x)=1.0_DP
     elseif (p_vec ==3) then
         i_initial = int( p_initial * dffts%nr3x ) + 1
         i_final   = int( p_final * dffts%nr3x )
         mask(1:dffts%nr1x,1:dffts%nr2x,i_initial:i_final)=1.0_DP
     endif
     !
     ! now print the current
     write(stdout,'(1x,a,1x,f12.7)') "Plane-Avg Current =", sum(mask*current_reshaped)*domega

  ! print full current to file(s)
  if (l_current_printfile.and.mod(istep,i_current_step)==0) then
    if (istep .lt. 10)                            write(cfile,'(I1)') istep
    if (istep .lt. 100  .and. istep .ge. 10)      write(cfile,'(I2)') istep
    if (istep .lt. 1000 .and. istep .ge. 100)     write(cfile,'(I3)') istep
    if (istep .lt. 10000 .and. istep .ge. 1000)   write(cfile,'(I4)') istep
    if (istep .lt. 100000 .and. istep .ge. 10000) write(cfile,'(I5)') istep
   ! cfile = 'current_'//trim(cfile)//'.dat'
   ! open(unit=50, file=cfile, status="REPLACE")
   ! if (p_vec == 1) i_final = dffts%nr1x
   ! if (p_vec == 2) i_final = dffts%nr2x
   ! if (p_vec == 3) i_final = dffts%nr3x
   ! do ipnt=1,i_final
   !   if (p_vec == 1) write(50,*) sum(current_reshaped(ipnt,:,:))*domega
   !   if (p_vec == 2) write(50,*) sum(current_reshaped(:,ipnt,:))*domega
   !   if (p_vec == 3) write(50,*) sum(current_reshaped(:,:,ipnt))*domega
   ! enddo
   ! close(50)
     !
     !
     !
     ! plot J and div(J) out
     !
      call plot_io (trim(prefix)//'_current_'//trim(cfile)//'.pp', 'FDE J', &
             dffts%nr1x, dffts%nr2x, dffts%nr3x, &
             dffts%nr1,  dffts%nr2,  dffts%nr3, nat, ntyp, ibrav, celldm, at, &
             gcutm, dual, ecutwfc, 0, atm, ityp, zv, tau, current_gathered, +1)
     !
     !
     if (l_divergence) then
       call plot_io (trim(prefix)//'_divergence_'//trim(cfile)//'.pp', 'FDE div-J', & 
              dffts%nr1x, dffts%nr2x, dffts%nr3x, &
              dffts%nr1,  dffts%nr2,  dffts%nr3, nat, ntyp, ibrav, celldm, at, &
              gcutm, dual, ecutwfc, 0, atm, ityp, zv, tau, divergence_gathered, +1)
     endif 
     !
     !
      call flush_unit(stdout)
     !
     !
  endif
  deallocate(current_reshaped,mask)
  deallocate(current_gathered)
  if (l_divergence) deallocate(divergence_gathered)
 endif ! ionode
  !
  RETURN
  !
END SUBROUTINE print_current
!
END MODULE current





