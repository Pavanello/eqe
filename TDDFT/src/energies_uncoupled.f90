subroutine energies_uncoupled()
!
!  calculate energy of the environment for an uncoupled calulcation   \int v_env(r) * \rho_env(r) dr
!
  USE fft_base,                 ONLY : dfftp, grid_gather, dfftl
  USE cell_base,                ONLY : celldm, at, ibrav, omega
  USE large_cell_base,                ONLY : celldml => celldm, atl => at, ibravl => ibrav, omegal => omega
  USE ions_base,                ONLY : ntyp => nsp, atm, zv
  USE gvect,                    ONLY : gcutm, ngm
  USE gvecl,                    ONLY : gcutml => gcutm, ngml => ngm
  USE gvecs,                    ONLY : dual
  USE wvfct,                    ONLY : ecutwfc
  USE io_global,                ONLY : ionode, stdout
  USE mp_global,                ONLY : my_image_id
  USE mp,                          ONLY : mp_sum
  USE scf,                      ONLY : rho, rho_core, rhog_core
  use input_parameters,         only : fde_xc_funct
  USE mp_global,  ONLY : intra_bgrp_comm
  USE fde
  use fde_routines
  use tddft_module
  implicit none

  real(dp), allocatable :: vemb(:,:), aux(:,:), rho_core_env(:), aux_fde(:,:)
  real(dp), allocatable :: rho_gauss_env(:) 
  complex(dp), allocatable :: rhog_gauss_env(:), rhog_core_env(:)
  complex(dp), allocatable :: environ_rhog(:,:)
  type(scf_type) :: rho_env
  real(dp) :: trash0, trash1, trash2, trash3
  real(dp) :: domega

!  allocate( vemb(dfftp%nnr,fde_frag_nspin))
  allocate( aux(dfftp%nnr,fde_frag_nspin) )
  if (fde_frag_nspin /= fde_nspin) allocate(aux_fde(dfftp%nnr,fde_nspin))
 

if (fde_frag_nspin /= fde_nspin) then
       e_tot = 0.0d0
       aux_fde(:,:)=0.d0
       call fde_fake_nspin(.true.)
!eh_env 
       call v_h( rho_fde%of_g, trash0, trash1, aux_fde )
!       call v_join_spin (aux,aux_fde) ! I don't think I need this, cause I'm not using the potential. check.
       e_tot = e_tot + trash0
       aux_fde(:,:)=0.d0
!exc_env
       call v_xc(rho_fde, rho_core_fde, rhog_core_fde, trash0, trash1, aux_fde)
!       call v_join_spin (aux,aux_fde) ! I don't think I need this, cause I'm not using the potential. check.
       e_tot = e_tot + trash0
       aux_fde(:,:)=0.d0
! ts_env
       !call fde_kin(rho_fde, rho_gauss_fde, rhog_gauss_fde, trash0, aux_fde)
       call errore('fde_kin needs to be adapted to linterlock!')
       e_tot = e_tot + trash0
       call fde_fake_nspin(.false.)
 else
!eh_env 
       aux(:,:)=0.d0
       call v_h( rho_fde%of_g, trash0, trash1, aux )
       e_tot = e_tot + trash0

!exc_env
       aux(:,:) = 0.d0
       call v_xc(rho_fde, rho_core_fde, rhog_core_fde, trash0, trash1, aux)
       e_tot = e_tot + trash0

!!!!!!!!!!
! ts_env
       aux(:,:) = 0.d0
       !call fde_kin(rho_fde, rho_gauss_fde, rhog_gauss_fde, trash0, aux)
       call errore('fde_kin needs to be adapted to linterlock!')
       e_tot = e_tot + trash0
endif

  deallocate( aux)
  if (fde_frag_nspin /= fde_nspin) deallocate(aux_fde)

end subroutine energies_uncoupled
