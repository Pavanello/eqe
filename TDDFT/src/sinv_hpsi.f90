!
! Copyright (C) 2001-2007 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
!SUBROUTINE sinv_hpsi( lda, n, m, hpsi, spsi )
SUBROUTINE sinv_hpsi_routine( lda, m, hpsi, sinv_psi )
  !----------------------------------------------------------------------------
  !
  ! ... This routine applies the S^-1 matrix to m wavefunctions psi
  ! ... and puts the results in sinv_psi.
  ! ... Requires the products of psi with all beta functions
  ! ... in array becp(nkb,m) (calculated in h_psi or by calbec)
  !
  ! ... input:
  !
  ! ...    lda   leading dimension of arrays psi, spsi
  ! ...    n     true dimension of psi, spsi
  ! ...    m     number of states psi
  ! ...    psi
  !
  ! ... output:
  !
  ! ...    sinv_psi  S^-1*psi
  !
  USE kinds,      ONLY : DP
  USE wvfct,                       ONLY : nbnd, npwx, npw, igk, g2kin, current_k, ecutwfc
  USE tddft_module
  USE uspp,                        ONLY : nkb, vkb, okvan

  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(IN) :: lda, m
  COMPLEX(DP), INTENT(IN)  :: hpsi(lda,m)
  COMPLEX(DP), INTENT(OUT) :: sinv_psi(lda,m)
  COMPLEX(DP)              :: tmp_psi(lda,m)
  External    :: s_1psi
  INTEGER     :: ik, ibnd, lter, flag_global, ip, is, fde_cycle
  !
  !
  !  If Norm-conserving
  IF ( nkb == 0 .OR. .NOT. okvan ) then
      sinv_psi = hpsi
      RETURN
  else
  !  If US-PP
      !call errore ('sinv_hpsi', 'Not implemented yet for ultrasoft', 1)
       !call s_psi(npwx, npw, nbnd, evc, psi)
       !s_1psi( npwx, n, psi, spsi )
       call sinv_cgsolver(s_1psi, hpsi, sinv_psi, npwx, npw, & 
            conv_threshold, lter, flag_global, nbnd )
  endif
  !
END SUBROUTINE sinv_hpsi_routine
