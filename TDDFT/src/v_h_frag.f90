!----------------------------------------------------------------------------
SUBROUTINE v_h_frag( rhog, rhor, ehart_frag, charge_frag)
  !----------------------------------------------------------------------------
  USE kinds,         ONLY : dp
  use lsda_mod,                 only : nspin
  use fft_base,                 only : dfftp, dffts, dfftl
  use fft_interfaces,           only : fwfft
  use cell_base,                only : omega, tpiba2
  use large_cell_base,                only : omegal => omega, tpiba2l => tpiba2
  use gvect,                    only : ngm, gstart
  use gvecl,                    only : ngml => ngm, gstartl => gstart, nll => nl
  use mp_global,                only : intra_pool_comm, intra_bgrp_comm
  use mp,                       only : mp_sum
  use io_global,                only : stdout
  use fde,                      only : linterlock
  use fde_routines
  implicit none
  !-- parameters --------------------------------------------------------------
  complex(dp), intent(in) :: rhog(ngm,nspin)
  real(dp), intent(in) ::    rhor(dfftp%nnr,nspin)
  real(dp), intent(out)   :: ehart_frag, charge_frag ! charge of single fragment
  !-- local variables ---------------------------------------------------------
  real(dp), allocatable    :: v_frag(:,:), v_fragl(:,:), rhor_large(:,:) 
  real(dp)                 :: tot_charge, domega
  complex(dp), allocatable :: rhog_large(:,:), gauxl(:)
  integer :: is
  

  ! calculate total Hartree potential and add it to XC potential
  ! no  need for fde_frag_nspin /= fde_nspin because working only with fragment quantities

 allocate( v_frag(dfftp%nnr,nspin) )
 if (linterlock)  allocate(v_fragl(dfftl%nnr,nspin))
 if (linterlock) allocate(rhog_large(ngml,nspin))
 if (linterlock) allocate(rhor_large(dfftl%nnr,nspin))
 if (linterlock) allocate(gauxl(dfftl%nnr))
 if (linterlock) then
      v_fragl = 0.d0
! transfer rhor to large cell, then fourier transform it to get rhog_large. From what I understood, that's the only way to transfer from small cell to large cell in G-space. I might be wrong.
      do is = 1, nspin
         call copy_pot_f2l(rhor(:,is), rhor_large(:,is))
         gauxl(:)=cmplx(rhor_large(:,is),0.d0,kind=dp)
         call fwfft('Custom', gauxl, dfftl)
         rhog_large(1:ngml,is)=gauxl(nll(1:ngml))
      enddo
      call v_h_large(rhog_large, ehart_frag, tot_charge, v_fragl)
      do is = 1, nspin
         call copy_pot_l2f(v_fragl(:,is), v_frag(:,is))
      enddo
 else ! linterlock
      v_frag = 0.0d0
      call v_h( rhog, ehart_frag, tot_charge, v_frag )
 endif

  ! calculate charge of fragment
  charge_frag = 0.d0
  if ( gstart == 2 ) then
     charge_frag = omega*real( rhog(1,1) )
     if ( nspin == 2 ) charge_frag = charge_frag + omega*real( rhog(1,2) )
  end if

  call mp_sum(charge_frag, intra_bgrp_comm)

  ! calculate Hartree energy of fragment
  ! should I do this on the big cell for linterlock?
  ! TODO: non-collinear
  domega = omega / dble(dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
  ehart_frag = sum(rhor(:,1) * v_frag(:,1))


  if (nspin == 2)  ehart_frag = ehart_frag + sum(rhor(:,2) * v_frag(:,2))
  ehart_frag = 0.5d0 * ehart_frag * domega

  CALL mp_sum(ehart_frag , intra_bgrp_comm )

  deallocate (v_frag)
  if (linterlock) deallocate(v_fragl,rhog_large,gauxl)
  return
END SUBROUTINE v_h_frag
  
