subroutine e_env()
!
!  equivalent to e_emb but calculates the "embedding" energy for the frozen
!  part. To save computation time, etxc_tot and ekin_tot are not calculated
!  so we only call v_xc and fde_kin with rho_env and substruct them
!
  USE fft_base,                 ONLY : dfftp, grid_gather, dfftl
  USE cell_base,                ONLY : celldm, at, ibrav, omega, bg, alat
  USE large_cell_base,          ONLY : celldml => celldm, atl => at, ibravl => ibrav, omegal => omega, bgl => bg,&
                                       alatl => alat
  USE ions_base,                ONLY : ntyp => nsp, atm, zv, nat, tau, ityp
  USE vlocal,        ONLY : strf, vloc
  USE gvect,                    ONLY : gcutm, ngm, gstart, g, gg , nl, nlm, igtongl
  USE gvecl,                    ONLY : gcutml => gcutm, ngml => ngm, gstartl => gstart, gl => g, ggl => gg,&
                                       nll => nl, nlml => nlm, igtongll => igtongl
  USE gvecs,                    ONLY : dual
  USE wvfct,                    ONLY : ecutwfc
  USE io_global,                ONLY : ionode, stdout
  USE mp_global,                ONLY : my_image_id
  USE mp,                          ONLY : mp_sum
  USE scf,                      ONLY : rho, rho_core, rhog_core, create_scf_type, destroy_scf_type
  USE fft_interfaces,ONLY : invfft
  use input_parameters,         only : fde_xc_funct
  USE mp_global,  ONLY : intra_bgrp_comm
  USE control_flags, ONLY : gamma_only
  USE fde
  use fde_routines
  use tddft_module
  USE martyna_tuckerman, ONLY : wg_corr_loc, do_comp_mt
  USE esm,       ONLY : esm_local, esm_bc, do_comp_esm
  implicit none

  real(dp), external :: ewald
  real(dp), allocatable :: vemb(:,:), aux(:,:), rho_core_env(:), aux_fde(:,:)
  real(dp), allocatable :: rho_gauss_env(:) 
  complex(dp), allocatable :: rhog_gauss_env(:), rhog_core_env(:)
  complex(dp), allocatable :: environ_rhog(:,:)
  real(dp), allocatable :: tau_emb(:,:)
  complex(dp), allocatable :: strf_emb(:,:)
  integer, allocatable :: ityp_emb(:)
  type(scf_type) :: rho_env
  real(dp) :: trash0, trash1, trash2, trash3
  real(dp) :: domega
  integer :: ierr, nat_emb, i, nt, ng
  COMPLEX(DP), ALLOCATABLE :: aux_compl (:), v_corr(:)

  allocate( vemb(dfftp%nnr,fde_frag_nspin))
  allocate( aux(dfftp%nnr,fde_frag_nspin) )
  if (fde_frag_nspin /= fde_nspin) allocate(aux_fde(dfftp%nnr,fde_frag_nspin))
  allocate( rho_gauss_env(dfftp%nnr), rhog_gauss_env(ngm) , stat=ierr)
  allocate( rho_core_env(dfftp%nnr), rhog_core_env(ngm) , stat=ierr)
  call create_scf_type(rho_env)

  if (fde_frag_nspin /= fde_nspin) then
    write(stdout,*) 'In e_emb: not sure if correct for open shell'
  endif
  rho_env%of_r(:,:) = rho_fde%of_r(:,:) - rho%of_r(:,:)
  rho_env%of_g(:,:) = rho_fde%of_g(:,:) - rho%of_g(:,:)
  rho_core_env(:) = rho_core_fde(:) - rho_core(:)
  rho_gauss_env(:) = rho_gauss_fde(:) - rho_gauss(:)
  rhog_core_env(:) = rhog_core_fde(:) - rhog_core(:)
  rhog_gauss_env(:) = rhog_gauss_fde(:) - rhog_gauss(:)

  e_environment = 0.0d0
  if (fde_frag_nspin /= fde_nspin) then
     aux_fde(:,:)=0.d0
     call fde_fake_nspin(.true.)
     call v_xc(rho_env, rho_core_env, rhog_core_env, trash0, trash1, aux_fde)
     exc_env = trash0
     e_environment = e_environment - exc_env
     aux_fde(:,:)=0.d0
     call errore('e_env needs to be adapted to linterlock!')
     !call fde_kin(rho_env, rho_gauss_env, rhog_gauss_env, trash0, aux_fde)
     ts_env = trash0
     e_environment = e_environment - ts_env
     aux_fde(:,:)=0.d0
     call v_h_fde( rho_env%of_g, rho_env%of_r, rho%of_g, trash0, trash1, aux_fde ) 
     eh_env = trash0
     e_environment = e_environment + eh_env
     call fde_fake_nspin(.false.)
  else
!exc_env
    aux(:,:) = 0.d0
    
    call v_xc(rho_env, rho_core_env, rhog_core_env, trash0, trash1, aux)
    exc_env = trash0
    e_environment = e_environment - exc_env
! ts_env
    aux(:,:) = 0.d0
     call errore('e_env needs to be adapted to linterlock!')
    !call fde_kin(rho_env, rho_gauss_env, rhog_gauss_env, trash0, aux)
    ts_env = trash0
    e_environment = e_environment - ts_env
!eh_env 
    aux(:,:)=0.0d0
    call v_h_fde( rho_env%of_g, rho_env%of_r, rho%of_g, trash0, trash1, aux ) 
    eh_env = trash0
    e_environment = e_environment + eh_env
 endif

!e_en_env
  ALLOCATE (aux_compl( dfftp%nnr))
  aux_compl(:)=0.0d0
  IF (do_comp_mt) THEN
      ALLOCATE(v_corr(ngm))
      CALL wg_corr_loc(omega,ntyp,ngm,zv,strf,v_corr)
      aux_compl(nl(:)) = v_corr(:)
      DEALLOCATE(v_corr)
  END IF
  do nt = 1, ntyp
     do ng = 1, ngm
        aux_compl (nl(ng))=aux_compl(nl(ng)) + vloc (igtongl (ng), nt) * strf (ng, nt)
     enddo
  enddo
  IF (gamma_only) THEN
      DO ng = 1, ngm
          aux_compl (nlm(ng)) = CONJG(aux_compl (nl(ng)))
      END DO
  END IF
  !
  IF ( do_comp_esm .AND. ( esm_bc .NE. 'pbc' ) ) THEN
     !
     ! ... Perform ESM correction to local potential
     !
      CALL esm_local ( aux_compl )
  ENDIF
  !
  ! ... v_of_0 is (Vloc)(G=0)
  !
!  v_of_0=0.0_DP
!  IF (gg(1) < eps8) v_of_0 = DBLE ( aux (nl(1)) )
  !
!  CALL mp_sum( v_of_0, intra_bgrp_comm )
  !
  ! ... aux = potential in G-space . FFT to real space
  !
  CALL invfft ('Dense', aux_compl, dfftp)
  aux (:,1) =  DBLE (aux_compl (:) )

   e_en_env = sum(rho_env%of_r(:,1)*aux(:,1))
  if (fde_frag_nspin == 2) e_en_env = e_en_env + sum(rho_env%of_r(:,2)*aux(:,1))
  call mp_sum(e_en_env, intra_bgrp_comm) 
  domega = omega / dble(dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
   e_en_env = e_en_env *  domega
!   write(stdout,*)'e_en_env', e_en_env
  e_environment = e_environment + e_en_env


!    deallocate( aux )
!!!!!!!!!!

  nat_emb=nat_fde-nat
  ALLOCATE ( strf_emb(ngm, ntyp) )
  allocate (tau_emb(3,nat_emb), ityp_emb(nat_emb))
  i = atm_offset(currfrag)
  tau_emb(:,:)=0.0d0
  tau_emb(1:3,1:i-1)=tau_fde(1:3,1:i-1)
  tau_emb(1:3,i:nat_emb)=tau_fde(1:3,i+nat:nat_fde)
  ityp_emb(:)=0.0d0
  ityp_emb(1:i-1)=ityp_fde(1:i-1)
  ityp_emb(i:nat_emb)=ityp_fde(i+nat:nat_fde)
  strf_emb(:,:) = strf_fde(:,:) - strf(:,:)
!  write(stdout,*)'ewld',ewld
!  write(stdout,*)'ewld_frag',ewld_frag
     ewld_emb = ewald( alat, nat_emb, ntyp, ityp_emb, zv, at, bg, tau_emb, &
                    omega, g, gg, ngm, gcutm, gstart, gamma_only, strf_emb )
!   write(stdout,*)'ewld_emb',ewld_emb
!   e_environment = e_environment + ewld_emb

!  allocate( aux(dfftp%nnr,1) )

!   write(stdout,*)'e_environment = ',e_environment
!!!!!!!!!!
 

  deallocate( aux, vemb, rho_gauss_env, rhog_gauss_env, rho_core_env, rhog_core_env )
  call destroy_scf_type(rho_env)

end subroutine e_env
