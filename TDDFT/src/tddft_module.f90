!
! Copyright (C) 2001-2010 Quantum-ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!

! TODO: nsave, restart_mode

!-----------------------------------------------------------------------
MODULE tddft_module
  !-----------------------------------------------------------------------
  !
  ! ... This module contains the variables used for TDDFT calculations
  !
  USE kinds,               ONLY : dp
  USE dynamics_module,     ONLY : dt           ! timestep
  USE control_flags,       ONLY : istep, nstep, iverbosity ! current step and number of steps
  USE fixed_occ,           ONLY : tfixed_occ, f_inp ! occupations from input
  use scf,                      only: scf_type
  use scf_large,                only: scf_type_large => scf_type
  IMPLICIT NONE
  SAVE
  
  character(80) :: job             ! 'optical'
  CHARACTER(len=256) :: tmp_dir_frozen 
  integer  :: e_direction               ! impulse electric field direction: 1-x 2-y 3-z
  integer  :: e_direction_twoph         ! impulse electric field direction: 1-x 2-y 3-z
  integer  :: q_direction1,q_direction2 ! impulse electric field gradient tensor component (i,j) 
  real(dp) :: e_strength                ! impulse electron field strength
  real(dp) :: conv_threshold       ! cg convergence threshold
  real(dp) :: pc_conv_threshold       ! predictor-corrector convergence threshold
  integer  :: nupdate_Dnm          ! update USPP Dnm matrix every n steps
  logical  :: l_circular_dichroism ! calculate circular dichroism
  logical  :: l_quadrupole              ! calculates quadrupole
  logical  :: l_twoph              ! calculates two-photon dynamics (linear response only, i.e., pulse)
  logical  :: l_current            ! calcs current across a plane specified by 2 parameters in current.f90
  logical  :: l_divergence         ! calcs current across a plane specified by 2 parameters in current.f90
  logical  :: l_current_printfile  ! print out current_istep.pp files
  integer  :: i_current_step       ! how many steps apart should print current_istep.dat files
  real(dp) :: current_screen       ! how much to screen G in computation of J and div-J
  integer  :: istep_twoph          ! impulse electric field direction: 1-x 2-y 3-z
  logical  :: l_tddft_restart      ! restart propagation from the last step
  logical  :: molecule             ! use molecular routuines
  logical  :: ehrenfest            ! .true. if moving the ions
  integer  :: itdpulse             ! .true. if time dependent electric field
  real(dp) :: t0, tau0, omega0, ttend, sigma0 ! parameters for tdpulse
  real(dp) :: alpha
  integer  :: itefield, icalc_ener, icoupled, color,max_color, ilate_coupling, ifrozen, my_link, nlink
  integer, allocatable ::  colors(:), my_chain(:)
  logical  :: cn, rk, em, etrs, magnus, update, coupled, print_bands, calc_ener, late_coupling, frozen, chain
  integer  :: pmax, rkmax
  real(dp) :: etot_frag, charge_frag, ehart_frag, eband_frag, en_frag, en_tot, &
              & e_embedding, e_int, e_int2
  real(dp) :: etot_frag_sum, e_embedding_env, etxc_frag, vtxc_frag
  real(dp) :: e_environment,  e_tot, etot_frag_int, en_tot_tot, ekin_tot, etxc_tot, ewld_frag
  real(dp) :: ewld_emb, etot_frag_mixed, etot_frag_t0, etot_fde_coupled
  real(dp) :: eh_env, e_en_env, exc_env, ts_env, ts_frag
  real(dp) :: ewld_int, etot_frag_sum2, etot_t0,ekin0_nadd_t0,ekin_nadd_t0,etxc0_nadd_t0, &
              & etxc_nadd_t0, e_int_uncoupled, e_int_coupled
  real(dp), allocatable :: energies_of_all(:,:)
  real(dp) ::  energies_of_t0(6)
  REAL(DP) :: &
  ! both emaxpos and eopreg need to be in the vacuum, close to each other 
      emaxpos,       &! position of the maximum of the field (0<emaxpos<1)
      eopreg,        &! amplitude of the inverse region (0<eopreg<1)
      eamp,          &
      etotefield      
  
  complex(dp), parameter :: i_complex = (0.0_dp,1.0_dp)

  real(dp), allocatable :: r_pos(:,:)     ! position operator in real space
  real(dp), allocatable :: r_pos_s(:,:)   ! position operator in real space (smooth grid)
  integer, allocatable :: nbnd_occ(:)     ! occupied bands for each k-point
  integer :: nbnd_occ_max                 ! max number of occupied bands
  integer :: inter_coupled_comm
  type(scf_type) :: rho_coupled, rho_frozen, rho_t0, rho_frozen_tmp, rho_tmp, rho_tmp1, rho_tmp2
  type(scf_type_large) :: rho_coupled_large, rho_frozen_large, rho_t0_large, rho_frozen_tmp_large, &
                          rho_tmp1_large, rho_tmp2_large,  rho_tmp_large

  integer, parameter :: iuntdwfc = 51     ! to save TDDFT intermediate wfcs
  integer :: nwordtdwfc 
  integer, parameter :: iunevcn = 52      ! evc for restart
  real(dp) :: alpha_pv                    ! shift of conduction levels

  integer :: tddft_exit_code = 0

  complex(dp) :: ee                     ! i*dt/2

  real(dp) :: thkns      = 0.1_DP ! thickness of slab for calculation of current
  real(dp) :: p_position = 0.5_DP ! position of center of slab for calculation of current

  complex(dp), allocatable :: tddft_psi(:,:,:), b(:,:)
  complex(dp), allocatable :: tddft_hpsi(:,:), tddft_spsi(:,:), tddft_Ppsi(:,:)
  ! for cheap uncoupled calcualtions:
  real(dp), allocatable :: zv_frozen(:), tau_frozen(:,:)
  integer :: nat_frozen,ntyp_frozen
  integer, allocatable :: ityp_frozen(:)
  complex(dp), allocatable :: strf_frozen(:,:), strf_frag_large(:,:)
  
! hack for linterlock, because of the restart setup in PW
  logical :: linterlock_local=.true.

contains

   function clean_stop()
    USE io_global,        ONLY : ionode
    USE mp_world,         ONLY : world_comm
  USE mp,               ONLY : mp_bcast
   IMPLICIT NONE
!
   logical :: clean_stop, file_exists
   clean_stop = .false. 
   if (ionode) then
       inquire(file='CLEAN_STOP',exist=file_exists)
       if (file_exists) clean_stop = .true.
   endif
   
   call mp_bcast(clean_stop,0,world_comm)

   end function clean_stop
            
END MODULE tddft_module

