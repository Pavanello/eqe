subroutine tddft_frozen_setup()

  USE kinds,                       ONLY : dp
  USE io_global,                   ONLY : stdout, ionode, ionode_id
  USE io_files,                    ONLY : nwordwfc, iunwfc, iunigk, wfc_dir
  USE ions_base,                ONLY : ntyp => nsp, atm, zv
  USE cell_base,                   ONLY : at, tpiba, tpiba2, alat, celldm, ibrav, bg
  USE large_cell_base,             ONLY : atl => at, tpiba2l => tpiba2, alatl => alat, &!, tpiba => tpiba, &
                                          celldml => celldm, ibravl => ibrav, bgl => bg
  USE wavefunctions_module,        ONLY : evc, psic
  USE klist,                       ONLY : nks, xk
  USE wvfct,                       ONLY : nbnd, npwx, npw, igk, g2kin, current_k, ecutwfc, et
  USE lsda_mod,                    ONLY : current_spin, isk, nspin
  USE becmod,                      ONLY : allocate_bec_type, is_allocated_bec_type, deallocate_bec_type, becp
  USE mp_pools,                    ONLY : inter_pool_comm
  USE mp,                          ONLY : mp_sum, mp_barrier, mp_bcast
  USE mp_images,         ONLY : my_image_id, nimage, me_image, intra_image_comm
  USE gvect,                       ONLY : ngm, g, gcutm, nl, eigts1, eigts2, eigts3
  USE gvecl,                       ONLY : ngml => ngm, gl => g, gcutml => gcutm, nll => nl, eigts1l => eigts1, &
                                          eigts2l => eigts2, eigts3l => eigts3
  USE gvecs,                    ONLY : dual, doublegrid
  USE fft_base,                    ONLY : dfftp, grid_scatter, dfftl, grid_scatter_large
  use fft_interfaces,              only : fwfft
  USE buffers,                     ONLY : get_buffer, save_buffer, open_buffer
  USE uspp,                        ONLY : nkb, vkb
  USE scf,                         ONLY : create_scf_type, rho, destroy_scf_type, scf_type_copy, vltot, v, vrs, kedtau
  use scf_large,                   only : create_scf_type_large => create_scf_type, destroy_scf_type_large => destroy_scf_type,&
                                          scf_type_copy_large => scf_type_copy, vltot_large => vltot
  USE ener,                        ONLY : etot, ehart, eband, ewld, etxc, deband, etxcc
  USE tddft_module
  USE dynamics_module              
  USE fde
  use fde_routines
  use ruku
  use cnmod
  use constants,                   ONLY : rytoev, tpi
  USE control_flags,   ONLY : io_level, gamma_only, use_para_diag, twfcollect

  IMPLICIT NONE
  include 'mpif.h'
  !------------------------------------------------------------------------
  character(len=3) :: dummyc(1000)
  real(dp) :: dummyr1, dummyr2, dummyr3, arg
  integer :: dummyarray1(1000)
  real(dp) :: dummyarray2(3,1000)
  real(dp) :: dummyarray3(6)
  real(dp) :: dummyarray4(3,3)
  real(dp) :: dummyarray5(1000)
  !------------------------------------------------------------------------
  CHARACTER(LEN=7) :: image_label
  CHARACTER(LEN=66) :: density_file,magnetization_file
  CHARACTER(LEN=27) :: line
  CHARACTER(LEN=27) :: title
  CHARACTER(LEN=6), EXTERNAL :: int_to_char
  integer :: istep_, nt, na, ng, my_image_label
  integer :: ik, is, ibnd, i, ii, plot_num, dummy1, dummy2, dummy3, dummy4, dummy5, dummy6, dummy7, dummy8, dummy9
  logical :: file_exists, exst_label, exst_rho, exst_magn
  integer, external :: find_free_unit
  real(dp), allocatable :: raux1(:), raux1_magn(:), raux1l(:), raux1l_magn(:)
  complex(dp), allocatable :: gauxl(:)
! initialize the frozen fragments not evolved in time by reading the density file .pp
!!!!! FROZEN DENSITY
  if (fde_nspin == 2) then
     call fde_fake_nspin(.true.)
     if (linterlock) then
        call create_scf_type_large(rho_frozen_tmp_large,.true.)
        call create_scf_type_large(rho_tmp_large,.true.)
     else
        call create_scf_type(rho_tmp,.true.)
        call create_scf_type(rho_frozen_tmp,.true.)
     endif
     call fde_fake_nspin(.false.)
  else
     if (linterlock) then
        call create_scf_type_large(rho_frozen_tmp_large,.true.)
        call create_scf_type_large(rho_tmp_large,.true.)
     else
        call create_scf_type(rho_tmp,.true.)
        call create_scf_type(rho_frozen_tmp,.true.)
     endif
  endif
  if (ionode) then
          allocate(raux1(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
          if (linterlock) allocate(raux1l(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
          raux1=0.0d0
          if (linterlock) raux1l=0.d0
          if (fde_nspin == 2) then
             allocate(raux1_magn(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
             if (linterlock) allocate(raux1l_magn(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
             raux1_magn=0.0d0
             if (linterlock) raux1l_magn=0.0d0
          endif
   endif
  if (linterlock) then
      rho_frozen_tmp_large%of_r=0.0d0
   else
       rho_frozen_tmp%of_r=0.0d0
   endif
   inquire(file='image_labels.in', exist=exst_label)
!  write(stdout,*) exst_label
   if (exst_label) read(87,*) line ! skipping comment line
!  write(stdout,*) line
   
   ii=0.0d0 
   
   do ii=1,ifrozen
!       write(stdout,*) ifrozen
       if (ionode) then
           !
           ! 
           if (exst_label) then
               read(87,*) my_image_label
               image_label = '_' // int_to_char(1)
           else
               write(stdout,*) 
               write(stdout,'(a)') "  warning! image_labels.in is not exist and all subsystems larger than image are frozen"
               write(stdout,*) 
               image_label = '_' // int_to_char(nimage+ii)
           endif
!           write(stdout,*) my_image_label 
!           write(stdout,'(a)') image_label
           !
           !
!           density_file=trim(prefix)//'_fde_rho_large'//trim(image_label)//'.pp'
!           write(stdout,'(2a)') "Reading density file ", density_file
!           inquire(file=density_file,exist=exst_rho)
!           write(stdout,*) exst_rho
!           if (.not.exst_rho) write(stdout,'(3a)')   "WARNING: density file ", &
!                                                         density_file, " NOT FOUND."
!           if(exst_rho) call plot_io (density_file, title, dummy1, dummy2, dummy3, &
!                             dummy4,  dummy5,  dummy6, nat_fde, ntyp, dummy9, &
!                             dummyarray3, dummyarray4, dummyr1, dummyr2, dummyr3, &
!                             plot_num, dummyc, dummyarray1, dummyarray5, &
!                             dummyarray2, raux1l, -1)
!           write(stdout,*) exst_rho
!                 write(stdout,*) ii
!                 write(stdout,*) dummyarray2
           !
           ! 
           density_file=trim(prefix)//'_fde_rho_large'//trim(image_label)//'.pp'
!          write(stdout,'(2a)') "Reading density file ", density_file
           inquire(file=density_file,exist=exst_rho)
           magnetization_file=trim(prefix)//'_fde_magn_large'//trim(image_label)//'.pp'
!                write(stdout,*) magnetization_file
           inquire(file=magnetization_file,exist=exst_magn)
                 !
                 !
!                 write(stdout,*) exst_magn
                 !
                 !
!                 if (ionode) then
!                  write(stdout,'(2a)') "Reading magnetization file ", magnetization_file
!                  if (.not.exst_magn) write(stdout,'(3a)')   "WARNING: magnetization file ", &
!                                                                  magnetization_file, " NOT FOUND."
!                 endif
                 !
                 !
                 ! if the frozen fragment is spin polarized, read the magnetization, otherwise it's zero
!                 write(stdout,*) exst_magn
           
!                 if(exst_magn.and.allocated(raux1l_magn)) call plot_io (magnetization_file, title, dummy1, dummy2, dummy3, &
!                               dummy4,  dummy5,  dummy6, nat_fde, ntyp, dummy9, &
!                               dummyarray3, dummyarray4, dummyr1, dummyr2, dummyr3, &
!                               plot_num, dummyc, dummyarray1, dummyarray5, &
!                               dummyarray2, raux1l_magn, -1)

!                 write(stdout,*) ii
!                 write(stdout,*) dummyarray2
!                 write(stdout,*) exst_magn
!                call errore('1','2',1) 
               if (nspin==2.and.exst_magn.and.allocated(raux1l_magn)) call plot_io (magnetization_file, & 
                                      title, dummy1, dummy2, dummy3, &
                                    dummy4,  dummy5,  dummy6, nat_fde, ntyp, dummy9, &
                                    dummyarray3, dummyarray4, dummyr1, dummyr2, dummyr3, &
                                    plot_num, dummyc, dummyarray1, dummyarray5, dummyarray2, & 
                                    raux1l_magn, -1)
!                 write(stdout,*) exst_magn
!                 if (exst_magn.and.allocated(raux1l_magn)) call plot_io (magnetization_file, title, dummy1, dummy2, dummy3, &
!                                    dummy4,  dummy5,  dummy6, nat_fde, ntyp, dummy9, &
!                                    dummyarray3, dummyarray4, dummyr1, dummyr2, dummyr3, &
!                                    plot_num, dummyc, dummyarray1, dummyarray5, dummyarray2, & 
!                                    raux1l_magn, -1)
!            endif
!      endif
!                 write(stdout,*) exst_magn
!                 write(stdout,*) linterlock
           !
           !
!           density_file=trim(prefix)//'_fde_rho_large'//trim(image_label)//'.pp'
!           write(stdout,'(2a)') "Reading density file ", density_file
!           inquire(file=density_file,exist=exst_rho)
!           write(stdout,*) exst_rho
!           if (.not.exst_rho) write(stdout,'(3a)')   "WARNING: density file ", &
!                                                         density_file, " NOT FOUND."
               if(exst_rho) call plot_io (density_file, title, dummy1, dummy2, dummy3, &
                             dummy4,  dummy5,  dummy6, nat_fde, ntyp, dummy9, &
                             dummyarray3, dummyarray4, dummyr1, dummyr2, dummyr3, &
                             plot_num, dummyc, dummyarray1, dummyarray5, &
                             dummyarray2, raux1l, -1)
           !
           !
        endif
        !
        !
        
!        write(stdout,*) '818'
!        call flush_unit(stdout)
        if (linterlock) then
             call grid_scatter_large(raux1l,rho_tmp_large%of_r(:,1))
                 if (fde_nspin == 2.and.exst_magn) then
!                  if (ionode) raux1l_mag(f2l(:))=raux1_magn(:)
                   call flush_unit(stdout)
                   call grid_scatter_large(raux1l_magn,rho_tmp_large%of_r(:,2))
                   write(stdout,*) raux1l_magn
                   call flush_unit(stdout)
                   rho_tmp_large%of_r(:,2) = rho_tmp_large%of_r(:,1)-rho_tmp_large%of_r(:,2)
                 else           
                   rho_tmp_large%of_r(:,2)=0.5d0*rho_tmp_large%of_r(:,1)
                   rho_tmp_large%of_r(:,1)=rho_tmp_large%of_r(:,2)
                 endif
          else ! linterlock
            call grid_scatter(raux1,rho_tmp%of_r(:,1))
            if (fde_nspin == 2) then 
                   if (exst_magn) then
                      call grid_scatter(raux1_magn,rho_tmp%of_r(:,2))
 ! recover the beta density from the magnetization, defined as rho(:,1) - rho(:,2)
                     rho_tmp%of_r(:,2) = rho_tmp%of_r(:,1)-rho_tmp%of_r(:,2)  ! replace later by daxpy
                   else
                   ! redistribute density of closed shell fragment into even alpha and beta parts like done in update_rho_fde
                     rho_tmp%of_r(:,2)=0.5d0*rho_tmp%of_r(:,1)
                     rho_tmp%of_r(:,1)=rho_tmp%of_r(:,2)
                   endif
             endif
         endif ! linterlock
             if (ii==1) then
                 call mp_bcast(nat_fde,ionode_id,intra_image_comm)
                 deallocate(tau_fde,ityp_fde)
                 allocate(tau_fde(3,nat_fde))
                 allocate(ityp_fde(nat_fde))
                 if (ionode) then
                     tau_fde=dummyarray2(:,1:nat_fde)
                     ityp_fde=dummyarray1(1:nat_fde)
                 ! recalculate the structure factor for the frozen fragment
                 endif
                 call mp_bcast(tau_fde,ionode_id,intra_image_comm)
                 call mp_bcast(ityp_fde,ionode_id,intra_image_comm)
                 
                 if (linterlock) then
! or should I call struct_fact with strf_fde_large and new nat_fde and tau_fde and then again calc_f2l?
                    strf_fde_large(:,:)=(0.d0,0.d0)
                    do nt = 1, ntyp
                       do na = 1, nat_fde
                           if (ityp_fde (na) .eq.nt) then
                             do ng = 1, ngml
                                arg = (gl (1, ng) * tau_fde (1, na) + gl (2, ng) * tau_fde (2, na) &
                                    + gl (3, ng) * tau_fde (3, na) ) * tpi
                                strf_fde_large (ng, nt) = strf_fde_large (ng, nt) + CMPLX(cos (arg), -sin (arg),kind=DP)
                              enddo
                            endif
                        enddo
                     enddo
!                   CALL struc_fact( nat_fde, tau_fde, ntyp, ityp_fde, ngml, gl, bgl, &
!                              dfftl%nr1, dfftl%nr2, dfftl%nr3, strf_fde_large, eigts1l, eigts2l, eigts3l )
!                   call calc_f2l(f2l, dfftp, dfftl, fde_cell_shift, fde_cell_offset)
                   call setlocal_fde_large(vltot_large, strf_fde_large)
                   call setlocal
                   call copy_pot_l2f(vltot_large, vltot)
                   call set_vrs (vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid)
!                   CALL struc_fact( nat_fde, tau_fde, ntyp, ityp_fde, ngml, gl, bgl, &
!                              dfftl%nr1, dfftl%nr2, dfftl%nr3, strf_fde_large, eigts1l, eigts2l, eigts3l )
!                   call calc_f2l(f2l, dfftp, dfftl, fde_cell_shift, fde_cell_offset)
                  else ! linterlock
                    strf_fde(:,:)=(0.d0,0.d0)
                    do nt = 1, ntyp
                       do na = 1, nat_fde
                           if (ityp_fde (na) .eq.nt) then
                             do ng = 1, ngm
                                arg = (g (1, ng) * tau_fde (1, na) + g (2, ng) * tau_fde (2, na) &
                                    + g (3, ng) * tau_fde (3, na) ) * tpi
                                strf_fde (ng, nt) = strf_fde (ng, nt) + CMPLX(cos (arg), -sin (arg),kind=DP)
                              enddo
                            endif
                        enddo
                     enddo
                  do nt=1,ntyp_frozen
                      call c_grid_gather_sum_scatter(strf_fde(:,nt))
                  enddo
             endif ! linterlock

             endif ! ii=1
       if (linterlock) then
                rho_frozen_tmp_large%of_r=rho_frozen_tmp_large%of_r+rho_tmp_large%of_r
       else
                rho_frozen_tmp%of_r=rho_frozen_tmp%of_r+rho_tmp%of_r
       endif
    enddo
    continue
    if (linterlock) then
      allocate(gauxl(dfftl%nnr))
      do is=1,fde_nspin
         gauxl(:)=cmplx(rho_frozen_tmp_large%of_r(:,is),0.d0,kind=dp)
         call fwfft('Custom',gauxl,dfftl)
         rho_frozen_tmp_large%of_g(1:ngml,is)=gauxl(nll(1:ngml))
      enddo
    else
      do is=1,fde_nspin
          ! using psic as work array
          psic(:)=rho_frozen_tmp%of_r(:,is)
          call fwfft('Dense',psic,dfftp)
          rho_frozen_tmp%of_g(:,is) = psic(nl(:))
      enddo
    endif
    if (linterlock)  call setlocal_fde_large(vltot_large,strf_fde_large)
    call setlocal()  ! needs to be called also in linterlock case (i think, judging by hinit0)
    if (linterlock) call copy_pot_l2f(vltot_large,vltot)
   if (allocated(raux1)) deallocate(raux1)
   if (fde_nspin == 2.and.allocated(raux1_magn)) deallocate(raux1_magn)
   if (linterlock) then
     if (allocated(raux1l)) deallocate(raux1l)
     if (allocated(gauxl)) deallocate(gauxl)
     if (fde_nspin == 2.and.allocated(raux1l_magn)) deallocate(raux1l_magn)
     call destroy_scf_type_large(rho_tmp_large)
   else
    call destroy_scf_type(rho_tmp)
   endif
end subroutine tddft_frozen_setup
