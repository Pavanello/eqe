!
! Copyright (C) 2001-2014 Quantum-ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!

! TODO: eventually, separate input files for FDE, and bcast to images,
! not to comm_world

!-----------------------------------------------------------------------
SUBROUTINE tddft_readin(filin)
  !-----------------------------------------------------------------------
  !
  ! ... Read in the tddft input file. The input file consists of a
  ! ... single namelist &inputtddft. See doc/user-manual.pdf for the
  ! ... list of input keywords.
  !
  USE tddft_module
  USE io_files,         ONLY : prefix, tmp_dir  
  USE io_global,        ONLY : stdout, stdin, ionode
  USE constants,        ONLY : bohr_radius_angs, au_sec, pi
  USE input_parameters, ONLY : max_seconds, wf_collect
  USE control_flags,          ONLY : twfcollect
  use mp_images, only : me_image, inter_fragment_comm, intra_image_comm, nimage,my_image_id
  use mp, only: mp_sum, mp_gather, mp_bcast, mp_comm_split
  USE fde
  use fde_routines
  use command_line_options, only : fancy_parallel_
  ! -- parameters --------------------------------------------------------
  implicit none
  character(*), intent(in) :: filin
  ! -- local variables ---------------------------------------------------
  integer :: ios 
  integer :: ierror
  integer :: dummy
  character(len=256), external :: trimcheck
  character(len=80) :: verbosity, tdpulse, tefield, integrator, predictor
  namelist /inputtddft/ job, prefix, tmp_dir, conv_threshold, verbosity,   &
                        dt, e_strength, e_direction, nstep, nupdate_Dnm,   &
                        l_circular_dichroism, l_tddft_restart, max_seconds,&
                        l_quadrupole, l_current, l_current_printfile,      &
                        i_current_step, l_divergence, current_screen,      &
                        molecule, ehrenfest, tdpulse, t0, tau0, omega0,    &
                        sigma0, alpha, ttend,                              &
                        tefield, emaxpos, eopreg, wf_collect, integrator,  &
                        predictor, update,                                 &
                        pc_conv_threshold, coupled, icoupled, print_bands, &
                        calc_ener, icalc_ener,                             &
                        late_coupling, ilate_coupling, frozen, ifrozen,    &
                        q_direction1, q_direction2,                        &
                        istep_twoph, l_twoph, e_direction_twoph,           &
                        thkns, p_position

                        

     open(unit=stdin, file=filin, form='formatted', status='old')
  
  ! define input defult values
  call get_env( 'ESPRESSO_TMPDIR', tmp_dir )
  if (trim(tmp_dir) == ' ') tmp_dir = './scratch/'
  tmp_dir = trimcheck(tmp_dir)
  job          = ''
  prefix       = 'pwscf'
  tmp_dir      = './scratch/'    
  verbosity    = 'low'
  dt           = 1.d0                      ! time step (default: 2 attosecond)
  e_strength   = 0.01d0                    ! impulse electric field strength (default: 0.01/Ang)
  e_direction  = 1                         ! impulse electric field direction: 1-x 2-y 3-z
  e_direction_twoph  = 1                         ! impulse electric field direction: 1-x 2-y 3-z
  q_direction1 = 1                         ! impulse electric field direction: 1-x 2-y 3-z
  q_direction2 = 2                         ! impulse electric field direction: 1-x 2-y 3-z
  conv_threshold = 1.0d-12                 ! convergence threshold    
  nstep        = 1000                      ! total time steps
  nupdate_Dnm  = 1                         ! update USPP Dnm every step
  l_circular_dichroism = .false.
  l_tddft_restart      = .false.
  l_quadrupole         = .false.
  l_twoph              = .false.
  l_current            = .false.
  l_divergence         = .false.
  l_current_printfile  = .false.
  i_current_step     = 10000000
  current_screen     = 0.5d0
  istep_twoph        = 0
  istep              = 0
  e_direction_twoph  = 2
  max_seconds  =  1.d7
  molecule     = .true.
  ehrenfest    = .false.
  tdpulse      = 'cosine'
  t0           = 0.0d0                    ! 0.2 femtoseconds
  tau0         = 200.0d0                    ! 0.2 femtoseconds
  omega0       = 18.0d0                     ! in femtoseconds^-1
  sigma0       = 100.0d0 
  alpha       = 1.0d0                     ! in femtoseconds^-1
  ttend         = 4000.0d0                    ! end time of pulse, 4 femtoseconds
  tefield       = 'wfshift'
  emaxpos       = 0.9d0
  eopreg        = 0.1d0
  eamp          = e_strength
  wf_collect    = .false.
! for the integrators
  integrator = 'cn2'
  cn = .false.
  rk = .false.
  em = .false.
  etrs = .false.
  magnus = .false.
  update = .true.
  coupled = .true.
  frozen = .false. ! says there are fragments that are to be frozen at t0
  ifrozen = 0 ! how many frozen fragments are there
  late_coupling = .false.
  chain = .false.
  nlink = 1
  my_link = 1
! default value: all fragments are coupled with each other, belong all to color=1
  icoupled = 1
  ilate_coupling=0
  pmax = 1
  rkmax = 2
  pc_conv_threshold = 1.0d-8
  print_bands = .false.
  calc_ener = .true.
  calc_ener_in_v_of_rho = .false.
! new value, icalc_ener every 100 steps
  icalc_ener = 100

  
  ! read input    
  read(stdin, inputtddft, err = 200, iostat = ios )

  ! check input
  if (max_seconds < 0.1d0) call errore ('tddft_readin', ' wrong max_seconds', 1)
200 call errore('tddft_readin', 'reading inputtddft namelist', abs(ios))

  select case (trim(verbosity))
     case('low')
       iverbosity = 1
     case('medium')
       iverbosity = 11
     case('high')
       iverbosity = 21
     case default
       call errore('tdddft_readin', 'verbosity can be ''low'', ''medium'' or ''high''', 1)
  end select
   
   select case(trim(tdpulse))
      case('uniform_efield')
          itdpulse=1
          write(stdout,*)'Applying uniform electric field'
      case('uniform_efg')
          itdpulse=10
          write(stdout,*)'Applying uniform electric field gradient'
      case('cosine')
          itdpulse=2
          write(stdout,*)'Applying damped td electric field'
      case('gaussian')
          itdpulse=3
          write(stdout,*)'Applying damped td electric field'
      case('cosine2')
          itdpulse=4
          ttend=nstep*dt
      case('cosine3')
          itdpulse=5
      case('cosine4')
          itdpulse=6
          write(stdout,*)'Applying undamped td electric field'
      case default
       call errore('tdddft_readin', 'unknown tdpulse', 1)
  end select
   select case(trim(tefield))
      case('external')
          write(stdout,*)'Applying field through external potential'
          itefield=1
      case('wfshift')
          write(stdout,*)'Applying field through a shift of wavefunction'
          itefield=2
      case default
       call errore('tdddft_readin', 'itefield can be ''external'' or ''wfshift''', 1)
  end select
   select case(trim(integrator))
      case('cnfull')
          write(stdout,*)'Integrating using Self-Consistent Cranck-Nicolson'
          cn = .true.
          pmax = 100
      case('cn4')
          write(stdout,*)'Integrating using Cranck-Nicolson 4th order'
          cn = .true.
          pmax = 4
      case('cn3')
          write(stdout,*)'Integrating using Cranck-Nicolson 3rd order'
          cn = .true.
          pmax = 3
      case('cn2')
          write(stdout,*)'Integrating using Cranck-Nicolson 2nd order'
          cn = .true.
          pmax = 2
      case('cn ')
          write(stdout,*)'Integrating using Cranck-Nicolson first order'
          cn = .true.
          pmax = 1
      case('rk4')
          write(stdout,*)'Integrating using 4th order Runge-Kutta'
          rk = .true.
          rkmax = 4
      case('rk2')
          write(stdout,*)'Integrating using 2nd order Runge-Kutta'
          rk = .true.
          rkmax = 2
      case('em')
          write(stdout,*)'Integrating using Exponentional Middle Point'
          em = .true.
      case('etrs')
          write(stdout,*)'Integrating using Exponentional Time Reversal Symmetry'
          etrs = .true.
      case('magnus')
          write(stdout,*)'Integrating using 4th order Magnus Expansion'
          magnus = .true.
      case default
          write(stdout,*)'Default: Integrating using 2nd order Cranck-Nicolson'
          cn = .true.
          pmax = 2
  end select
  
  twfcollect = wf_collect
  
  if (.not.coupled.and.update) then
     write(stdout,*)'update and uncoupled are not combinable, update set to .false.'
     update = .false.
  endif
  
  if (late_coupling) write(stdout,*)'Caution, experimental option!!! not thoroughly tested, results may be unphysical'
   
   if (chain) then
        write(stdout,*)'Caution, experimental option, thoroughly bugged'
   endif
           

  ! convert to atomic units
  e_strength = e_strength * bohr_radius_angs ! change from Ryd/Ang to Ryd/Bohr
  dt = dt * 1.d-18 / (2.d0*au_sec)           ! change from femto-second to a.u. (Rydberg unit)
  if (itdpulse==2) then
     t0 = t0 * 1.d-18 / (2.d0*au_sec)           ! change from 0.2 femto-second to a.u. (Rydberg unit)
     tau0 = tau0 * 1.d-18 / (2.d0*au_sec)           ! change from 0.2 femto-second to a.u. (Rydberg unit)
     omega0 = omega0 / 1.d-15 * (2.d0*au_sec)           ! change 18 fs-1 to a.u.
      ttend = ttend * 1.d-18 / (2.d0*au_sec)                   ! end time of pulse, 0.4 femtoseconds
   elseif (itdpulse==3) then
      t0 = t0* 1.d-18 / (2.d0*au_sec) 
   elseif (itdpulse>=4) then
      t0 = t0* 1.d-18 / (2.d0*au_sec) 
!      sigma0= sigma0* 1.d-18 / (2.d0*au_sec) 
       omega0 = omega0 * 0.036749309d0 * 2.0d0! omega0 given in eV and changed to au
  endif
 
  eamp = e_strength
!
!*******************************************************************************
if (do_fde) then
!*******************************************************************************
! A different inter_fragment_comm for TDDFT contains only the coupled subsystems.
if (coupled) then
   color=icoupled ! icoupled=1 unless specified otherwise in input file
   if (icoupled>999) write(stdout,*)'Caution, may coincide with the color of an uncoupled fragment'
else
   color=my_image_id+1000 ! in uncoupled version, every subsystem has its own color. Adding 1000 to ensure this doesn't by chance coincide with any icoupled
endif

if (late_coupling) then
    coupled=.false.
    if (ilate_coupling==0) call errore('tdddft_readin', 'late coupling needs to know when to kick in', 1)
    write(stdout,*)'will introduce coupling after ',ilate_coupling*dt/1.d-18*(2.d0*au_sec),' attoseconds'
    color=icoupled ! we want them to be together in one communicator once the coupling kicks in
endif
     
! a check for correct assigning of the color. Note that it is only meaningful on
! ionodes
allocate(colors(nimage))
colors = 0.0d0
if (ionode) then
   call mp_comm_split(inter_fragment_comm,color,me_image,inter_coupled_comm)
   if (.not.coupled) color=color-1000 ! substracting the 100 for the sum
   call mp_gather(color,colors,0,inter_fragment_comm)
   call mp_bcast(colors,0,inter_fragment_comm)
endif
call mp_bcast(colors,0,intra_image_comm)
write(stdout,*)'colors',colors
! find out the number of coupled clusters:
max_color=maxval(colors)
! create an array for storing the energy values:
allocate(energies_of_all(6,nimage))
energies_of_all=0.0d0

if (.not.coupled.and.fancy_parallel_) call errore('tddft_input','uncoupled calculations require -nfp flag',1)
if (frozen.and.fancy_parallel_) call errore('tddft_input','frozen calculations require -nfp flag',1)
if (frozen.and.calc_ener) then
    write(stdout,'(a)')'Frozen calculations not combinable with energy calculations (you may accept this as a challenge)' 
   calc_ener = .false.
endif


! a check that the splitting is done correctly. On the ionodes of the coupled
! subsystems, it should give the number of coupled subsystems. On the ionodes of
! the uncoupled subsystems, it should give the number of uncoupled subsystems
! times 2. On not ionodes, it should just give "1" or "2" since they are not
! part of the sum
!*******************************************************************************
endif ! if (do_fde) then
!*******************************************************************************


#ifdef __MPI
  ! broadcast input variables  
  call tddft_bcast_input
#endif

END SUBROUTINE tddft_readin


#ifdef __MPI
!-----------------------------------------------------------------------
SUBROUTINE tddft_bcast_input
  !-----------------------------------------------------------------------
  !
  ! ... Broadcast input data to all processors 
  !
  USE mp_world,         ONLY : world_comm
  USE mp_images,        ONLY : intra_image_comm
  USE mp,               ONLY : mp_bcast
  USE io_files,         ONLY : prefix, tmp_dir
  USE input_parameters, ONLY : max_seconds
  USE tddft_module
  USE fde,               ONLY : do_fde

  implicit none
  integer, parameter :: root = 0    

  if (do_fde) then
  call mp_bcast(job, root, intra_image_comm)
  call mp_bcast(prefix, root, intra_image_comm)
  call mp_bcast(tmp_dir, root, intra_image_comm)
  call mp_bcast(dt, root, intra_image_comm)
  call mp_bcast(e_strength, root, intra_image_comm)
  call mp_bcast(e_direction, root, intra_image_comm)
  call mp_bcast(q_direction1, root, intra_image_comm)
  call mp_bcast(q_direction2, root, intra_image_comm)
  call mp_bcast(conv_threshold, root, intra_image_comm)
  call mp_bcast(pc_conv_threshold, root, intra_image_comm)
  call mp_bcast(nstep, root, intra_image_comm)
  call mp_bcast(nupdate_Dnm , root, intra_image_comm)
  call mp_bcast(l_circular_dichroism, root, intra_image_comm)
  call mp_bcast(l_tddft_restart, root, intra_image_comm)
  call mp_bcast(l_quadrupole, root, intra_image_comm)
  call mp_bcast(l_twoph, root, intra_image_comm)
  call mp_bcast(l_current, root, intra_image_comm)
  call mp_bcast(l_divergence, root, intra_image_comm)
  call mp_bcast(l_current_printfile, root, intra_image_comm)
  call mp_bcast(current_screen, root, intra_image_comm)
  call mp_bcast(i_current_step, root, intra_image_comm)
  call mp_bcast(istep_twoph, root, intra_image_comm)
  call mp_bcast(istep, root, intra_image_comm)
  call mp_bcast(e_direction_twoph, root, intra_image_comm)
  call mp_bcast(iverbosity, root, intra_image_comm)
  call mp_bcast(max_seconds, root, intra_image_comm)
  call mp_bcast(molecule, root, intra_image_comm)
  call mp_bcast(ehrenfest, root, intra_image_comm)
  call mp_bcast(thkns, root, intra_image_comm)
  call mp_bcast(p_position, root, intra_image_comm)
  else
  call mp_bcast(job, root, world_comm)
  call mp_bcast(prefix, root, world_comm)
  call mp_bcast(tmp_dir, root, world_comm)
  call mp_bcast(dt, root, world_comm)
  call mp_bcast(e_strength, root, world_comm)
  call mp_bcast(e_direction, root, world_comm)
  call mp_bcast(q_direction1, root, world_comm)
  call mp_bcast(q_direction2, root, world_comm)
  call mp_bcast(conv_threshold, root, world_comm)
  call mp_bcast(pc_conv_threshold, root, world_comm)
  call mp_bcast(nstep, root, world_comm)
  call mp_bcast(nupdate_Dnm , root, world_comm)
  call mp_bcast(l_circular_dichroism, root, world_comm)
  call mp_bcast(l_tddft_restart, root, world_comm)
  call mp_bcast(l_quadrupole, root, world_comm)
  call mp_bcast(l_twoph, root, world_comm)
  call mp_bcast(l_current, root, world_comm)
  call mp_bcast(l_divergence, root, world_comm)
  call mp_bcast(l_current_printfile, root, world_comm)
  call mp_bcast(current_screen, root, world_comm)
  call mp_bcast(i_current_step, root, world_comm)
  call mp_bcast(istep_twoph, root, world_comm)
  call mp_bcast(istep, root, world_comm)
  call mp_bcast(e_direction_twoph, root, world_comm)
  call mp_bcast(iverbosity, root, world_comm)
  call mp_bcast(max_seconds, root, world_comm)
  call mp_bcast(molecule, root, world_comm)
  call mp_bcast(ehrenfest, root, world_comm)
  call mp_bcast(thkns, root, intra_image_comm)
  call mp_bcast(p_position, root, intra_image_comm)
  endif

END SUBROUTINE tddft_bcast_input
#endif
  

!-----------------------------------------------------------------------
SUBROUTINE tddft_allocate
  !-----------------------------------------------------------------------
  !
  ! ... Allocate memory for TDDFT
  !
  USE tddft_module
  USE klist,         ONLY : nkstot
  USE wvfct,         ONLY : btype, nbndx
  USE tddft_module

  implicit none

  ! needed by sum_band
  allocate(btype(nbndx,nkstot))
  btype = 1
    
END SUBROUTINE tddft_allocate


!-----------------------------------------------------------------------
SUBROUTINE tddft_summary
  !-----------------------------------------------------------------------
  !
  ! ... Print a short summary of the calculation
  !
  USE io_global,        ONLY : stdout
  USE lsda_mod,         ONLY : nspin
  USE wvfct,            ONLY : nbnd
  USE tddft_module
  implicit none
  integer :: is
    
  write(stdout,*)

  write(stdout,'(5X,''Calculation type      : '',A12)') job
  if (molecule) then
     write(stdout,'(5X,''System is             : molecule'')')
  else
     write(stdout,'(5X,''System is             : crystal'')')
  endif
  if (ehrenfest) write(stdout,'(5X,''Ehrenest dynamics'')')
  write(stdout,'(5X,''Number or steps       : '',I12)') nstep
  write(stdout,'(5X,''Time step             : '',F12.4,'' rydberg_atomic_time'')') dt
  !write(stdout,'(5X,''Electric field dir.   : '',I12,'' (1=x,2=y,3=z)'')') e_direction
  !write(stdout,'(5X,''Electric field impulse: '',F12.4,'' bohrradius^-1'')') e_strength

  write(stdout,*)

  if (tfixed_occ) then
     write(stdout,'(5X,''Occupations from input:'')')
     do is = 1, nspin
       write(stdout,'(5X,''ispin='',I1,'': '')',advance='no') is
       write(stdout,'(10(F4.2,2X))') f_inp(1:nbnd,is)
     enddo
    write(stdout,*)
  endif
     
  call flush_unit( stdout )

END SUBROUTINE tddft_summary
  
  

!-----------------------------------------------------------------------
SUBROUTINE tddft_openfil
  !
  ! ... Open files needed for TDDFT
  !
  USE tddft_module   
  USE wvfct,            ONLY : nbnd, npwx
  USE ldaU,             ONLY : lda_plus_U, nwfcU
  USE io_files,         ONLY : iunhub, iunwfc, &
                               nwordwfcU, nwordwfc,  seqopn, iunigk
  USE noncollin_module, ONLY : npol
  USE buffers,          ONLY : open_buffer
  USE control_flags,    ONLY : io_level    
  use ruku,             ONLY : iunruku1, iunruku2, iunruku3, iunruku4
  IMPLICIT NONE  
  character*1, parameter :: dir(3) = (/'x', 'y', 'z'/)
  logical :: exst, opnd

  !
  ! ... nwordwfc is the record length (IN REAL WORDS)
  ! ... for the direct-access file containing wavefunctions
  ! ... io_level > 0 : open a file; io_level <= 0 : open a buffer
  !
  nwordwfc = nbnd*npwx*npol
  CALL open_buffer( iunwfc, 'wfc', nwordwfc, io_level, exst )

  ! do not overwrite wfc
  nwordwfc = nbnd*npwx*npol
  CALL open_buffer( iunevcn, 'wfc'//dir(e_direction), nwordwfc, io_level, exst )

  if (rk) then
  ! for ruku
    nwordtdwfc = nbnd*npwx*npol
    CALL open_buffer( iunruku1, 'ruku1', nwordtdwfc, io_level, exst )
    CALL open_buffer( iunruku2, 'ruku2', nwordtdwfc, io_level, exst )
    CALL open_buffer( iunruku3, 'ruku3', nwordtdwfc, io_level, exst )
    CALL open_buffer( iunruku4, 'ruku4', nwordtdwfc, io_level, exst )
  endif

  ! for restart
  nwordtdwfc = nbnd*npwx*npol
  CALL open_buffer( iuntdwfc, 'tmp'//dir(e_direction), nwordtdwfc*2, io_level, exst )

  ! For atomic wavefunctions
  INQUIRE( UNIT = iunigk, OPENED = opnd )
  IF(.NOT. opnd) CALL seqopn( iunigk, 'igk', 'UNFORMATTED', exst )

  ! ... Needed for LDA+U
  ! ... iunhub contains the (orthogonalized) atomic wfcs * S
  nwordwfcU = npwx*nwfcU*npol
  IF ( lda_plus_u ) &
     CALL open_buffer( iunhub, 'hub', nwordwfcU, io_level, exst )

END SUBROUTINE tddft_openfil


!-----------------------------------------------------------------------
SUBROUTINE tddft_closefil
  !-----------------------------------------------------------------------
  !
  ! ... Close files opened by TDDFT
  !
  USE ldaU,             ONLY : lda_plus_U  
  USE io_files,         ONLY : iunhub, iunwfc
  USE buffers,          ONLY : close_buffer

  call close_buffer( iunwfc, 'keep' )
  call close_buffer( iunevcn, 'keep' )
  if ( lda_plus_u ) call close_buffer ( iunhub, status = 'keep' )

END SUBROUTINE tddft_closefil



!-----------------------------------------------------------------------
SUBROUTINE print_clock_tddft
  !-----------------------------------------------------------------------
  !
  ! ... Print clocks
  !
  USE io_global,  ONLY : stdout
  IMPLICIT NONE

  write(stdout,*) '    Initialization:'
  call print_clock ('tddft_setup')
  call print_clock ('gksort')
  write(stdout,*)
  write(stdout,*) '    SCF routines'
  call print_clock ('greenf')
  call print_clock ('ch_psi')
  call print_clock ('h_psi')
  call print_clock ('s_psi')
  call print_clock ('v_of_rho')
  call print_clock ('v_xc')
  call print_clock ('v_h')
  call print_clock ('fde_nonadd')
  call print_clock ('set_vrs')
  call print_clock ('init_us_2')
  call print_clock ('update_rho')
  write(stdout,*)
  write(stdout,*) '    Real time evolution'
  call print_clock ('updateH')
  call print_clock ('crank_nicolson')
  call print_clock ('runge_kutta')
  call print_clock ('dipole')
  call print_clock ('quadrupole')
  call print_clock ('circular')
  write(stdout,*)
  write(stdout,*) '    General routines'
  call print_clock ('calbec')
  call print_clock ('fft')
  call print_clock ('ffts')
  call print_clock ('fftw')
  call print_clock ('cinterpolate')
  call print_clock ('davcio')
  call print_clock ('write_rec')
  write(stdout,*)

#ifdef __MPI
  write(stdout,*) '    Parallel routines'
  call print_clock ('reduce')  
  call print_clock( 'fft_scatter' )
  call print_clock( 'ALLTOALL' )
  write(stdout,*)
#endif
  call print_clock ('TDFFT') 

END SUBROUTINE print_clock_tddft



!-----------------------------------------------------------------------
SUBROUTINE tddft_memory_report
  !-----------------------------------------------------------------------
  !
  ! ... Print estimated memory usage
  !
  USE io_global,                 ONLY : stdout
  USE noncollin_module,          ONLY : npol
  USE uspp,                      ONLY : nkb
  USE fft_base,                  ONLY : dffts
  USE pwcom
  IMPLICIT NONE
  integer, parameter :: Mb=1024*1024, complex_size=16, real_size=8

  ! the conversions to double prevent integer overflow in very large run
  write(stdout,'(5x,"Largest allocated arrays",5x,"est. size (Mb)",5x,"dimensions")')

  write(stdout,'(8x,"KS wavefunctions at k     ",f10.2," Mb",5x,"(",i8,",",i5,")")') &
     complex_size*nbnd*npol*DBLE(npwx)/Mb, npwx*npol,nbnd

  write(stdout,'(8x,"First-order wavefunctions ",f10.2," Mb",5x,"(",i8,",",i5,",",i3")")') &
     complex_size*nbnd*npol*DBLE(npwx)*10/Mb, npwx*npol,nbnd,10

  write(stdout,'(8x,"Charge/spin density       ",f10.2," Mb",5x,"(",i8,",",i5,")")') &
     real_size*dble(dffts%nnr)*nspin/Mb, dffts%nnr, nspin
  
  write(stdout,'(8x,"NL pseudopotentials       ",f10.2," Mb",5x,"(",i8,",",i5,")")') &
     complex_size*nkb*DBLE(npwx)/Mb, npwx, nkb
  write(stdout,*)

END SUBROUTINE tddft_memory_report


!-----------------------------------------------------------------------
SUBROUTINE tddft_read_cards
  !-----------------------------------------------------------------------
  !
  ! ... Read in extra cards
  !
  USE mp_world,         ONLY : world_comm
  USE mp_images,     ONLY : intra_image_comm, my_image_id,me_image
  USE mp,               ONLY : mp_bcast
  USE io_global,        ONLY : ionode, stdout, stdin
  USE fixed_occ,        ONLY : f_inp, tfixed_occ
  USE lsda_mod,         ONLY : nspin
  USE wvfct,            ONLY : nbnd
  USE parser,           ONLY : read_line
  USE tddft_module
  USE fde,               ONLY : do_fde
  implicit none
  character(len=256)         :: input_line
  character(len=80)          :: card
  character(len=1), external :: capital
  logical                    :: tend
  integer                    :: i, is
  integer, parameter         :: root = 0
  
  allocate(f_inp(nbnd,nspin))
  tfixed_occ = .false.
 
  if (.not. ionode) goto 400
 
  read_cards: do
    call read_line(input_line, end_of_file=tend, ionode_only=.true.)
    if (tend) exit read_cards
    if (input_line == ' ' .or. input_line(1:1) == '#' .or. input_line(1:1) == '!' ) &
      cycle read_cards
    read(input_line,*) card
    do i = 1, len_trim(input_line)
       input_line(i: ) = capital(input_line(i:i))
    enddo
  
    if (trim(card) == 'OCCUPATIONS') then
       tfixed_occ = .true.
       do is = 1, nspin
         read(stdin,*) f_inp(1:nbnd,is)
       enddo
    elseif (trim(card) == 'CHAIN') then
        chain=.true.
        read(stdin,*)nlink
        read(stdin,*)my_link
        allocate(my_chain(nlink))
        read(stdin,*)my_chain(1:nlink)
    else
       write(stdout,*) 'Warning: card '//trim(card)//' ignored'
    endif
    
  enddo read_cards
400 continue
   
  if (do_fde) then
     call mp_bcast(tfixed_occ, root, intra_image_comm)
     call mp_bcast(f_inp, root, intra_image_comm)
     call mp_bcast(nlink,root,intra_image_comm)
     call mp_bcast(my_link,root,intra_image_comm)
     call mp_bcast(chain,root,intra_image_comm)
     if (chain) then
          if (.not.ionode) allocate(my_chain(nlink))
          call mp_bcast(my_chain,root,intra_image_comm)
     endif
  else
     call mp_bcast(tfixed_occ, root, world_comm)
     call mp_bcast(f_inp, root, world_comm)
  endif

  if (tfixed_occ) call weights
!  call weights
  
  return
END SUBROUTINE tddft_read_cards


SUBROUTINE TddftPrintOnFile(one,two)
!
! This should be in a "print" module. See if they have it in QE 6.1??
!
 USE tddft_module
 USE ions_base,                ONLY : ntyp => nsp, atm, zv, tau, ityp, nat
 USE gvect,                    ONLY : gcutm
 USE gvecs,                    ONLY : dual
 USE wvfct,                    ONLY : ecutwfc
 USE io_global,                ONLY : ionode, stdout
 USE io_files,                 ONLY : prefix
 USE cell_base,                ONLY : celldm, at, ibrav
 USE fft_base,                 ONLY : dfftp, grid_gather
 USE io_global,                ONLY : stdout, ionode


  real(dp), intent(in) :: one(dfftp%nnr)
  real(dp), intent(in) :: two(dfftp%nnr)


  real(dp),allocatable :: raux(:)
  character(20)         :: cfile
     !
     !
     ! Michele: I print \delta\rho to file to compare to div-J
     !
     if (l_current_printfile.and.mod(istep,i_current_step)==0) then
     if (ionode) then
        if (istep .lt. 10)                            write(cfile,'(I1)') istep
        if (istep .lt. 100  .and. istep .ge. 10)      write(cfile,'(I2)') istep
        if (istep .lt. 1000 .and. istep .ge. 100)     write(cfile,'(I3)') istep
        if (istep .lt. 10000 .and. istep .ge. 1000)   write(cfile,'(I4)') istep
        if (istep .lt. 100000 .and. istep .ge. 10000) write(cfile,'(I5)') istep
   
       allocate(raux(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
     endif
     !
     !
     call grid_gather(one-two,raux)
     !
     !
     if (ionode) then
       call plot_io (trim(prefix)//'_deltarho_'//trim(cfile)//'.pp', 'FDE drho', &
              dfftp%nr1x, dfftp%nr2x, dfftp%nr3x,                                &
              dfftp%nr1,  dfftp%nr2,  dfftp%nr3, nat, ntyp, ibrav, celldm, at,   &
              gcutm, dual, ecutwfc, 0, atm, ityp, zv, tau, raux, +1)
       deallocate(raux)
     endif !ionode
     endif !current
     !
     !
     !
END SUBROUTINE TddftPrintOnFile



