
!----------------------------------------------------------------------------
SUBROUTINE fde_energies_coupled ()
  !----------------------------------------------------------------------------
  !
  !  Subroutine to calculate the FDE total and non-additive energies
  !
  use ener,                     only : etot, ewld, ehart, vtxc, etxc, eband
  use mp,                       only : mp_sum, mp_barrier, mp_bcast, mp_gather
  use mp_images,                only : inter_fragment_comm, intra_image_comm, nimage,my_image_id,me_image
  use io_global,                only : ionode, ionode_id, stdout
  use mp_world,                 only : world_comm
  use mp_images, only : my_image_id
  use fde
  use fde_routines
  use input_parameters,         only : fde_xc_funct
  use tddft_module            
  use io_global, only : stdout

  implicit none
  real(dp) :: ekin0_nadd_tot,etxc0_nadd_tot,ekin_nadd_tot,etxc_nadd_tot,etot_sum_tot
  integer :: i
  
 
! part one, sum over the parts that are within the coupled system:
  etot_frag_mixed=etot_frag
  if (ionode) call mp_sum(etot_frag_mixed,inter_coupled_comm)
  call mp_bcast(etot_frag_mixed, ionode_id, intra_image_comm)
  etot_sum_tot = etot 
  if (ionode) call mp_sum(etot_sum_tot, inter_coupled_comm)
  call mp_bcast(etot_sum_tot, ionode_id, intra_image_comm)
  ekin0_nadd_tot=ekin0_nadd
  if (ionode) call mp_sum(ekin0_nadd_tot, inter_coupled_comm)
  call mp_bcast(ekin0_nadd_tot, ionode_id, intra_image_comm)
  etxc0_nadd_tot=etxc0_nadd
  if (ionode) call mp_sum(etxc0_nadd_tot, inter_coupled_comm)
  call mp_bcast(etxc0_nadd_tot, ionode_id, intra_image_comm)
  ekin_nadd_tot=ekin_nadd
  etxc_nadd_tot=etxc_nadd

! part two, add the parts of the subsystems which are not coupled to this one by using their t0 values
  do i=1,nimage
  if (colors(i).ne.color) then
      etot_frag_mixed=etot_frag_mixed+energies_of_all(1,i)
      etot_sum_tot = etot_sum_tot+energies_of_all(2,i)
      ekin0_nadd_tot = ekin0_nadd_tot+energies_of_all(3,i)
      etxc0_nadd_tot = etxc0_nadd_tot+energies_of_all(5,i)
   endif
  enddo
  
  ekin_nadd_tot = ekin_nadd_tot - ekin0_nadd_tot
  etxc_nadd_tot = etxc_nadd_tot - etxc0_nadd_tot

  edc_fde = -dble(nfragments-1+ifrozen)*(ewld)
  etot_fde= etot_sum_tot + edc_fde + ekin_nadd_tot + etxc_nadd_tot 

  ! =============================================
  ! DEBUG PRINT
  ! if (ionode) then
  !  if (iverbosity>10) then
  !    write(stdout,*) "etot_sum_tot = ", etot_sum_tot
  !    write(stdout,*) "edc_fde = ", edc_fde
  !    write(stdout,*) "ekin_nadd_tot = ", ekin_nadd_tot
  !    write(stdout,*) "etxc_nadd_tot = ", etxc_nadd_tot
  !  endif
  ! endif
  ! =============================================

! e_int 
!!!!!!!!!!!!!!!!!!!!!!!!!
  e_int = etot_fde - etot_frag_mixed

END SUBROUTINE fde_energies_coupled
