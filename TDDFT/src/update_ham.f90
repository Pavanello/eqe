!
! Copyright (C) 2001-2014 Quantum-ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!

!-----------------------------------------------------------------------
SUBROUTINE update_hamiltonian(istepp)
  !-----------------------------------------------------------------------
  !
  ! ... Update the hamiltonian
  !
  USE kinds,         ONLY : dp
  USE ldaU,          ONLY : lda_plus_U
  USE scf,           ONLY : rho, rho_core, rhog_core, vltot, v, kedtau, vrs, scf_type_copy, &
                            create_scf_type, destroy_scf_type
  use scf_large,     ONLY : create_scf_type_large => create_scf_type, scf_type_copy_large => scf_type_copy,&
                            destroy_scf_type_large => destroy_scf_type
  USE fft_base,      ONLY : dfftp, dfftl
  USE gvecs,         ONLY : doublegrid,dual
  USE io_global,     ONLY : stdout, ionode, ionode_id
  USE io_files,      ONLY : iunwfc
  USE lsda_mod,      ONLY : nspin
  USE uspp,          ONLY : okvan, nkb
  USE dfunct,        ONLY : newd
  USE tddft_module   
  USE becmod,        ONLY : becp, allocate_bec_type, deallocate_bec_type, &
                            is_allocated_bec_type
  USE wvfct,         ONLY : nbnd,ecutwfc
  USE ldaU,          ONLY : eth
  USE ener,          ONLY : etot, eband, etxc, ehart, etxcc, vtxc, ewld  
  USE cell_base,     ONLY : at, bg, alat, omega, celldm, ibrav
  USE large_cell_base,     ONLY : atl => at, bgl => bg, alatl => alat, omegal => omega, celldml => celldm, ibravl => ibrav
! ** change pointers but not for alat**. Alat is the same for dense and
! large cells, but sometimes still called at a different name. 
!  USE cell_base_large,     ONLY :  at, bg, alat, omega
!
! tau is weird now, tau_fde has the full coordinates but tau is fictional
! coordinates because we force the molecules, wherever they are, to be centered
! in the small cell, so the position of the atoms in tau and tau_fde is
! different.
! do not gather coordinates, unless you do a scatter first, which enforces tau
! to have the real coordinates. But if you want to calculate the non-local
! potential, you want the fake tau, because you want it to be in the center of
! the cell or your density will be fucked up.
  USE ions_base,     ONLY : nsp, atm, zv, nat, tau, ityp
  USE gvect,         ONLY : ngm, gstart, g, gg, gcutm, nl, eigts1, eigts2, eigts3
  USE gvecl,         ONLY : ngml => ngm, gstartl => gstart, gl => g, ggl => gg, gcutml => gcutm, nll => nl,&
                            eigts1l => eigts1, eigts2l => eigts2, eigts3l => eigts3
  USE control_flags, ONLY : gamma_only
! a new module called vlocal_large, within it vltot. 
! strf is a bitch but the one from vlocal_large is not used. strf_fde is
! meaningless now. Instead, use strf_fde_large which is in the fde module, the
! structure factor of the total system on the large cell, used for the local
! potential and whatever else you need it for.
  USE vlocal,        ONLY : strf
  USE fde
  use fde_routines
  use mp_images, only : my_image_id, inter_fragment_comm, intra_image_comm, me_image
  use mp_world,                 only : world_comm
  use mp,                       only : mp_sum, mp_barrier, mp_bcast, mp_gather
  use fft_interfaces,              only : fwfft
! modules needed for adding the add_efield_t
  USE noncollin_module, ONLY :  nspin_lsda
! for calling struc_fact
  implicit none
  interface
    subroutine calculate_eband(istepp)
      integer, intent(in), optional :: istepp
    end subroutine calculate_eband
  end interface

  integer, intent(in) :: istepp
  integer :: is
  real(dp) :: charge, ewld_frag2
  real(dp), external :: ewald
  real(dp), allocatable :: aux(:,:) 
  complex(dp), allocatable :: gauxl(:)
  CHARACTER(LEN=6), EXTERNAL :: int_to_char

  call start_clock('updateH')

  ! this will ensure that v_of_rho uses the large grid
  calc_ener_in_v_of_rho=(ISTEPP==0.OR.istepp==1.or.(CALC_ENER.AND.(MOD(ISTEPP,ICALC_ENER)==0)))
  
  if (is_allocated_bec_type(becp)) call deallocate_bec_type(becp)
  if (do_fde) then
   if (istepp==0) then 
!     if (.not.coupled) then
! make sure that when mixing coupled and uncoupled subsystem, rho_fde has a density made from the shifted evc for the fragments within
! the coupled set, and density made from unshifted evc for the fragments within the uncoupled set.
       call create_scf_type(rho_tmp,.true.)
       call fde_fake_nspin(.true.)
       call create_scf_type(rho_tmp1,.true.)
       call create_scf_type(rho_tmp2,.true.)
       call create_scf_type_large(rho_tmp1_large,.true.)
       call create_scf_type_large(rho_tmp2_large,.true.)
       call fde_fake_nspin(.false.)
       rho_tmp%of_r=0.0d0
       rho_tmp%of_g=0.0d0
       rho_tmp1%of_r=0.0d0
       rho_tmp1%of_g=0.0d0
       rho_tmp2%of_r=0.0d0
       rho_tmp2%of_g=0.0d0
       rho_tmp1_large%of_r=0.0d0
       rho_tmp2_large%of_r=0.0d0
       rho_tmp1_large%of_g=0.0d0
       rho_tmp2_large%of_g=0.0d0
! creating rho into the fragment cell, should still be OK.
       call sum_band_tddft(iunwfc,rho_tmp)
       call sum_band_tddft(iunevcn,rho)
       if (chain) then
         call update_rho_new_chain(rho_tmp,rho_tmp1,rho_tmp1_large,inter_fragment_comm)
       else 
           call update_rho_new(rho_tmp,rho_tmp1,rho_tmp1_large,inter_coupled_comm)
      endif
       call update_rho_new(rho_tmp,rho_tmp2,rho_tmp2_large,inter_fragment_comm)
       if (chain) then
           call update_rho_new_chain(rho,rho_fde,rho_fde_large,inter_fragment_comm)
       else
           call update_rho_new(rho, rho_fde, rho_fde_large, inter_coupled_comm)
       endif
! no need for nspin adjustment: these are all "total densities", summed over all fragments, so have both components
       rho_frozen%of_r=rho_frozen%of_r+(rho_tmp2%of_r-rho_tmp1%of_r)
       rho_frozen%of_g=rho_frozen%of_g+(rho_tmp2%of_g-rho_tmp1%of_g)
       rho_fde%of_r=rho_fde%of_r+rho_frozen%of_r ! if not (frozen), rho_frozen is just empty
       rho_fde%of_g=rho_fde%of_g+rho_frozen%of_g
       if (linterlock) then
          rho_frozen_large%of_r=rho_frozen_large%of_r+(rho_tmp2_large%of_r-rho_tmp1_large%of_r)
!          rho_frozen_large%of_g=rho_frozen_large%of_g+(rho_tmp2_large%of_g-rho_tmp1_large%of_g)
       allocate(gauxl(dfftl%nnr))
            do is=1,fde_nspin
               gauxl(:) = cmplx(rho_frozen_large%of_r(:,is), 0.d0, kind=dp)
               call fwfft('Custom', gauxl, dfftl)
               rho_frozen_large%of_g(1:ngml,is) = gauxl(nll(1:ngml))
             enddo
        deallocate(gauxl)
          rho_fde_large%of_r=rho_fde_large%of_r+rho_frozen_large%of_r ! if not (frozen), rho_frozen is just empty
          rho_fde_large%of_g=rho_fde_large%of_g+rho_frozen_large%of_g
     
           
       endif
       
       call destroy_scf_type(rho_tmp)
       call destroy_scf_type(rho_tmp1)
       call destroy_scf_type(rho_tmp2)
       if (linterlock) then
          call destroy_scf_type_large(rho_tmp1_large)
          call destroy_scf_type_large(rho_tmp2_large)
       endif
       call scf_type_copy(rho_fde,rho_coupled)
       if (linterlock) call scf_type_copy_large(rho_fde_large,rho_coupled_large)
!
   elseif (late_coupling.and.(ilate_coupling==istepp)) then
!
       write(stdout,*)' LATE_COUPLING IS PROBABLY NOT COMPATIBLE WITH FROZEN OR CHAIN, PROCEED WITH CAUTION '
       coupled=.true.
       call sum_band_tddft(iunevcn,rho)
       call update_rho_fde(rho, .true.)
       rho_fde%of_r=rho_fde%of_r+rho_frozen%of_r
       rho_fde%of_g=rho_fde%of_g+rho_frozen%of_g
       if (linterlock) then
          rho_fde_large%of_r=rho_fde_large%of_r+rho_frozen_large%of_r
          rho_fde_large%of_g=rho_fde_large%of_g+rho_frozen_large%of_g
       endif
       call scf_type_copy(rho_fde,rho_coupled)
       if (linterlock) call scf_type_copy_large(rho_fde_large,rho_coupled_large)
       rho_t0%of_r = 0.0d0
       rho_t0%of_g = 0.0d0
       rho_t0_large%of_r = 0.0d0
       rho_t0_large%of_g = 0.0d0
       ! copy rho to rho_t0 
       call update_rho_t0(rho,  inter_coupled_comm)
! OK for open shell, all densities are sums over fragments
       rho_frozen%of_r=rho_coupled%of_r-rho_t0%of_r
       rho_frozen%of_g=rho_coupled%of_g-rho_t0%of_g
       if (linterlock) then
          rho_frozen_large%of_r=rho_coupled_large%of_r-rho_t0_large%of_r
          rho_frozen_large%of_g=rho_coupled_large%of_g-rho_t0_large%of_g
!       allocate(gauxl(dfftl%nnr))
!            do is=1,fde_nspin
!               gauxl(:) = cmplx(rho_frozen_large%of_r(:,is), 0.d0, kind=dp)
!               call fwfft('Custom', gauxl, dfftl)
!               rho_frozen_large%of_g(1:ngml,is) = gauxl(nll(1:ngml))
!             enddo
!        deallocate(gauxl)
       endif
       call update_rho_coupled(rho, .true.,inter_coupled_comm)
   else
      call sum_band_tddft(iunevcn,rho)
      if (coupled) then
! update_rho_fde is changed a lot but this should still work.
! we have more rho's: rho is fragment density on the dense grid, rho_fde is the
! total density on the dense grid and a new density rho_fde_large, the real
! rho_fde. 
! Local potentials that depend on rho_fde are calculated on the dense grid, and
! the hartree is calculated on the large grid, depending rho_fde_large. Other
! non-local shit is still broken (like non-local correlation or non-local
! kinetic voodoo).
! the potential you calculate on this grid is OK, but the nonadditive energy is
! meaningless. the full nonadditive energy is calculated in fde_nonadditive()
           if (chain) then
             call update_rho_chain(rho, .true.,inter_fragment_comm)
           else
             call update_rho_coupled(rho, .true.,inter_coupled_comm)
           endif
      else
! update_rho_fde for false doesn't work, needs to be fixed
             call update_rho_coupled(rho, .false.,inter_coupled_comm)
      endif
    endif
  else ! do_fde
      call sum_band_tddft(iunevcn,rho)
   endif

           
  if (.not. is_allocated_bec_type(becp)) call allocate_bec_type(nkb, nbnd, becp)


  if (lda_plus_U) then
    call new_ns
    if (iverbosity > 20) call write_ns()
  end if
    
! v_of_rho is changed but the call to it should probably work.
  ! calculate HXC-potential
  call v_of_rho( rho, rho_core, rhog_core, ehart, etxc, vtxc, eth, etotefield, charge, v )
! PRINT POTENTIAL FOR DEBUGGING
!  call plot_io('potentialv_'//trim(int_to_char(istepp))//trim(int_to_char(my_image_id))//'.pp','v_of_rho'&
!                ,dfftp%nr1x,dfftp%nr2x,dfftp%nr3x,&
!                dfftp%nr1,dfftp%nr2,dfftp%nr3,nat,nsp,ibrav,celldm,at,&
!                gcutm,dual,ecutwfc,0,atm,ityp,zv,tau,v%of_r,+1)

! add local version of add_efield_t
! Caution: we're using our own tefield, which is different from the tefield in
! extfield module so when add_efield is called through v_of_rho, tefield is
! false even if it is true in tddft. Reason: first variable set to false and
! forces exit within add_efield.
   if (itdpulse>1.and.(dt*(istepp-1)<=ttend)) then
     DO is = 1, nspin_lsda
! needs to be adjusted. Probably want to do this on the large cell and the
! scatter or whatever to the dense. Alessandro needs to fix the add_efield in
! PW.
        CALL add_efield_t(v%of_r(1,is),   .true.)
     END DO
   endif 

! PRINT POTENTIAL FOR DEBUGGING
!  call plot_io('potential_vltot_'//trim(int_to_char(istepp))//trim(int_to_char(my_image_id))//'.pp',&
!                'vltot',dfftp%nr1x,dfftp%nr2x,dfftp%nr3x,&
!                dfftp%nr1,dfftp%nr2,dfftp%nr3,nat,nsp,ibrav,celldm,at,&
!                gcutm,dual,ecutwfc,0,atm,ityp,zv,tau,vltot,+1)
!
  ! calculate total local potential (external + scf)

! vltot is calculated in hinit0, where it calls init_vloc and that calculates
! the potential for each atom type, and then you calculated the structure factor
! and the call setlocal_fde_large which calculates vltot on the large grid
! (vltot_large). the vltot_large is then copied to the dense cell in potinit.
  call set_vrs(vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid)    
! PRINT POTENTIAL FOR DEBUGGING
!  call plot_io('potential_'//trim(int_to_char(istepp))//trim(int_to_char(my_image_id))//'.pp',&
!                'vrs',dfftp%nr1x,dfftp%nr2x,dfftp%nr3x,&
!                dfftp%nr1,dfftp%nr2,dfftp%nr3,nat,nsp,ibrav,celldm,at,&
!                gcutm,dual,ecutwfc,0,atm,ityp,zv,tau,vrs,+1)
 
!****************************************************************
! with the interlocking shit, calculating energies is very expensive, make
! calc_ener as default only every 100 stops or so.
   !IF (ISTEPP==0.OR.istepp==1.or.(CALC_ENER.AND.(MOD(ISTEPP,ICALC_ENER)==0))) then
   IF (calc_ener_in_v_of_rho) then
!****************************************************************
  ! calculate eband and etot
 call calculate_eband()

  if (do_fde .and. .not. fde_init_rho) then
    if (linterlock) then
! not sure if I want to calculate ewld_frag using the "large" cell or the "normal" cell.
! I might need an strf_large, subsystem on the large cell




       ewld_frag = ewald( alatl, nat, nsp, ityp, zv, atl, bgl, tau_large, &
                   omegal, gl, ggl, ngml, gcutml, gstartl, gamma_only, strf_frag_large )

       ewld = ewald( alatl, nat_fde, nsp, ityp_fde, zv, atl, bgl, tau_fde, &
                   omegal, gl, ggl, ngml, gcutml, gstartl, gamma_only, strf_fde_large )
    else
       ewld_frag = ewald( alat, nat, nsp, ityp, zv, at, bg, tau, &
                   omega, g, gg, ngm, gcutm, gstart, gamma_only, strf )
       ewld = ewald( alat, nat_fde, nsp, ityp_fde, zv, at, bg, tau_fde, &
                   omega, g, gg, ngm, gcutm, gstart, gamma_only, strf_fde )
    endif
  else
     ewld = ewald( alat, nat, nsp, ityp, zv, at, bg, tau, &
                   omega, g, gg, ngm, gcutm, gstart, gamma_only, strf )
  endif
! etot is calculated for each fragment separately, but the ehart is calculated
! using the total rho_fde. So in the uncoupled version, summing the energies up
! doesn't make sense because the rho_fde is NOT THE SAME on the different
! fragments and hence the total energy will be not correct. To fix this, we need
! to introduce a "total energy" for each fragment, where instead of summing the
! etot up over inter_fragment_comm tot etot_fde, the etot_fde of each fragment
! is partially updated with the etot of the fragment itself AND the ehart of the
! frozen fragments which also changes because it is calculated with the rho_fde.
!
! for a mixed coupled/uncoupled calculation, after calculating the coupled total
! energy, we need to add the uncoupled energy to the coupled one to get the
! total energy 
  etot = eband + ( etxc - etxcc ) + ewld + ehart
!=================================================
! DEBUG OUT FOR ENERGY
!
! if (ionode) then
! if (iverbosity>10) then
!    write(stdout,*) "Hartree = ", ehart
!    write(stdout,*) "Eband   = ", eband
!    write(stdout,*) "Ewald   = ", ewld
!    write(stdout,*) "etxc-etxcc = ", ( etxc - etxcc )
! endif
! endif
!
!=================================================

  if (do_fde) then
     !=================================================
     ! DISABLING THIS FOR NOW
     !=================================================
     if (.false.) then
      etot_frag_mixed=0.0d0
!********************************************************************
       call en_diff() ! substract \int v_eN \rho_env for a "pure" en_frag
       ! calculation of v_h_frag needs to be done now on the large cell. I need a new
       ! quantity: rho of the fragment but on the large cell (allocate that shit). 
       ! look at v_h_large in PW.
       call v_h_frag(rho%of_g, rho%of_r, ehart_frag, charge_frag)
       allocate( aux(dfftp%nnr,nspin) )
       ! for v_xc: if I only want the potential, the dense cell is OK (for local xc
       ! functionals). I actually want the energy, but using rho, not rho_fde...
       call v_xc(rho, rho_core, rhog_core,etxc_frag,vtxc_frag, aux)
       deallocate(aux)
       etot_frag = eband - en_tot + en_frag + ehart_frag + etxc_frag + ewld_frag
       etot_frag_mixed = etot_frag
       if (istepp==0) then
                energies_of_t0(1) = etot_frag
                energies_of_t0(2) = etot
                energies_of_t0(3) = ekin0_nadd
                energies_of_t0(4)= ekin_nadd
                energies_of_t0(5) = etxc0_nadd
                energies_of_t0(6)= etxc_nadd
       endif
     
       ! gather all t_0 energies in 1 big array and distribute it between everyone
       if (ionode)  call mp_gather(energies_of_t0,energies_of_all,0,inter_fragment_comm)
       if (ionode) call mp_bcast(energies_of_all,0,inter_fragment_comm)
       call mp_bcast(energies_of_all,0,intra_image_comm)
       ! now sum up the energies using "coupled" part for colors(i)=color and "uncoupled" for colors(i)<>color      
      endif !.false.
      !=================================================
      ! Done with Disabled part
      !=================================================

      call fde_energies_coupled()
      
  endif ! do_fde



!****************************************************************
  ENDIF ! calc_ener
!****************************************************************
 


  ! calculate new D_nm matrix for ultrasoft pseudopotential
  if (okvan) then
    if (istepp == -1 .or. ( (nupdate_Dnm /= 0 .and. mod(istepp,nupdate_Dnm) == 0) ) ) then
      call newd()
    endif
  endif
    
  call deallocate_bec_type(becp)
  call stop_clock('updateH')
    
END SUBROUTINE update_hamiltonian

 !-----------------------------------------------------------------------
 SUBROUTINE calculate_eband(istepp)
  !-----------------------------------------------------------------------
  !
  USE kinds,                       ONLY : dp
!  USE wavefunctions_module,        ONLY : evc
  USE io_global,                   ONLY : stdout
  USE io_files,                    ONLY : nwordwfc, iunwfc, iunigk
  USE klist,                       ONLY : nks, wk
  USE wvfct,                       ONLY : nbnd, npwx, npw, wg, current_k, et, igk, g2kin
  USE lsda_mod,                    ONLY : current_spin, isk, nspin
  USE becmod,                      ONLY : becp, calbec, allocate_bec_type, &
                                          is_allocated_bec_type, deallocate_bec_type
  USE mp,                          ONLY : mp_sum, mp_barrier
  USE mp_pools,                    ONLY : intra_pool_comm, inter_pool_comm
  USE buffers,                     ONLY : get_buffer, save_buffer
  USE uspp,                        ONLY : nkb, vkb
  USE pwcom
  USE tddft_module
  use constants,                   only : rytoev
  ! ... Calculate band energy
  !
  implicit none
  complex(dp), allocatable :: hpsi(:,:)
  integer :: ik, ibnd
  complex(dp), external :: zdotc
  integer, intent(in), optional :: istepp
  complex(dp), allocatable :: evc_temp(:,:)
 
  if (.not. is_allocated_bec_type(becp)) call allocate_bec_type(nkb, nbnd, becp)
  allocate(hpsi(npwx,nbnd))
  allocate(evc_temp(npwx,nbnd))
  eband = 0.d0
  ! loop over k-points     
  if (nks > 1) rewind (iunigk)
  do ik = 1, nks
    current_k = ik
    current_spin = isk(ik)
   
!*******************************************************************
!! NEW PART, taken from PW/src
!    npw = ngk(ik)
!     IF (nks > 1) THEN
!        READ (iunigk) igk
!     endif
!    
!    ! read wfcs from file and compute becp
!    evc = (0.d0, 0.d0)
!    if (present(istepp)) then 
!        call get_buffer (evc, nwordwfc, iunwfc, ik)
!    else
!        call get_buffer(evc, nwordwfc, iunevcn, ik)
!    endif
!    call init_us_2(npw, igk, xk(1,ik), vkb)

!*******************************************************************
! OLD PART, CHECK IF OK: IT IS NOT
!    ! initialize at k-point k 
    call gk_sort(xk(1,ik), ngm, g, ecutwfc/tpiba2, npw, igk, g2kin)
    g2kin = g2kin * tpiba2
    call init_us_2(npw, igk, xk(1,ik), vkb)
!
    ! read wfcs from file and compute becp
    evc_temp = (0.d0, 0.d0)
    if (present(istepp)) then 
        call get_buffer (evc_temp, nwordwfc, iunwfc, ik)
    else
        call get_buffer(evc_temp, nwordwfc, iunevcn, ik)
    endif
!
!*******************************************************************
    call calbec(npw, vkb, evc_temp, becp)

 
    ! calculate H |psi_current>
!    call h_psi(npwx, npw, nbnd_occ(ik), evc_temp, hpsi)
    call h_psi(npwx, npw, nbnd, evc_temp, hpsi)
    
!    ! add to eband
!    do ibnd = 1, nbnd_occ(ik)
  do ibnd = 1, nbnd
        et(ibnd,ik) = real(zdotc(npw,hpsi(1,ibnd),1,evc_temp(1,ibnd),1),dp)
    enddo
    do ibnd = 1, nbnd_occ(ik)
        eband = eband + wg(ibnd,ik)*real(zdotc(npw,hpsi(1,ibnd),1,evc_temp(1,ibnd),1),dp)
    enddo
  enddo
  call mp_sum(et, intra_pool_comm)
  call mp_sum(et, inter_pool_comm)
  call mp_sum(eband, intra_pool_comm)
  call mp_sum(eband, inter_pool_comm)
  
  eband = eband + delta_e()
  
  deallocate(hpsi)
  deallocate(evc_temp)
  call deallocate_bec_type(becp)
  
  CONTAINS
  
   !-----------------------------------------------------------------------
   FUNCTION delta_e()
     !-----------------------------------------------------------------------
     ! ... delta_e = - \int rho%of_r(r)  v%of_r(r)
     !               - \int rho%kin_r(r) v%kin_r(r) [for Meta-GGA]
     !               - \sum rho%ns       v%ns       [for LDA+U]
     !               - \sum becsum       D1_Hxc     [for PAW]
     USE scf,              ONLY : scf_type, rho, v
     USE funct,            ONLY : dft_is_meta
     USE fft_base,         ONLY : dfftp, dfftl
     USE noncollin_module, ONLY : noncolin
     USE mp_bands,         ONLY : intra_bgrp_comm
     USE paw_variables,    ONLY : okpaw, ddd_paw
     IMPLICIT NONE
     REAL(DP) :: delta_e, delta_e_hub
     !
     delta_e = - SUM( rho%of_r(:,:)*v%of_r(:,:) )
     !
     IF ( dft_is_meta() ) &
        delta_e = delta_e - SUM( rho%kin_r(:,:)*v%kin_r(:,:) )
     !
     delta_e = omega * delta_e / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
     !
     CALL mp_sum( delta_e, intra_bgrp_comm )
     !
     if (lda_plus_u) then
       if (noncolin) then
         delta_e_hub = - real(SUM (rho%ns_nc(:,:,:,:)*v%ns_nc(:,:,:,:)),kind=dp)
         delta_e = delta_e + delta_e_hub
       else
         delta_e_hub = - SUM (rho%ns(:,:,:,:)*v%ns(:,:,:,:))
         if (nspin==1) delta_e_hub = 2.d0 * delta_e_hub
         delta_e = delta_e + delta_e_hub
       endif
     end if
     !
     IF (okpaw) delta_e = delta_e - SUM(ddd_paw(:,:,:)*rho%bec(:,:,:))
     !
     RETURN
     !
   END FUNCTION delta_e

   !-----------------------------------------------------------------------
   FUNCTION delta_e_frag()
     !-----------------------------------------------------------------------
     ! ... delta_e = - \int rho%of_r(r)  v%of_r(r)
     !               - \int rho%kin_r(r) v%kin_r(r) [for Meta-GGA]
     !               - \sum rho%ns       v%ns       [for LDA+U]
     !               - \sum becsum       D1_Hxc     [for PAW]
     USE scf,              ONLY : scf_type, rho, v
     USE funct,            ONLY : dft_is_meta
     USE fft_base,         ONLY : dfftp, dfftl
     USE noncollin_module, ONLY : noncolin
     USE mp_bands,         ONLY : intra_bgrp_comm
     USE paw_variables,    ONLY : okpaw, ddd_paw
     use fde
     IMPLICIT NONE
     REAL(DP) :: delta_e_frag, delta_e_hub
     real(dp), allocatable    :: v_frag(:,:), v_new(:,:), vh(:,:)
      real(dp) :: dummy1, dummy2
     !
     allocate( v_frag(dfftp%nnr,nspin) )
     allocate( v_new(dfftp%nnr,nspin) )
     allocate( vh(dfftp%nnr,nspin) )

     call v_h( rho%of_g, dummy1, dummy2, v_frag )
     call v_h( rho_fde%of_g, dummy1, dummy2, vh )
 
     v_new(:,:) = v%of_r(:,:) - vh(:,:) + v_frag(:,:)

     delta_e_frag = - SUM( rho%of_r(:,:)*v_new(:,:) )
     !
     delta_e_frag = omega * delta_e_frag / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
     !
     CALL mp_sum( delta_e_frag, intra_bgrp_comm )
     !
     !
     deallocate(v_frag,v_new,vh)
     !
     RETURN
     !
   END FUNCTION delta_e_frag
 END SUBROUTINE calculate_eband

   
   
 !-----------------------------------------------------------------------
 SUBROUTINE calculate_eband_deltat(rho_deltat,v_deltat)
  !-----------------------------------------------------------------------
  !
  USE kinds,                       ONLY : dp
!  USE wavefunctions_module,        ONLY : evc
  USE io_global,                   ONLY : stdout
  USE io_files,                    ONLY : nwordwfc, iunwfc, iunigk
  USE klist,                       ONLY : nks
  USE wvfct,                       ONLY : nbnd, npwx, npw, wg, current_k, et, igk, g2kin
  USE lsda_mod,                    ONLY : current_spin, isk, nspin
  USE becmod,                      ONLY : becp, calbec, allocate_bec_type, &
                                          is_allocated_bec_type
  USE mp,                          ONLY : mp_sum, mp_barrier
  USE mp_pools,                    ONLY : intra_pool_comm, inter_pool_comm
  USE buffers,                     ONLY : get_buffer, save_buffer
  USE uspp,                        ONLY : nkb, vkb
  USE pwcom
  USE tddft_module
  use scf, only : scf_type
  ! ... Calculate band energy
  !
  implicit none
  complex(dp), allocatable :: hpsi(:,:)
  complex(dp), allocatable :: tddft_psi_deltat(:,:,:)
  integer :: ik, ibnd
  complex(dp), external :: zdotc
  type (scf_type) :: rho_deltat, v_deltat
 
  if (.not. is_allocated_bec_type(becp)) call allocate_bec_type(nkb, nbnd, becp)
  allocate(hpsi(npwx,nbnd))
   allocate (tddft_psi_deltat(npwx,nbnd,2))
  eband = 0.d0
  ! loop over k-points     
  if (nks > 1) rewind (iunigk)
  do ik = 1, nks
    current_k = ik
    current_spin = isk(ik)
   
!*******************************************************************
!! NEW PART, taken from PW/src
!    npw = ngk(ik)
!     IF (nks > 1) THEN
!        READ (iunigk) igk
!     endif
!    
!    ! read wfcs from file and compute becp
!    evc = (0.d0, 0.d0)
!    if (present(istepp)) then 
!        call get_buffer (evc, nwordwfc, iunwfc, ik)
!    else
!        call get_buffer(evc, nwordwfc, iunevcn, ik)
!    endif
!    call init_us_2(npw, igk, xk(1,ik), vkb)

!*******************************************************************
! OLD PART, CHECK IF OK: IT IS NOT
!    ! initialize at k-point k 
    call gk_sort(xk(1,ik), ngm, g, ecutwfc/tpiba2, npw, igk, g2kin)
    g2kin = g2kin * tpiba2
    call init_us_2(npw, igk, xk(1,ik), vkb)
!
    ! read wfcs from file and compute becp
        call get_buffer(tddft_psi_deltat, 2*nwordwfc, iuntdwfc, ik)
!
!*******************************************************************
    call calbec(npw, vkb, tddft_psi_deltat(:,:,2), becp)

 
    ! calculate H |psi_current>
!    call h_psi(npwx, npw, nbnd_occ(ik), evc, hpsi)
    call h_psi(npwx, npw, nbnd, tddft_psi_deltat(:,:,2), hpsi)
    
    ! add to eband
!    do ibnd = 1, nbnd_occ(ik)
    do ibnd = 1, nbnd
        et(ibnd,ik) = real(zdotc(npw,hpsi(1,ibnd),1,tddft_psi_deltat(1,ibnd,2),1),dp)
    enddo
    do ibnd = 1, nbnd_occ(ik)
        eband = eband + wg(ibnd,ik)*real(zdotc(npw,hpsi(1,ibnd),1,tddft_psi_deltat(1,ibnd,2),1),dp)
    enddo
  enddo
  call mp_sum(et, intra_pool_comm)
  call mp_sum(et, inter_pool_comm)
  call mp_sum(eband, intra_pool_comm)
  call mp_sum(eband, inter_pool_comm)
  
  eband = eband + delta_e_deltat(rho_deltat,v_deltat)
  
  deallocate(hpsi)
  deallocate(tddft_psi_deltat)
  
  CONTAINS
  
   !-----------------------------------------------------------------------
   FUNCTION delta_e_deltat(rho,v)
     !-----------------------------------------------------------------------
     ! ... delta_e = - \int rho%of_r(r)  v%of_r(r)
     !               - \int rho%kin_r(r) v%kin_r(r) [for Meta-GGA]
     !               - \sum rho%ns       v%ns       [for LDA+U]
     !               - \sum becsum       D1_Hxc     [for PAW]
     USE scf,              ONLY : scf_type
     USE funct,            ONLY : dft_is_meta
     USE fft_base,         ONLY : dfftp, dfftl
     USE noncollin_module, ONLY : noncolin
     USE mp_bands,         ONLY : intra_bgrp_comm
     USE paw_variables,    ONLY : okpaw, ddd_paw
     IMPLICIT NONE
     REAL(DP) :: delta_e_deltat, delta_e_hub
     type (scf_type), intent(inout) :: rho, v
     !
     delta_e_deltat = - SUM( rho%of_r(:,:)*v%of_r(:,:) )
     !
     IF ( dft_is_meta() ) &
        delta_e_deltat = delta_e_deltat - SUM( rho%kin_r(:,:)*v%kin_r(:,:) )
     !
     delta_e_deltat = omega * delta_e_deltat / ( dfftp%nr1*dfftp%nr2*dfftp%nr3 )
     !
     CALL mp_sum( delta_e_deltat, intra_bgrp_comm )
     !
     if (lda_plus_u) then
       if (noncolin) then
         delta_e_hub = - real(SUM (rho%ns_nc(:,:,:,:)*v%ns_nc(:,:,:,:)),kind=dp)
         delta_e_deltat = delta_e_deltat + delta_e_hub
       else
         delta_e_hub = - SUM (rho%ns(:,:,:,:)*v%ns(:,:,:,:))
         if (nspin==1) delta_e_hub = 2.d0 * delta_e_hub
         delta_e_deltat = delta_e_deltat + delta_e_hub
       endif
     end if
     !
     IF (okpaw) delta_e_deltat = delta_e_deltat - SUM(ddd_paw(:,:,:)*rho%bec(:,:,:))
     !
     RETURN
     !
   END FUNCTION delta_e_deltat

 END SUBROUTINE calculate_eband_deltat
