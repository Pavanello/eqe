subroutine e_emb()
!
!  calculate interaction energy as \int v_emb(r) * \rho_I(r) dr
!
  USE fft_base,                 ONLY : dfftp, grid_gather, dfftl
  USE cell_base,                ONLY : celldm, at, ibrav, omega, bg
  USE large_cell_base,                ONLY : celldml => celldm, atl => at, ibravl => ibrav, omegal => omega, bgl => bg
  USE ions_base,                ONLY : ntyp => nsp, atm, zv, nat, tau, ityp
  USE vlocal,        ONLY : strf
  USE gvect,                    ONLY : gcutm, ngm, gstart, g, gg 
  USE gvecl,                    ONLY : gcutml => gcutm, ngml => ngm, gstartl => gstart, gl => g, ggl => gg 
  USE gvecs,                    ONLY : dual
  USE wvfct,                    ONLY : ecutwfc
  USE io_global,                ONLY : ionode, stdout
  USE mp_global,                ONLY : my_image_id
  USE mp,                          ONLY : mp_sum
  USE scf,                      ONLY : rho, rho_core, rhog_core
  use input_parameters,         only : fde_xc_funct
  use ener,                     only : ewld
  USE control_flags, ONLY : gamma_only
  USE mp_global,  ONLY : intra_bgrp_comm
  USE fde
  use fde_routines
  use tddft_module
  implicit none

  real(dp), external :: ewald
  real(dp), allocatable :: aux(:,:)
  complex(dp), allocatable :: environ_rhog(:,:)
  real(dp) :: trash0, trash1, trash2, trash3
  real(dp) :: domega
  real(dp), allocatable :: tau_emb(:,:)
  complex(dp), allocatable :: strf_emb(:,:)
  integer :: i, nat_emb
  integer, allocatable :: ityp_emb(:)

  allocate( environ_rhog(ngm,fde_frag_nspin) )
  allocate( aux(dfftp%nnr,fde_frag_nspin) )

  domega = omega / dble(dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
  e_embedding = 0.0d0
  aux(:,:) = 0.0d0

  call fde_nonadditive(rho, rho_fde, rho_core, rhog_core, &
                          rho_core_fde, rhog_core_fde, &
                          rho_gauss, rhog_gauss, &
                          rho_gauss_fde, rhog_gauss_fde, &
                          trash0, &
                          trash1, &
                          trash2, &
                          trash3, aux)
   e_embedding = e_embedding + trash0 - trash1 + trash2
!   write(stdout,*)'ekin, ekin0, etxc',trash0, trash1, trash2
   ekin_tot = trash0
   etxc_tot = trash2

  if ( trim(fde_xc_funct) == 'SAME' ) then
!    aux(:,:) = 0.d0
!   call v_xc(rho, rho_core, rhog_core, trash0, trash1, aux)
!   write(stdout,*)'etxc0',trash0
    e_embedding = e_embedding - etxc_frag
  endif

  if (fde_frag_nspin /= fde_nspin) then
    write(stdout,*) 'In e_emb: not sure if correct for open shell'
    environ_rhog(:,:) = rho_fde%of_g(:,:) - rho%of_g(:,:) 
    !TODO: what when only some fragments are openshell?
  else
    environ_rhog(:,:) = rho_fde%of_g(:,:) - rho%of_g(:,:) 
  endif
  aux(:,:)=0.d0
  call v_h_fde( rho%of_g, rho%of_r, environ_rhog, trash0, trash1, aux ) 
!    write(stdout,*)'ehart_envj',trash0
    e_embedding = e_embedding + trash0
!!!!!!!!!!
!!!!!!!!!!

  aux(:,:)=0.d0
 call setlocal_embed(aux(:,1))

  trash0 = sum(rho%of_r(:,1)*aux(:,1))
  if (fde_frag_nspin == 2) trash0 = trash0 + sum(rho%of_r(:,2)*aux(:,1))
  call mp_sum(trash0, intra_bgrp_comm) 
  domega = omega / dble(dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
   trash0 = trash0 *  domega
  !  write(stdout,*)'e_en_env',trash0
   e_embedding = e_embedding + trash0
!   write(stdout,*)'electronic e_embedding',e_embedding

!  nat_emb=nat_fde-nat
!  ALLOCATE ( strf_emb(ngm, ntyp) )
!  allocate (tau_emb(3,nat_emb), ityp_emb(nat_emb))
!  i = atm_offset(currfrag)
!  tau_emb(:,:)=0.0d0
!  tau_emb(1:3,1:i-1)=tau_fde(1:3,1:i-1)
!  tau_emb(1:3,i:nat_emb)=tau_fde(1:3,i+nat:nat_fde)
!  ityp_emb(:)=0.0d0
!  ityp_emb(1:i-1)=ityp_fde(1:i-1)
!  ityp_emb(i:nat_emb)=ityp_fde(i+nat:nat_fde)
!  strf_emb(:,:) = strf_fde(:,:) - strf(:,:)
!!  write(stdout,*)'ewld',ewld
!!  write(stdout,*)'ewld_frag',ewld_frag
!     ewld_emb = ewald( alat, nat_emb, ntyp, ityp_emb, zv, at, bg, tau_emb, &
!                   omega, g, gg, ngm, gcutm, gstart, gamma_only, strf_emb )
!!  write(stdout,*)'ewld_env',ewld_emb
!     ewld_emb=ewld-ewld_frag-ewld_emb
!!  write(stdout,*)'ewld_emb',ewld_emb
!  deallocate(strf_emb,tau_emb,ityp_emb)
!!  e_embedding = e_embedding+ewld_emb

!   write(stdout,*)'total e_embedding',e_embedding

  deallocate( aux, environ_rhog )
end subroutine e_emb
