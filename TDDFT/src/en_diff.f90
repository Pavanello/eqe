subroutine en_diff()
! substract the eN part that is not of the fragment
! calculated "en_tot" which is /int vloc_tot rho_frag, to get ekin_frag = eband-en_tot

  use fft_base,                 only : dfftp, dfftl
  USE fft_interfaces,           ONLY : invfft
  use fft_base, only : grid_gather_large
  use ions_base,                only : nat, ityp, ntyp => nsp, if_pos
  use gvect,                    only : nl, ngm, igtongl
  use gvecl,                    only : nll => nl, ngml => ngm, igtongll => igtongl
  USE mp,                          ONLY : mp_sum, mp_bcast
  use mp_images, only : intra_image_comm
  use cell_base,                only : omega
  use large_cell_base,                only : omegal => omega
  USE vlocal,    ONLY : strf, vloc
  use vlocal_large, only : vloc_large
  use scf,       only : rho
  use mp_global,                only : intra_bgrp_comm
  use fde
  use fde_routines
  use tddft_module
  USE io_global,     ONLY : stdout, ionode
  use mp_large, only : intra_lgrp_comm
  use lsda_mod,                 only : nspin
  implicit none
  integer :: nt, ng
  complex(dp), allocatable :: aux1(:), aux2(:) 
  real(dp) , allocatable :: aux_real(:), rauxl(:), raux(:), aux1_real(:), aux2_real(:), rauxl2(:), raux2(:)
  real(dp) :: domega, domegal

  if (linterlock) then
      ALLOCATE (aux1( dfftl%nnr), aux1_real(dfftl%nnr))
      ALLOCATE (aux2( dfftl%nnr), aux2_real(dfftl%nnr))
!      allocate (rauxl(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
!      allocate (rauxl2(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
      allocate (raux2(dfftl%nnr))
!      allocate (raux(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
  else
      ALLOCATE (aux1( dfftp%nnr))
      ALLOCATE (aux2( dfftp%nnr), aux_real(dfftp%nnr))
  endif
  aux1(:)=(0.d0,0.d0)
  aux2(:)=(0.d0,0.d0)
!  if (frozen) then
!  do nt = 1, ntyp
!     do ng = 1, ngm
!        aux1 (nl(ng))=aux1(nl(ng)) + vloc (igtongl (ng), nt) * strf_frozen (ng, nt)
!        aux2 (nl(ng))=aux2(nl(ng)) + vloc (igtongl (ng), nt) * strf (ng, nt)
!     enddo
!  enddo
!  else
!=======================================================================================
  if (linterlock) then
!=======================================================================================
     do nt = 1, ntyp
        do ng = 1, ngml
           aux1 (nll(ng))=aux1(nll(ng)) + vloc_large (igtongll (ng), nt) * strf_fde_large (ng, nt)
           aux2 (nll(ng))=aux2(nll(ng)) + vloc_large (igtongll (ng), nt) * strf_frag_large (ng, nt)
        enddo
     enddo
!     do nt = 1, ntyp
!        do ng = 1, ngm
!           aux2 (nl(ng))=aux2(nl(ng)) + vloc (igtongl (ng), nt) * strf (ng, nt)
!        enddo
!     enddo
!      call copy_pot_l2f(aux1(:),aux2(:))
!  endif
     CALL invfft ('Custom', aux1, dfftl)
     CALL invfft ('Custom', aux2, dfftl)

     aux1_real(:) = dble(aux1(:))
     aux2_real(:) = dble(aux2(:))
! I need the fragment density with the *total* vloc*, not just the part scattered on this fragment.
! so I need to do part of the copy_pot_l2f part where it gathers but does not scatter.
!     call copy_pot_l2f(aux1_real,rauxl)
!     call grid_gather_large(aux1_real, rauxl)  ! gathers aux1_real on first processor of pool
!     call grid_gather_large(aux2_real, rauxl2)  ! gathers aux2_real on first processor of pool
!     raux = 0.d0 
     raux2 = 0.d0 
     call copy_pot_f2l(rho%of_r(:,1),raux2)
!     call grid_gather_large(raux2,raux)
     
!     if (ionode) en_tot = sum(raux(:)*rauxl(:))
     en_tot = sum(raux2(:)*aux1_real(:))
     if (nspin == 2) then
         call copy_pot_f2l(rho%of_r(:,2),raux2)
          en_tot = en_tot + sum(raux2(:)*aux1_real(:))
      endif
        
!     call mp_bcast(en_tot,0,intra_image_comm)
     
  
! for integral, I need to transfer rho_fde_large to the fragment as well:
!     call grid_gather_large(rho_fde_large%of_r(:,1),raux(:))
     en_tot_tot = sum(rho_fde_large%of_r(:,1)*aux1_real(:))
     if (nspin == 2) en_tot_tot = en_tot_tot + sum(rho_fde_large%of_r(:,2)*aux1_real(:))


      raux2(:)=0.d0
      call copy_pot_f2l(rho%of_r(:,1), raux2(:))
!      call grid_gather_large(raux2,raux)
!      if (ionode) en_frag = sum(raux(:)*rauxl2(:))
      en_frag = sum(raux2(:)*aux2_real(:))
     if (nspin == 2) en_frag = en_frag + sum(raux2(:)*aux2_real(:))
     CALL mp_sum(en_tot , intra_lgrp_comm )
     CALL mp_sum(en_frag , intra_lgrp_comm )
     CALL mp_sum(en_tot_tot , intra_lgrp_comm )
!=======================================================================================
   else
!=======================================================================================
      do nt = 1, ntyp
         do ng = 1, ngm
            aux1 (nl(ng))=aux1(nl(ng)) + vloc (igtongl (ng), nt) * strf_fde (ng, nt)
            aux2 (nl(ng))=aux2(nl(ng)) + vloc (igtongl (ng), nt) * strf (ng, nt)
         enddo
      enddo
!  endif
      CALL invfft ('Dense', aux1, dfftp)
      CALL invfft ('Dense', aux2, dfftp)

      aux_real(:) = dble(aux1(:))
      en_tot = sum(rho%of_r(:,1)*aux_real(:))
!  if (frozen) then
!     en_tot_tot = sum(rho_coupled%of_r(:,1)*aux_real(:)) !not including frozen fragment in energy calculation
!   else
         en_tot_tot = sum(rho_fde%of_r(:,1)*aux_real(:))
!   endif
      aux_real(:) = dble(aux2(:))
      en_frag= sum(rho%of_r(:,1) * aux_real(:))
      CALL mp_sum(en_tot , intra_bgrp_comm )
      CALL mp_sum(en_frag , intra_bgrp_comm )
      CALL mp_sum(en_tot_tot , intra_bgrp_comm )
!=======================================================================================
   endif ! linterlock
!=======================================================================================

  domega = omega / dble(dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
  if (linterlock)  domegal = omegal /dble(dfftl%nr1 * dfftl%nr2 * dfftl%nr3)
  if (linterlock) then
    en_tot = en_tot * domegal
    en_tot_tot = en_tot_tot * domegal
  else
    en_tot = en_tot * domega
    en_tot_tot = en_tot_tot * domega
  endif
   en_frag = en_frag * domega
  if (linterlock)  then
          deallocate(aux1, aux2, aux1_real, aux2_real,raux2)
  else
      deallocate(aux1,aux2, aux_real)
  endif

end subroutine en_diff
