!----------------------------------------------------------------------------
SUBROUTINE fde_energies_tddft ( conv )
  !----------------------------------------------------------------------------
  !
  !  Subroutine to calculate the FDE total and non-additive energies
  !
  use ener,                     only : etot, ewld, ehart, vtxc, etxc, eband
  use mp,                       only : mp_sum, mp_barrier, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use io_global,                only : ionode, ionode_id, stdout
  use mp_world,                 only : world_comm
  use fde
  use fde_routines
  use input_parameters,         only : fde_xc_funct
  use tddft_module,             only : inter_coupled_comm
  use io_global, only : stdout

  implicit none
  logical , intent(in) :: conv
  real(dp) :: ekin0_nadd_coupled,etxc0_nadd_coupled,ekin_nadd_coupled,etxc_nadd_coupled
  etot_sum = etot 
  
  if ( conv .and. trim(fde_xc_funct) == 'SAME' ) then
    etxc0_nadd = etxc
  endif

  if (ionode) call mp_sum(etot_sum, inter_coupled_comm)
  call mp_bcast(etot_sum, ionode_id, intra_image_comm)
  ekin0_nadd_coupled=ekin0_nadd
  if (ionode) call mp_sum(ekin0_nadd_coupled, inter_coupled_comm)
  call mp_bcast(ekin0_nadd_coupled, ionode_id, intra_image_comm)
  etxc0_nadd_coupled=etxc0_nadd
  if (ionode) call mp_sum(etxc0_nadd_coupled, inter_coupled_comm)
  call mp_bcast(etxc0_nadd_coupled, ionode_id, intra_image_comm)

!  if ( fde_fat ) etot_fde_old = etot_fde
  ekin_nadd_coupled = ekin_nadd - ekin0_nadd_coupled
  etxc_nadd_coupled = etxc_nadd - etxc0_nadd_coupled
  edc_fde = -dble(nfragments-1)*(ewld)
!  edc_fde = -dble(color-1)*(ewld)
  etot_fde= etot_sum + edc_fde + ekin_nadd_coupled + etxc_nadd_coupled

END SUBROUTINE fde_energies_tddft

!----------------------------------------------------------------------------
SUBROUTINE fde_energies_tot ()
  !----------------------------------------------------------------------------
  !
  !  Subroutine to calculate the FDE total and non-additive energies
  !
  use ener,                     only : etot, ewld, ehart, vtxc, etxc, eband
  use mp,                       only : mp_sum, mp_barrier, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use io_global,                only : ionode, ionode_id, stdout
  use mp_world,                 only : world_comm
  use mp_images, only : my_image_id
  use fde
  use input_parameters,         only : fde_xc_funct
  use tddft_module            
  use io_global, only : stdout

  implicit none
  real(dp) :: ekin0_nadd_tot,etxc0_nadd_tot,ekin_nadd_tot,etxc_nadd_tot,etot_sum_tot

  if (coupled) then
      etot_sum_tot = etot 
      ekin0_nadd_tot = ekin0_nadd
      ekin_nadd_tot = ekin_nadd
      etxc0_nadd_tot = etxc0_nadd
      etxc_nadd_tot = etxc_nadd

  else
      etot_sum_tot = etot_t0
      ekin0_nadd_tot = ekin0_nadd_t0
      ekin_nadd_tot = ekin_nadd_t0
      etxc0_nadd_tot = etxc0_nadd_t0
      etxc_nadd_tot = etxc_nadd_t0
   endif
  

  if (ionode) call mp_sum(etot_sum_tot, inter_fragment_comm)
  call mp_bcast(etot_sum_tot, ionode_id, intra_image_comm)

  if (ionode) call mp_sum(ekin0_nadd_tot, inter_fragment_comm)
  call mp_bcast(ekin0_nadd_tot, ionode_id, intra_image_comm)
  if (ionode) call mp_sum(etxc0_nadd_tot, inter_fragment_comm)
  call mp_bcast(etxc0_nadd_tot, ionode_id, intra_image_comm)

!  if ( fde_fat ) etot_fde_old = etot_fde
  ekin_nadd_tot = ekin_nadd_tot - ekin0_nadd_tot
  etxc_nadd_tot = etxc_nadd_tot - etxc0_nadd_tot

  edc_fde = -dble(nfragments-1)*(ewld)
!  edc_fde = -dble(color-1)*(ewld)
  etot_fde= etot_sum_tot + edc_fde + ekin_nadd_tot + etxc_nadd_tot 

END SUBROUTINE fde_energies_tot
