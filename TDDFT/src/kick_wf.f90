!
! Copyright (C) 2001-2014 Quantum-ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!

! Purpose: module to kick the wf at some time istep and with kick time
! itdpulse

module kick_wf

  USE kinds,                       ONLY : dp
  USE io_global,                   ONLY : stdout, ionode
  USE io_files,                    ONLY : nwordwfc, iunwfc, iunigk, wfc_dir
  USE cell_base,                   ONLY : at, tpiba, tpiba2, alat, celldm, ibrav
  USE wavefunctions_module,        ONLY : evc, psic
  USE klist,                       ONLY : nks, xk
  USE wvfct,                       ONLY : nbnd, npwx, npw, igk, g2kin, current_k, ecutwfc, et
  USE lsda_mod,                    ONLY : current_spin, isk, nspin
  USE mp_pools,                    ONLY : inter_pool_comm
  USE mp,                          ONLY : mp_sum, mp_barrier, mp_bcast
  USE mp_images,                   ONLY : my_image_id, nimage, me_image, intra_image_comm
  USE gvect,                       ONLY : ngm, g, gcutm, nl
  USE gvecs,                       ONLY : nls
  USE fft_base,                    ONLY : dffts
  USE fft_interfaces,              ONLY : invfft, fwfft
  USE buffers,                     ONLY : get_buffer, save_buffer, open_buffer
  USE uspp,                        ONLY : nkb, vkb
  USE ener,                        ONLY : etot, ehart, eband, ewld, etxc, deband, etxcc
  USE constants,                   ONLY : e2
  USE tddft_module
  USE fde
  use fde_routines
  IMPLICIT NONE

Public :: Kick_Wavefunction

contains

!
! *******************************************************************************
!
!   Kick_Wavefunction is the driver for kicking the wf
!
! *******************************************************************************
!
subroutine Kick_Wavefunction (PulseType,direction)
  integer, intent(in) :: PulseType, direction
  integer :: ik


     do ik = 1,nks
        current_k = ik
        ! initialize at k-point k 
        CALL start_clock('init_k')
        call gk_sort(xk(1,ik), ngm, g, ecutwfc/tpiba2, npw, igk, g2kin)
        g2kin = g2kin * tpiba2
        call init_us_2(npw, igk, xk(1,ik), vkb)
        CALL stop_clock('init_k')
        ! some output
          if (iverbosity > 0) then
            write(stdout,'(/a,1x,I3)') 'Kpoint:',ik
            call flush_unit(stdout)
          endif

        ! Retrieve wavefunction from buffer

        if (istep<1) then
         call get_buffer (evc, nwordwfc, iunwfc, ik)
         tddft_psi(:,:,2) = evc(:,1:nbnd)  ! not shifted
        else
         call get_buffer (evc, nwordwfc, iunevcn, ik)
         call get_buffer (tddft_psi, nwordtdwfc*2, iuntdwfc, ik)
        endif

!========================================================================!
!                           SELECT THE KICK                              !
!========================================================================!
           select case (PulseType)
             case(1)
              write(stdout,'(a)') 'Kicking Wavefunction with E-field pulse' 
              write(stdout,'(a,1x,I6,1x,a,1x,F10.5)') &
                 'Shifting wavefunction at Step=',istep,'e_strength=',e_strength
              ! apply the E-field shift
              write(stdout,'(a,1x,I1)') 'Along direction', direction 
              call apply_electric_field(tddft_psi(:,:,1),direction)
             case(2-6)
              if (iverbosity>10) write(stdout,'(a)') 'Pumping the Field in update_ham'
             case(7-9)
              call errore ('Kick not implemented')
             case(10)
              write(stdout,'(a)') 'Kicking Wavefunction with E-Field Gradient pulse' 
              write(stdout,'(a,1x,I6,1x,a,1x,F10.5)') &
                 'Shifting wavefunction at Step=',istep,'e_strength=',e_strength
              ! apply the E-field shift
              if (l_twoph) call errore ('Two-photon quadrupole response not implemented')
              write(stdout,'(a,2(1x,I1))') 'Along directions', q_direction1, q_direction2
              call apply_efg(tddft_psi(:,:,1))
             case default
              call errore ('Kick not implemented')
           end select
!========================================================================!
!                       DONE SELECTING THE KICK                          !
!========================================================================!

        ! save wavefunctions to disk
        call save_buffer (tddft_psi(:,:,1), nwordwfc, iunevcn, ik)
        call save_buffer (tddft_psi, nwordtdwfc*2, iuntdwfc, ik)
     enddo

end subroutine Kick_Wavefunction
!
! *******************************************************************************
!
SUBROUTINE apply_electric_field(tddftpsi,direction)
  !
  implicit none

  integer,      intent(in) :: direction
  complex(dp), intent(inout) :: tddftpsi(npwx,nbnd)
  
  integer :: ik, ibnd, ir
  complex(dp) :: psic(dffts%nnr)
  real(dp) :: phase 
  
 ! The k-point loop is carried out outside of this routine
  ik = current_k
  do ibnd = 1, nbnd
    ! transform wavefunction from reciprocal space into real space
    psic = (0.d0, 0.d0)
    psic(nls(igk(1:npw))) = evc(1:npw, ibnd)
    call invfft ('Wave', psic, dffts)  

!$omp parallel do
       do ir = 1, dffts%nnr
         ! This is an approximation if ULTRASOFT are used
         phase = -e2* e_strength * r_pos_s(direction,ir) * alat  
         psic(ir) = psic(ir) * cmplx(cos(phase),sin(phase),dp)
       enddo
!$omp end parallel do

    ! back to reciprocal space
    call fwfft ('Wave', psic, dffts)  
       
    ! save in main QE band buffer evc
    evc(1:npw,ibnd) = psic(nls(igk(1:npw)))
  enddo
    
  ! save buffer evc on disk
  call save_buffer (evc, nwordwfc, iunevcn, ik)
  tddftpsi(1:npwx,1:nbnd) = evc(1:npwx,1:nbnd)
    
END SUBROUTINE apply_electric_field
!
! *******************************************************************************
!
SUBROUTINE apply_efg(tddftpsi)
  !
  implicit none

  complex(dp), intent(inout) :: tddftpsi(npwx,nbnd)
  integer     :: ik, ibnd, ir, jr
  complex(dp) :: psic(dffts%nnr)
  real(dp)    :: phase(dffts%nnr)
  logical     :: is_diagonal
  


  is_diagonal=(q_direction1==q_direction2)

!$omp parallel do
       do ir = 1, dffts%nnr
         ! This is an approximation if ULTRASOFT are used
         if (is_diagonal) then
            phase(ir) = - (1.0d0/6.0d0) * (  e2 * e_strength * &
                        (3.0d0 * r_pos_s(q_direction1,ir) * r_pos_s(q_direction2,ir) - &
                          (r_pos_s(1,ir)**2 + r_pos_s(2,ir)**2 + r_pos_s(3,ir)**2) ) * alat * alat ) 
         else
            phase(ir) = - (1.0d0/6.0d0) * ( 3.0d0 * e2 * e_strength * r_pos_s(q_direction1,ir) * &
           & r_pos_s(q_direction2,ir) * alat * alat )
         endif
       enddo
!$omp end parallel do

 ! The k-point loop is carried out outside of this routine
  ik = current_k
  do ibnd = 1, nbnd
    ! transform wavefunction from reciprocal space into real space
    psic = (0.d0, 0.d0)
    psic(nls(igk(1:npw))) = evc(1:npw, ibnd)
    call invfft ('Wave', psic, dffts)  

!$omp parallel do
       do ir = 1, dffts%nnr
          psic(ir) = psic(ir) * cmplx(cos(phase(ir)),sin(phase(ir)),dp)
       enddo
!$omp end parallel do

    ! back to reciprocal space
    call fwfft ('Wave', psic, dffts)  
       
    ! save in main QE band buffer evc
    evc(1:npw,ibnd) = psic(nls(igk(1:npw)))
  enddo
    
  ! save buffer evc on disk
  call save_buffer (evc, nwordwfc, iunevcn, ik)
  tddftpsi(1:npwx,1:nbnd) = evc(1:npwx,1:nbnd)
    
END SUBROUTINE apply_efg

end module kick_wf
