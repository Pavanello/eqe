
!----------------------------------------------------------------------------
SUBROUTINE update_rho_new (density, density_tot, density_tot_large, comm)
  !----------------------------------------------------------------------------
  ! Calculates the rho_fde at t0 but only using the coupled subsystems
  !
  !  Subroutine to update rho_fde. If total is .true. rho_fde is the sum of the densities
  !  of the fragments. If total is .false. rho_fde of each image is only updated with the 
  !  contribution of the new rhoin of the single fragment
  !
  use mp,                       only : mp_sum, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use fde
  use fde_routines
  use scf,                      only : scf_type, scf_type_copy
  use scf_large,                only : scf_type_large => scf_type
  use io_global,                only : stdout, ionode, ionode_id
  USE fft_interfaces,       ONLY : fwfft, invfft
  use fft_base,                 only : dfftp, grid_gather, grid_scatter, dfftl,  &
                                       grid_gather_large, grid_scatter_large
  use gvect,                    only : ngm, nl
  use gvecl,                    only : ngml => ngm, nll => nl
  use lsda_mod,                 only : nspin
  use tddft_module
  use mp_images,                only : my_image_id

  implicit none
  type(scf_type), intent(in) :: density! scf_type, either rho or rhoin
  type(scf_type), intent(inout) ::  density_tot ! scf_type, either rho or rhoin
  type(scf_type_large), intent(inout) :: density_tot_large
  integer             :: is
  integer, intent(in) :: comm
  real(dp), allocatable :: raux2(:)
  real(dp), allocatable :: raux2l(:)
  complex(dp), allocatable :: gaux(:)
  complex(dp), allocatable :: gauxl(:)
  
   call start_clock('update_rho_new')
        if (iverbosity>10) write(stdout,*) 'TOTAL FDE DENSITY UPDATE in update_rho_new'
        if (fde_frag_nspin == fde_nspin) then
           call scf_type_copy(density, density_tot)
        else
           density_tot%of_r(:,1) = 0.5_dp * density%of_r(:,1)
           density_tot%of_r(:,2) = density_tot%of_r(:,1)
           density_tot%of_g(:,1) = 0.5_dp * density%of_g(:,1)
           density_tot%of_g(:,2) = density_tot%of_g(:,1)
        endif
        if (ionode) allocate(raux2(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
        if (ionode.and.linterlock) allocate(raux2l(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
        if (linterlock) allocate( gaux(dfftp%nnr), gauxl(dfftl%nnr))
        do is=1,fde_nspin
        !
           call grid_gather(density_tot%of_r(:,is), raux2)
           if (linterlock) then
              if (ionode) then
                 raux2l = 0.d0
                 raux2l(f2l(:)) = raux2(:)
                 call mp_sum(raux2l, comm)
                 if (.not. fde_dotsonlarge) then
                    raux2 = 0.d0
                    raux2(:) = raux2l(f2l(:))
                 endif
               endif
               !
               call grid_scatter_large(raux2l, density_tot_large%of_r(:,is))

               gauxl(:) = cmplx(density_tot_large%of_r(:,is), 0.d0, kind=dp)
               call fwfft('Custom', gauxl, dfftl)
               density_tot_large%of_g(1:ngml,is) = gauxl(nll(1:ngml))
               !
               if (.not.fde_dotsonlarge) then
                  call grid_scatter(raux2, density_tot%of_r(:,is))
                  gaux(:) = cmplx(density_tot%of_r(:,is), 0.d0, kind=dp)
                  call fwfft ('Dense', gaux, dfftp)
                  density_tot%of_g(1:ngm,is) = gaux(nl(1:ngm))
               endif
            else
               if (ionode) call mp_sum(raux2, comm)
               call grid_scatter(raux2, density_tot%of_r(:,is))
           !
               call c_grid_gather_sum_scatter_coupled(density_tot%of_g(:,is),comm)
           !
            endif
        end do
        if (ionode) deallocate(raux2)
        if (linterlock) then
            deallocate(gaux, gauxl)
            if (ionode) deallocate(raux2l)
        endif

   call stop_clock('update_rho_new')
   return

END SUBROUTINE update_rho_new


!----------------------------------------------------------------------------
SUBROUTINE update_rho_new_chain (density, density_tot, density_tot_large, comm)
  !----------------------------------------------------------------------------
  ! Calculates the rho_fde at t0 but only using the coupled subsystems
  !
  !  Subroutine to update rho_fde. If total is .true. rho_fde is the sum of the densities
  !  of the fragments. If total is .false. rho_fde of each image is only updated with the 
  !  contribution of the new rhoin of the single fragment
  !
  use mp,                       only : mp_sum, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use fde
  use scf,                      only : scf_type, scf_type_copy, create_scf_type, destroy_scf_type
  use scf_large,                      only : scf_type_large => scf_type 
  use io_global,                only : stdout, ionode, ionode_id
  use mp_images,                only : my_image_id
  use fft_interfaces,           only : fwfft, invfft
  use fft_base,                 only : dfftp, grid_gather, grid_scatter, dfftl, &
                                       grid_gather_large, grid_scatter_large
  use lsda_mod,                 only : nspin
  use tddft_module
  use gvect,                    only : ngm, nl
  use gvecl,                    only : ngml => ngm, nll => nl

  implicit none
  type(scf_type), intent(in) :: density! scf_type, either rho or rhoin
  type(scf_type), intent(inout) ::  density_tot ! scf_type, either rho or rhoin
  type(scf_type_large), intent(inout) :: density_tot_large
  integer             :: is,i
  integer, intent(in) :: comm
  real(dp), allocatable :: raux1(:), tmp_of_r(:,:,:), tmp_of_r_large(:,:,:)
  complex(dp), allocatable :: tmp_of_g(:,:,:), tmp_of_g_large(:,:,:)
  real(dp), allocatable :: raux1l(:)
  complex(dp), allocatable :: gaux(:)
  complex(dp), allocatable :: gauxl(:)
  
  
   call start_clock('update_rho_chain')
        if (iverbosity>10) write(stdout,*) 'CHAIN FDE DENSITY UPDATE'
        allocate(tmp_of_r(dfftp%nnr,nlink,nspin))
        allocate(tmp_of_g(ngm,nlink,nspin))
        if (linterlock) then
           allocate(tmp_of_r_large(dfftl%nnr,nlink,nspin))
           allocate(tmp_of_g_large(ngml,nlink,nspin))
        endif
        do i=1,nlink
           if (fde_frag_nspin == fde_nspin) then
               tmp_of_r(:,i,:) = density%of_r(:,:)*my_chain(i)
               tmp_of_g(:,i,:) = density%of_g(:,:)*my_chain(i)
           else              
               tmp_of_r(:,i,1) = 0.5_dp * density%of_r(:,1)*my_chain(i)
               tmp_of_r(:,i,2) = tmp_of_r(:,1,i)
               tmp_of_g(:,i,1) = 0.5_dp* density%of_g(:,1)*my_chain(i)
               tmp_of_g(:,i,2) = tmp_of_g(:,1,i)
           endif
        enddo
        do i=1,nlink
           if (ionode) allocate(raux1(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
           if (ionode.and.linterlock) allocate(raux1l(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
           if (linterlock) allocate( gaux(dfftp%nnr), gauxl(dfftl%nnr))
           do is=1,fde_nspin
              !
              call grid_gather(tmp_of_r(:,i,is), raux1)
              if (linterlock) then
                 if (ionode) then
                    raux1l = 0.d0
                    raux1l(f2l(:)) = raux1(:)
                    call mp_sum(raux1l, comm)
                    if (.not. fde_dotsonlarge) then
                       raux1 = 0.d0
                       raux1(:) = raux1l(f2l(:))
                    endif
                  endif
               !
                  call grid_scatter_large(raux1l, tmp_of_r_large(:,i,is))

                  gauxl(:) = cmplx(tmp_of_r_large(:,i,is), 0.d0, kind=dp)
                  call fwfft('Custom', gauxl, dfftl)
                  tmp_of_g_large(1:ngml,i,is) = gauxl(nll(1:ngml))
               !
                  if (.not.fde_dotsonlarge) then
                     call grid_scatter(raux1, tmp_of_r(:,i,is))
                     gaux(:) = cmplx(tmp_of_r(:,i,is), 0.d0, kind=dp)
                     call fwfft ('Dense', gaux, dfftp)
                     tmp_of_g(1:ngm,i,is) = gaux(nl(1:ngm))
                  endif
            else ! linterlock
              if (ionode) call mp_sum(raux1, comm)
              call grid_scatter(raux1, tmp_of_r(:,i,is))
              !
              call c_grid_gather_sum_scatter_coupled(tmp_of_g(:,i,is),comm)
              !
            endif
           end do
           if (ionode) deallocate(raux1)
           if (ionode.and.linterlock) deallocate(raux1l)
           if (linterlock) deallocate(gaux,gauxl)
        enddo
        density_tot%of_r(:,:)=tmp_of_r(:,my_link,:)
        density_tot%of_g(:,:)=tmp_of_g(:,my_link,:)
        if (linterlock) then
           density_tot_large%of_r(:,:)=tmp_of_r_large(:,my_link,:)
           density_tot_large%of_g(:,:)=tmp_of_g_large(:,my_link,:)
        endif
        deallocate(tmp_of_r,tmp_of_g)
         if (linterlock) deallocate(tmp_of_r_large,tmp_of_g_large)
   
        

   call stop_clock('update_rho_chain')
   return

END SUBROUTINE update_rho_new_chain








