
!----------------------------------------------------------------------------
SUBROUTINE update_rho_coupled (density, total,comm)
  !----------------------------------------------------------------------------
  ! MODIFICATION OF UPDATE_RHO_FDE FROM PW, BUT USING A DIFFERENT COMMUNICATOR, INTER_COUPLED_COMM
  ! WHICH REPLACES INTER_FRAGMENT_COMM, CONTAINING ONLY THE COUPLED SUBSYSTEMS
  !
  !  Subroutine to update rho_fde. If total is .true. rho_fde is the sum of the densities
  !  of the fragments. If total is .false. rho_fde of each image is only updated with the 
  !  contribution of the new rhoin of the single fragment
  !
  use mp,                       only : mp_sum, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use fde
  use fde_routines
  use scf,                      only : scf_type, scf_type_copy
  use scf_large,                only : scf_type_large => scf_type
  use io_global,                only : stdout, ionode, ionode_id
  use fft_interfaces,           only : fwfft, invfft
  use fft_base,                 only : dfftp, grid_gather, grid_scatter, dfftl, &
                                       grid_gather_large, grid_scatter_large
  use gvect,                    only : ngm, nl
  use gvecl,                    only : ngml => ngm, nll => nl
  use lsda_mod,                 only : nspin
  use mp_images,                only : my_image_id
  use control_flags,            only : iverbosity
  use tddft_module
  USE command_line_options, ONLY:  fancy_parallel_

  implicit none
  type(scf_type), intent(in) :: density ! scf_type, either rho or rhoin
  logical, intent(in) :: total
  integer             :: is
  integer, intent(in) :: comm
  real(dp), allocatable :: raux1(:), rho_old_large(:,:), density_large(:,:), raux(:,:)
  real(dp), allocatable :: raux1l(:)
  complex(dp), allocatable :: gaux(:)
  complex(dp), allocatable :: gauxl(:)
  
   call start_clock('update_rho')
     if (total) then
        if (iverbosity>10) write(stdout,*) 'TOTAL FDE DENSITY UPDATE in update_rho_coupled'
        if (fde_frag_nspin == fde_nspin) then
           call scf_type_copy(density, rho_coupled)
        else
           rho_coupled%of_r(:,1) = 0.5_dp * density%of_r(:,1)
           rho_coupled%of_r(:,2) = rho_coupled%of_r(:,1)
           rho_coupled%of_g(:,1) = 0.5_dp * density%of_g(:,1)
           rho_coupled%of_g(:,2) = rho_coupled%of_g(:,1)
        endif
        if (ionode) allocate(raux1(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
        if (ionode.and.linterlock) allocate(raux1l(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
        if (linterlock) allocate( gaux(dfftp%nnr), gauxl(dfftl%nnr))

        do is=1,fde_nspin
        !
           call grid_gather(rho_coupled%of_r(:,is), raux1)
           if (linterlock) then
              if (ionode) then
                 raux1l = 0.d0
                 raux1l(f2l(:)) = raux1(:)
                 call mp_sum(raux1l, comm)
                 if (.not. fde_dotsonlarge) then
                    raux1 = 0.d0
                    raux1(:) = raux1l(f2l(:))
                 endif
               endif
               !
               call grid_scatter_large(raux1l, rho_coupled_large%of_r(:,is))

               gauxl(:) = cmplx(rho_coupled_large%of_r(:,is), 0.d0, kind=dp)
               call fwfft('Custom', gauxl, dfftl)
               rho_coupled_large%of_g(1:ngml,is) = gauxl(nll(1:ngml))
               !
               if (.not.fde_dotsonlarge) then
                  call grid_scatter(raux1, rho_coupled%of_r(:,is))
                  gaux(:) = cmplx(rho_coupled%of_r(:,is), 0.d0, kind=dp)
                  call fwfft ('Dense', gaux, dfftp)
                  rho_coupled%of_g(1:ngm,is) = gaux(nl(1:ngm))
               endif
            else ! linterlock
           !
              if (ionode) call mp_sum(raux1, comm)
              call grid_scatter(raux1, rho_coupled%of_r(:,is))
           !
              call c_grid_gather_sum_scatter_coupled(rho_coupled%of_g(:,is),comm)
            endif
           !
        end do

        if (ionode) deallocate(raux1)
        if (linterlock) then
            deallocate(gaux, gauxl)
            if (ionode) deallocate(raux1l)
        endif
! OK for open shell because rho_coupled and rho_frozen are sums over all fragments
         rho_fde%of_r = rho_coupled%of_r + rho_frozen%of_r
         rho_fde%of_g = rho_coupled%of_g + rho_frozen%of_g
         if (linterlock) then
             rho_fde_large%of_r = rho_coupled_large%of_r + rho_frozen_large%of_r
             rho_fde_large%of_g = rho_coupled_large%of_g + rho_frozen_large%of_g
         endif

     else ! if total
        if (iverbosity>10) write(stdout,*) 'PARTIAL FDE DENSITY UPDATE in update_rho_coupled'
        if (linterlock) then
           if (fancy_parallel_) call errore('update_rho_coupled','Partial density update can only be combined with -nfp',1)
!           allocate(rho_old_large(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x,fde_frag_nspin)) ! to hold rho_old on large grid
!           allocate(density_large(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x,fde_frag_nspin)) ! to hold density on large grid
           allocate(rho_old_large(dfftl%nnr,fde_frag_nspin)) ! to hold rho_old on large grid
           allocate(density_large(dfftl%nnr,fde_frag_nspin)) ! to hold density on large grid
           allocate(gauxl(dfftl%nnr)) ! to hold rho_old on large grid
           rho_old_large(:,:)=0.d0
           density_large(:,:)=0.d0
!           rho_old_large(f2l(:),:)=rho_old%of_r(:,:)
!           density_large(f2l(:),:)=density%of_r(:,:)
           do is=1,nspin
           call copy_pot_f2l(rho_old%of_r(:,is),rho_old_large(:,is))
           call copy_pot_f2l(density%of_r(:,is),density_large(:,is))
           enddo
            
           if (fde_frag_nspin == fde_nspin) then
              rho_fde_large%of_r = rho_fde_large%of_r - rho_old_large + density_large
              
           else
             rho_fde_large%of_r(:,1)=rho_fde_large%of_r(:,1) - 0.5_dp * (rho_old_large(:,1) - density_large(:,1)) 
             rho_fde_large%of_r(:,2)=rho_fde_large%of_r(:,2) - 0.5_dp * (rho_old_large(:,1) - density_large(:,1)) 
        endif
! now transform rho_fde_large to g-space
        do is=1,fde_nspin
           gauxl(:)=cmplx(rho_fde_large%of_r(:,is),0.d0,kind=dp)
           call fwfft('Custom', gauxl, dfftl)
           rho_fde_large%of_g(1:ngml,is)=gauxl(nll(1:ngml))
         enddo
        else ! linterlock
            if (fde_frag_nspin == fde_nspin) then
               rho_fde%of_r = rho_fde%of_r - rho_old%of_r + density%of_r
               rho_fde%of_g = rho_fde%of_g - rho_old%of_g + density%of_g
            else
               rho_fde%of_r(:,1) = rho_fde%of_r(:,1) - 0.5_dp * (rho_old%of_r(:,1) - density%of_r(:,1))
               rho_fde%of_r(:,2) = rho_fde%of_r(:,2) - 0.5_dp * (rho_old%of_r(:,1) - density%of_r(:,1))
               rho_fde%of_g(:,1) = rho_fde%of_g(:,1) - 0.5_dp * (rho_old%of_g(:,1) - density%of_g(:,1))
               rho_fde%of_g(:,2) = rho_fde%of_g(:,2) - 0.5_dp * (rho_old%of_g(:,1) - density%of_g(:,1))
            endif
        endif ! linterlock
        deallocate(rho_old_large,density_large,gauxl)
     end if

   call stop_clock('update_rho')
   return

END SUBROUTINE update_rho_coupled
!----------------------------------------------------------------------------
SUBROUTINE update_rho_t0 (density, comm)
  !----------------------------------------------------------------------------
  ! Calculates the rho_fde at t0 but only using the coupled subsystems
  !
  !  Subroutine to update rho_fde. If total is .true. rho_fde is the sum of the densities
  !  of the fragments. If total is .false. rho_fde of each image is only updated with the 
  !  contribution of the new rhoin of the single fragment
  !
  use mp,                       only : mp_sum, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use fde
  use scf,                      only : scf_type, scf_type_copy
  use scf_large,                only : scf_type_large => scf_type
  use io_global,                only : stdout, ionode, ionode_id
  use fft_interfaces,           only : fwfft, invfft
  use fft_base,                 only : dfftp, grid_gather, grid_scatter, dfftl, &
                                       grid_gather_large, grid_scatter_large
  use gvect,                    only : ngm, nl
  use gvecl,                    only : ngml => ngm, nll => nl
  use lsda_mod,                 only : nspin
  use mp_images,                only : my_image_id
  use tddft_module

  implicit none
  type(scf_type), intent(in) :: density ! scf_type, either rho or rhoin
  integer             :: is
  integer, intent(in) :: comm
  real(dp), allocatable :: raux1(:)
  real(dp), allocatable :: raux1l(:)
  complex(dp), allocatable :: gaux(:)
  complex(dp), allocatable :: gauxl(:)
  
   call start_clock('update_rho_t0')
        if (iverbosity>10) write(stdout,*) 'TOTAL FDE DENSITY UPDATE in update_rho_t0'
        if (fde_frag_nspin == fde_nspin) then
           call scf_type_copy(density, rho_t0)
        else
           rho_t0%of_r(:,1) = 0.5_dp * density%of_r(:,1)
           rho_t0%of_r(:,2) = rho_t0%of_r(:,1)
           rho_t0%of_g(:,1) = 0.5_dp * density%of_g(:,1)
           rho_t0%of_g(:,2) = rho_t0%of_g(:,1)
        endif
        if (ionode) allocate(raux1(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
        if (ionode.and.linterlock) allocate(raux1l(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
        if (linterlock) allocate( gaux(dfftp%nnr), gauxl(dfftl%nnr))
        do is=1,fde_nspin
           !
           call grid_gather(rho_t0%of_r(:,is), raux1)
           if (linterlock) then
              if (ionode) then
                 raux1l = 0.d0
                 raux1l(f2l(:)) = raux1(:)
                 call mp_sum(raux1l, comm)
                 if (.not. fde_dotsonlarge) then
                    raux1 = 0.d0
                    raux1(:) = raux1l(f2l(:))
                 endif
               endif
               call grid_scatter_large(raux1l, rho_t0_large%of_r(:,is))

               gauxl(:) = cmplx(rho_t0_large%of_r(:,is), 0.d0, kind=dp)
               call fwfft('Custom', gauxl, dfftl)
               rho_t0_large%of_g(1:ngml,is) = gauxl(nll(1:ngml))
               !
               if (.not.fde_dotsonlarge) then
                  call grid_scatter(raux1, rho_t0%of_r(:,is))
                  gaux(:) = cmplx(rho_t0%of_r(:,is), 0.d0, kind=dp)
                  call fwfft ('Dense', gaux, dfftp)
                  rho_t0%of_g(1:ngm,is) = gaux(nl(1:ngm))
               endif
            else ! linterlock
                 if (ionode) call mp_sum(raux1, comm)
                 call grid_scatter(raux1, rho_t0%of_r(:,is))
                 !
                 call c_grid_gather_sum_scatter_coupled(rho_t0%of_g(:,is),comm)
            endif ! linterlock
           !
        end do
        if (ionode) deallocate(raux1)
        if (linterlock) then
            deallocate(gaux, gauxl)
            if (ionode) deallocate(raux1l)
        endif

   call stop_clock('update_rho_t0')
   return

END SUBROUTINE update_rho_t0

!----------------------------------------------------------------------------
SUBROUTINE update_rho_chain (density, total,comm)
  !----------------------------------------------------------------------------
  ! MODIFICATION OF UPDATE_RHO_FDE FROM PW, BUT USING A DIFFERENT COMMUNICATOR, INTER_COUPLED_COMM
  ! WHICH REPLACES INTER_FRAGMENT_COMM, CONTAINING ONLY THE COUPLED SUBSYSTEMS
  !
  !  Subroutine to update rho_fde. If total is .true. rho_fde is the sum of the densities
  !  of the fragments. If total is .false. rho_fde of each image is only updated with the 
  !  contribution of the new rhoin of the single fragment
  !
  use mp,                       only : mp_sum, mp_bcast
  use mp_images,                only : inter_fragment_comm, intra_image_comm
  use fde
  use scf,                      only : scf_type, scf_type_copy
  use scf_large,                only : scf_type_large => scf_type
  use io_global,                only : stdout, ionode, ionode_id
  use lsda_mod,                 only : nspin
  use mp_images,                only : my_image_id
  use control_flags,            only : iverbosity
  use tddft_module
  use fft_interfaces,           only : fwfft, invfft
  use fft_base,                 only : dfftp, grid_gather, grid_scatter, dfftl, &
                                       grid_gather_large, grid_scatter_large
  use gvect,                    only : ngm, nl
  use gvecl,                    only : ngml => ngm, nll => nl

  implicit none
  type(scf_type), intent(in) :: density ! scf_type, either rho or rhoin
  logical, intent(in) :: total
  integer             :: is,i
  integer, intent(in) :: comm
  real(dp), allocatable :: raux1(:), tmp_of_r(:,:,:), tmp_of_r_large(:,:,:)
  complex(dp), allocatable :: tmp_of_g(:,:,:), tmp_of_g_large(:,:,:)
  real(dp), allocatable :: raux1l(:)
  complex(dp), allocatable :: gaux(:)
  complex(dp), allocatable :: gauxl(:)
  
   call start_clock('update_rho_chain')
     if (total) then
        if (iverbosity>10) write(stdout,*) 'CHAIN FDE DENSITY UPDATE'
        allocate(tmp_of_r(dfftp%nnr,nlink,nspin))
        allocate(tmp_of_g(ngm,nlink,nspin))
        if (linterlock) then
           allocate(tmp_of_r_large(dfftl%nnr,nlink,nspin))
           allocate(tmp_of_g_large(ngml,nlink,nspin))
        endif
        do i=1,nlink
           if (fde_frag_nspin == fde_nspin) then
               tmp_of_r(:,i,:) = density%of_r(:,:)*my_chain(i)
               tmp_of_g(:,i,:) = density%of_g(:,:)*my_chain(i)
           else
               tmp_of_r(:,i,1) = 0.5_dp * density%of_r(:,1)*my_chain(i)
               tmp_of_r(:,i,2) = tmp_of_r(:,i,1)
               tmp_of_g(:,i,1) = 0.5_dp* density%of_g(:,1)*my_chain(i)
               tmp_of_g(:,i,2) = tmp_of_g(:,i,1)
           endif
        enddo
        do i=1,nlink
           if (ionode) allocate(raux1(dfftp%nr1x*dfftp%nr2x*dfftp%nr3x))
           if (ionode.and.linterlock) allocate(raux1l(dfftl%nr1x*dfftl%nr2x*dfftl%nr3x))
           if (linterlock) allocate( gaux(dfftp%nnr), gauxl(dfftl%nnr))
           do is=1,fde_nspin
              !
              call grid_gather(tmp_of_r(:,i,is), raux1)
              if (linterlock) then
                 if (ionode) then
                    raux1l = 0.d0
                    raux1l(f2l(:)) = raux1(:)
                    call mp_sum(raux1l, comm)
                    if (.not. fde_dotsonlarge) then
                       raux1 = 0.d0
                       raux1(:) = raux1l(f2l(:))
                    endif
                  endif
               !
                  call grid_scatter_large(raux1l, tmp_of_r_large(:,i,is))

                  gauxl(:) = cmplx(tmp_of_r_large(:,i,is), 0.d0, kind=dp)
                  call fwfft('Custom', gauxl, dfftl)
                  tmp_of_g_large(1:ngml,i,is) = gauxl(nll(1:ngml))
               !
                  if (.not.fde_dotsonlarge) then
                     call grid_scatter(raux1, tmp_of_r(:,i,is))
                     gaux(:) = cmplx(tmp_of_r(:,i,is), 0.d0, kind=dp)
                     call fwfft ('Dense', gaux, dfftp)
                     tmp_of_g(1:ngm,i,is) = gaux(nl(1:ngm))
                  endif
            else ! linterlock
              if (ionode) call mp_sum(raux1, comm)
              call grid_scatter(raux1, tmp_of_r(:,i,is))
              !
              call c_grid_gather_sum_scatter_coupled(tmp_of_g(:,i,is),comm)
              !
            endif ! linterlock
           end do
           if (ionode) deallocate(raux1)
           if (ionode.and.linterlock) deallocate(raux1l)
           if (linterlock) deallocate(gaux,gauxl)
        enddo
! OK for open shell, all densities are already sums over fragments
          rho_coupled%of_r(:,:)=tmp_of_r(:,my_link,:)
          rho_coupled%of_g(:,:)=tmp_of_g(:,my_link,:)
         rho_fde%of_r = rho_coupled%of_r + rho_frozen%of_r
         rho_fde%of_g = rho_coupled%of_g + rho_frozen%of_g
         if (linterlock) then
             rho_coupled_large%of_r(:,:)=tmp_of_r_large(:,my_link,:)
             rho_coupled_large%of_g(:,:)=tmp_of_g_large(:,my_link,:)
             rho_fde_large%of_r = rho_coupled_large%of_r + rho_frozen_large%of_r
             rho_fde_large%of_g = rho_coupled_large%of_g + rho_frozen_large%of_g
         endif

         deallocate(tmp_of_r,tmp_of_g)
         if (linterlock) deallocate(tmp_of_r_large,tmp_of_g_large)
     else
!        if (iverbosity>10) write(stdout,*) 'Chain option uncombinable (for now) with coupled=.false., use either frozen&
!        or setup the my_chain correctly'
        call errore('update_rho_chain','Chain option uncombinable (for now) with coupled=.false., &
                    use either frozen or setup the my_chain correctly',1)
        if (fde_frag_nspin == fde_nspin) then
           rho_fde%of_r = rho_fde%of_r - rho_old%of_r + density%of_r
           rho_fde%of_g = rho_fde%of_g - rho_old%of_g + density%of_g
        else
           rho_fde%of_r(:,1) = rho_fde%of_r(:,1) - 0.5_dp * (rho_old%of_r(:,1) - density%of_r(:,1))
           rho_fde%of_r(:,2) = rho_fde%of_r(:,2) - 0.5_dp * (rho_old%of_r(:,1) - density%of_r(:,1))
           rho_fde%of_g(:,1) = rho_fde%of_g(:,1) - 0.5_dp * (rho_old%of_g(:,1) - density%of_g(:,1))
           rho_fde%of_g(:,2) = rho_fde%of_g(:,2) - 0.5_dp * (rho_old%of_g(:,1) - density%of_g(:,1))
        endif
     end if

   call stop_clock('update_rho_chain')
   return

END SUBROUTINE update_rho_chain
!----------------------------------------------------------------------------

!----------------------------------------------------------------------------
SUBROUTINE c_grid_gather_sum_scatter_coupled( vec, commut )
  !----------------------------------------------------------------------------
  !
  ! ... gathers nproc distributed data on the first processor of every pool,
  ! ....... sum the gathered quantity across all the fragments,
  ! ....... scatter the gathered and summed data to the processors of every pool
  !
  ! ... COMPLEX*16  vec  = distributed variable (ngm)
  !
  USE kinds,     ONLY : DP
  USE gvect,     ONLY : ngm, ngm_g, ig_l2g
  USE fft_base,  ONLY : dfftp
  USE mp,        ONLY : mp_sum, mp_barrier, mp_bcast
  USE mp_images, ONLY : inter_fragment_comm
  USE io_global, only : ionode, ionode_id
  !
  IMPLICIT NONE
  !
  COMPLEX(DP), intent(inout) :: vec(ngm)
  COMPLEX(DP), allocatable :: gathered_vec( : )
  !
#if defined (__MPI)
  !
  INTEGER :: i, idx
  integer, intent(in) :: commut
  !
  ! Gathering

  allocate(gathered_vec(ngm_g))
  
  gathered_vec(:) = cmplx(0.d0, 0.d0, kind=dp)
 
  do i = 1, ngm
    idx = ig_l2g(i)
    gathered_vec(idx) = vec(i)
  enddo

  call mp_sum(gathered_vec, dfftp%comm)

  ! Summing across fragments

  if (ionode) call mp_sum(gathered_vec, commut)

  ! Scattering
  call mp_bcast(gathered_vec, ionode_id, dfftp%comm)
  do i = 1, ngm
    idx = ig_l2g(i)
    vec(i) = gathered_vec(idx)
  end do 

  deallocate(gathered_vec)

#else
  CALL errore('c_grid_gather_sum_scatter', 'do not use in serial execution', 1)
#endif
  !
  RETURN
  !
END SUBROUTINE c_grid_gather_sum_scatter_coupled







