## `QUICK START GUIDE`
*plot_optical_spectra* is a python code which generates optical absorption spectra from rt-TDDFT calculations along with other capabilities which are explained in the code itself.

There are two different types of this code:

- **Jupyter notebook.**

- **Python script.**

Both of these generate the spectra in the same way the main difference being in their corresponding interfaces i.e. the python script can be <br \>
executed from the usual terminal or an **IDE** (Interactive Development Environment) like [**Spyder**](https://github.com/spyder-ide/spyder) whereas, the Jupyter notebook requires *Jupyter* web application.

The best way to use *Jupyter* web application is by installing [**Anaconda**](https://www.continuum.io/downloads) python distribution containing all the necessary python packages like scipy, numpy, pandas, matplotlib, etc.

## `Executing the code`

### **Jupyter Notebook**

```
jupyter-notebook plot_optical_spectra.ipynb
```
or
```
jupyter notebook plot_optical_spectra.ipynb
```

### **Python Script**
```
python plot_optical_spectra.py
```
or
```
./plot_optical_spectra.py
```

The ``./plot_optical_spectra.py`` works when this script has executable permission.
```
-rwxr-xr-- 1 133759 109360 14349 Aug 23 13:28 plot_optical_spectra.py
```

