{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## In order to run this code properly (and as it is), jupyter should be installed thorough Anaconda package.\n",
    "\n",
    "### This code generates optical spectra from TDDFT output along with other capabilities.\n",
    "\n",
    "### Few concise definitions before proceeding:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\textbf{Uncoupled Calculations:}$ These correspond to calculations which employs only the one-body response ($\\chi_I^U$) in other words, the embedding potential is computed at each time step only updating the subsystem time-dependent density and leaving the density of the surrounding subsystems frozen at t = 0."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\textbf{Coupled Calculations:}$ These correspond to calculations which employes full many-body response ($\\chi_I^C$).\n",
    "\n",
    "$$\\chi_I^C = \\chi_I^U + \\sum_{J\\neq I}^{N_S}\\chi_I^U K_{IJ} \\chi_I^C$$\n",
    "\n",
    "where, $I$ and $J$ are subscripts denoting subsystems.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\textbf{NOTE:}$ Optical spectra corresponding to Coupled calculations can have negative peaks whereas the spectra for uncoupled calculations should not. (Or atleast relatively very small negative values).\n",
    "\n",
    "For this reason, I have added a section for computing $\\textbf{Outliers}$ (to be used for uncoupled) with the choice of customising the threshold/cutoff to determine which subsystem's uncoupled calculations give rise to negative peaks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A bit of theory:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The definition of frequency-dependent dipole:\n",
    "$$\n",
    "\\text{dip_FFT}=\\delta\\mu(\\omega)=\\int \\delta\\mu(t) e^{-i\\frac{\\omega t}{\\hbar}}dt.\n",
    "$$\n",
    "The backward transform is:\n",
    "$$\n",
    "\\delta\\mu(t) = \\frac{1}{2\\pi\\hbar}\\int \\delta\\mu(\\omega) e^{\\frac{\\omega t}{\\hbar}}d\\omega,\n",
    "$$\n",
    "in this way, units are preserved: \n",
    "    (1) $\\mu(t)$ in $ea_0$ or $Cm$; \n",
    "    (2) $\\mu(\\omega)$ in $e a_0 \\frac{\\hbar}{\\text{Eh}}$ or $s C m$.\n",
    "    \n",
    "Because numpy.fft gives the following Fourier components (norm='None')\n",
    "$$\n",
    "A_k = \\sum_{m=0}^{n-1} a_m \\exp\\left\\{-2\\pi i{mk \\over n}\\right\\} \\qquad k = 0,\\ldots,n-1.\n",
    "$$\n",
    "We must multiply by $dt$ the output of fft. Also, $dt$ must be scaled to reproduce the $i\\omega t$ exponent:\n",
    "$$\n",
    "\\text{numpy.dt} = \\frac{dt}{2\\pi}.\n",
    "$$\n",
    "\n",
    "The signal is given by:\n",
    "$$\n",
    "\\text{sigma}=\\sigma(\\omega)=\\frac{2}{\\pi}\\frac{\\delta\\mu}{\\delta E}\n",
    "$$ \n",
    "The strength function is:\n",
    "$$\n",
    "\\text{S} = \\omega\\sigma(\\omega).\n",
    "$$ \n",
    "The sum rule (number of active electrons) is given by\n",
    "$$\n",
    "N_\\text{active}=\\int S(\\omega)d \\omega.\n",
    "$$\n",
    "The $\\omega=0$ polarizability becomes:\n",
    "$$\n",
    "\\text{alpha}=\\alpha(0)=\\int\\frac{S(\\omega)}{\\omega^2}d \\omega\n",
    "$$\n",
    "or it should also be given by: \n",
    "$$\n",
    "\\alpha(i\\omega)=\\frac{3}{\\pi}\\int\\delta\\mu(t) e^{-\\omega t}dt,\n",
    "$$\n",
    "but because the value of $\\alpha(i\\omega)$ at $\\omega=0$ cannot be computed numerically (random number generation...), we identify it as\n",
    "$$\n",
    "\\alpha(0)\\simeq \\max_{\\omega} \\alpha(i\\omega)\n",
    "$$\n",
    "and then check against the one computed from the sum rules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Workflow:\n",
    "\n",
    "1. The following cell is where the fun computation starts.\n",
    "\n",
    "2. But before that, user must provide details about the TDDFT calculations on which this code is about to be executed. For eg. number of subsystems, number of directions along which the electric field was subjected to, etc.\n",
    "    These inputs should be fairly obvious once the code is executed and the user input section starts.\n",
    "\n",
    "3. At the end of the computations, a basic interactive plot of the optical absorption spectra is generated and if given the option similar spectra for $\\alpha_{i\\omega}$ is also generated.\n",
    "\n",
    "4. Ability to smooth over the rough plots (for asthetic reasons) is also available towards the end of the code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import sys\n",
    "from __future__ import print_function\n",
    "\n",
    "from ipywidgets import interact\n",
    "import ipywidgets as widgets\n",
    "\n",
    "from scipy.signal import savgol_filter as savitzky_golay # This is the smoothing function.\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Important user inputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Parameters needed for optical spectrum computation to be extraceted from TDDFT input file or output (User's choice).\n",
    "# Either input or output (case insensitive)!!\n",
    "\n",
    "print (\"Parameters from input or output?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    choice1 = str(raw_input())\n",
    "else:\n",
    "    choice1 = str(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Number of subsystems. Just an integer number.\n",
    "\n",
    "print (\"Number of subsystems?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    subsystems = int(raw_input())\n",
    "else:\n",
    "    subsystems = int(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Number of directions to be covered in this single execution of code.\n",
    "\n",
    "print (\"Number of directions?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    directions = int(raw_input())\n",
    "else:\n",
    "    directions = int(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Path leading to directory containing outputs corresponding to each direction given separately.\n",
    "\n",
    "print (\"Path to the outputs (assuming these are in separate folders for each direction)\\n# For ex. 1. Output file directory = \\\"../../x_dir/Input.tddft_0.out\\\" then path = \\\"../../x_dir/\\\"\\n# 2. If output file and this code are in the same dir. then path = \\\"./\\\"\\n\")\n",
    "paths = []\n",
    "for i in range(directions):\n",
    "    if sys.version_info[0] < 3:\n",
    "        paths.append(str(raw_input()))\n",
    "    else:\n",
    "        paths.append(str(input()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Prefix of the output file name succeeding by _<subsystem number>.out\n",
    "# For ex. Output file name = \"Input.tddft_0.out\" then prefix = \"Input.tddft\"\n",
    "\n",
    "print (\"Prefix of outputs (assuming prefix follows with _<subsytem number>.out)\\n# For ex. Output file name = \\\"Input.tddft_0.out\\\" then prefix = \\\"Input.tddft\\\"\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    prefix = str(raw_input())\n",
    "else:\n",
    "    prefix = str(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"Want to compute alpha_iw (y/n)?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    alpha_iw_choice = str(raw_input())\n",
    "else:\n",
    "    alpha_iw_choice = str(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"Want to compute sumrule (y/n)?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    sumrule_choice = str(raw_input())\n",
    "else:\n",
    "    sumrule_choice = str(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"Want to compute polarizabilities (alpha(0)) (y/n)?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    polarizability_choice = str(raw_input())\n",
    "else:\n",
    "    polarizability_choice = str(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "outliers_cutoff = 0.1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"Want to compute outliers for uncoupled calculations (y/n)?\\n\")\n",
    "if sys.version_info[0] < 3:\n",
    "    outliers_choice = str(raw_input())\n",
    "else:\n",
    "    outliers_choice = str(input())\n",
    "\n",
    "if (outliers_choice.lower() == 'y'):\n",
    "    print (\"\\nDefault value for outliers is %f, do you want to change this value? (y/n)\\n\" % (outliers_cutoff))\n",
    "    if sys.version_info[0] < 3:\n",
    "        outliers_cutoff_choice = str(raw_input())\n",
    "    else:\n",
    "        outliers_cutoff_choice = str(input())\n",
    "    if outliers_cutoff_choice.lower() == 'y':\n",
    "        print (\"\\nCutoff for outliers (absolute value):\\n\")\n",
    "        if sys.version_info[0] < 3:\n",
    "            outliers_cutoff = float(raw_input())\n",
    "        else:\n",
    "            outliers_cutoff = float(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "damping_factor = 0.05 # in eV"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"Damping factor = %f eV. Do you want to change this value? (y/n)\\n\" % (damping_factor))\n",
    "if sys.version_info[0] < 3:\n",
    "    damping_factor_choice = str(raw_input())\n",
    "else:\n",
    "    damping_factor_choice = str(input())\n",
    "\n",
    "if damping_factor_choice.lower() == 'y':\n",
    "    print (\"Damping factor = \")\n",
    "    if sys.version_info[0] < 3:\n",
    "        damping_factor = float(raw_input())\n",
    "    else:\n",
    "        damping_factor = float(input())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constants in SI unit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "eV_IN_J                  = 1.60217733e-19                  # eV/J \n",
    "HBAR                     = 1.054572669125102e-34           # J*s/(2*pi)\n",
    "H_CONST                  = 6.62607004081e-34               # J*s\n",
    "BOHR_TO_A                = 0.529177249                     # Ang / Bohr \n",
    "A_IN_M                   = 1.0e-10                         # meters / Ang  \n",
    "ELECTRON_REST_MASS_IN_KG = 9.1093897e-31                   # Kg / m_e  \n",
    "ELEMENTARY_CHARGE_IN_C   = 1.60217733e-19                  # Coul / e  \n",
    "S_IN_AU                  = 2.41888432650516e-17            # s / a.u.\n",
    "ATTOSECOND_IN_S          = 1.0e-18                         # s / as \n",
    "ATTOSECOND_IN_AU         = ATTOSECOND_IN_S / S_IN_AU       # 0.0413413733366\n",
    "ATTOSECOND_IN_RY_AU      = ATTOSECOND_IN_AU / 2            # 0.0206706866683\n",
    "Eh_IN_eV                 = 27.211385                       # eV / Eh\n",
    "RY_IN_Eh                 = 0.5                             # Eh / Ry\n",
    "RY_IN_eV                 = Eh_IN_eV * RY_IN_Eh             # eV / Ry\n",
    "Eh_IN_J                  = eV_IN_J / Eh_IN_eV              # J / Eh\n",
    "C_IN_AU                  = 137.0359996287515               # au of speed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Damping function.;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def damp_funct(t0, daping_factor):\n",
    "    return np.exp(-(t0) * 2.0 * np.pi * daping_factor/Eh_IN_eV )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def lt_funct(t0, f0):\n",
    "    return np.exp( - t0 * 2.0 * np.pi * f0 )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Function to read dipole moment and other parameters from the output file.;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def dipole_from_file(outfile, choice2):\n",
    "    with open(outfile+'.out','r') as outfil:\n",
    "        lines = outfil.readlines()\n",
    "        data = []\n",
    "        outfil.close()\n",
    "        for i in range(0,len(lines)):\n",
    "            line = lines[i]\n",
    "            if (line[0:3] == 'DIP'):\n",
    "                nline = line.split()\n",
    "                nline = nline[3:]\n",
    "                data.append(nline)               #  data = Dipole Moment\n",
    "                \n",
    "            if choice2.lower() == 'output':\n",
    "                if 'Along direction' in line:\n",
    "                    nline = line.split()[-1]\n",
    "                    e_dir = int(nline)            #  e_dir = Direction along which field is applied\n",
    "                if 'e_strength' in line:\n",
    "                    line2 = lines[i].split()\n",
    "                    e_str = 2*float(line2[-1])            # e_str = Electric field strength in Eh / Bohr -- This need to be figured out\n",
    "                if 'Time step' in line:\n",
    "                    nline = line.split()\n",
    "                    dt_fs = float(nline[3])/0.0207    # dt_fs = Time step in a.s.\n",
    "    \n",
    "    if choice2.lower() == 'input':     \n",
    "        print (\"Input file name taken from prefix provided, by\\n replacing _<subsystem number>.out with _<subsystem number>.in\\n\")\n",
    "        e_dir, e_str, dt_fs = from_input_file(outfile+'.in')\n",
    "        \n",
    "    return data, e_dir, e_str, dt_fs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def from_input_file(inputfile):\n",
    "    with open(inputfile,'r') as infil:\n",
    "        lines = infil.readlines()\n",
    "        infil.close()\n",
    "        for i in range(0,len(lines)):\n",
    "            line = lines[i]\n",
    "            if 'e_direction' in line:\n",
    "                e_dir = int(line.split()[-1])      #  e_dir = Direction along which field is applied\n",
    "            if 'e_strength' in line:\n",
    "                e_str = float(line.split()[-1])    # e_str = Electric field strength in Eh / Bohr -- This need to be figured out\n",
    "            if ' dt ' in line:\n",
    "                #dt_fs = float(nline[3])/0.0207    # dt_fs = Time step in a.s.\n",
    "                dt_fs = float(line.split()[-1])\n",
    "        return e_dir, e_str, dt_fs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optical Absorption Spectra calculation;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fourier Transform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def fft_transform (dipole_data, e_direction, dt_au):\n",
    "    \n",
    "    e_dir = e_direction - 1\n",
    "    \n",
    "    dipole_data = np.array(dipole_data, dtype=np.float)\n",
    "    dip = dipole_data[:,e_dir] - dipole_data[0,e_dir]     # dip in e*bohr\n",
    "    \n",
    "    x = np.linspace(0,len(dip),len(dip),endpoint=False)\n",
    "    d = dt_au / (2*np.pi)\n",
    "    t = x * d             \n",
    "    damp = damp_funct(t, damping_factor)                   # Damping function with unitless argument\n",
    "    \n",
    "    # Multiply dipole difference by damping function\n",
    "    signal = dip * damp\n",
    "    \n",
    "    # Get the frequencies of the FFT and convert to eV\n",
    "    \n",
    "    omega = np.fft.rfftfreq(len(signal),d=d)    # E = 2 pi / dt\n",
    "    d_omega = (omega[2] - omega[1])\n",
    "    \n",
    "    # Calculate the FFT\n",
    "    dip_FFT = np.fft.rfft(signal) * dt_au\n",
    "    return dip_FFT, omega, d_omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Laplace Transform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def laplace_transform (dipole_data, e_direction, dt_au):\n",
    "    \n",
    "    e_dir = e_direction - 1\n",
    "    \n",
    "    dipole_data = np.array(dipole_data, dtype=np.float)\n",
    "    dip = dipole_data[:,e_dir] - dipole_data[0,e_dir]     # dip in e*bohr\n",
    "    \n",
    "    x = np.linspace(0,len(dip),len(dip),endpoint=False)\n",
    "    d = dt_au / (2*np.pi)\n",
    "    t = x * d             \n",
    "    \n",
    "    damp = damp_funct(t, damping_factor)                   # Damping function with unitless argument\n",
    "    \n",
    "    # Multiply dipole difference by damping function\n",
    "    \n",
    "    signal = dip * damp\n",
    "    \n",
    "    # Get the frequencies of the FFT and convert to eV\n",
    "    omega = np.fft.rfftfreq(len(signal),d=d)    # E = 2 pi / dt\n",
    "\n",
    "    dip_LT = np.zeros(len(omega)) + 1j * np.zeros(len(omega))\n",
    "    for i in range(0,len(omega)):\n",
    "        dip_LT[i]  = 3.0 * np.sum(lt_funct(t[:], omega[i]) * signal[:]) * dt_au / np.pi\n",
    "    \n",
    "    return dip_LT, omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## TDDFT Output file;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Optical Spectra"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "final_strength = []\n",
    "final_alpha_iw = []\n",
    "outliers = []\n",
    "\n",
    "for i in range(subsystems):\n",
    "    \n",
    "    S = []\n",
    "    sumrule = []\n",
    "    polarizability = []\n",
    "    dip_lt_iw = []\n",
    "    print (\"%i\" % i, end=\"\\r\")\n",
    "    for j in range(directions):\n",
    "        outputfile = paths[j] + \"/\" + prefix + \"_\" + str(i)\n",
    "        print (outputfile)\n",
    "        dipole_data, e_direction, e_strength, dt = dipole_from_file(outputfile, choice1)\n",
    "        \n",
    "        dt_au = dt * ATTOSECOND_IN_AU\n",
    "    \n",
    "        dip_FFT, omega, d_omega = fft_transform(dipole_data, e_direction, dt_au)\n",
    "    \n",
    "        sigma = dip_FFT * 2. / e_strength / np.pi\n",
    "        \n",
    "        S.append(sigma * omega)\n",
    "        \n",
    "        if sumrule_choice.lower() == 'y':\n",
    "            sumrule.append(np.sum(np.imag(sigma * omega)) * d_omega)\n",
    "        \n",
    "        if polarizability_choice.lower() == 'y':\n",
    "            polarizability.append(np.sum(np.imag(sigma[1:] * omega[1:])/omega[1:]**2) * d_omega)\n",
    "            \n",
    "        if alpha_iw_choice.lower() == 'y':\n",
    "            dip_LT, omega_LT = laplace_transform(dipole_data, e_direction, dt_au)\n",
    "            dip_lt_iw.append(- dip_LT.real / e_strength)\n",
    "\n",
    "    strength = np.mean(np.array(S), axis = 0)\n",
    "    \n",
    "    if sumrule_choice.lower() == 'y':\n",
    "        print (\"Sumrule = \", np.average(sumrule))\n",
    "    \n",
    "    if polarizability_choice.lower() == 'y':\n",
    "        print('alpha(0) = ', np.average(polarizability), 'au (Bohr cube)')\n",
    "        print('alpha(0) = ', np.average(polarizability) * BOHR_TO_A**3, 'Angstroms cube')\n",
    "        \n",
    "    if polarizability_choice.lower() == 'y' and directions == 3:\n",
    "        polarizability_anisotropy = polarizability[2] - 0.5 * (polarizability[0] + polarizability[1])\n",
    "        \n",
    "        print('Anisotropic Polarizability = ', polarizability_anisotropy, 'au (Bohr cube)')\n",
    "        print('Anisotropic Polarizability = ', polarizability_anisotropy * BOHR_TO_A**3, 'Angstroms cube')\n",
    "    \n",
    "    \n",
    "    if alpha_iw_choice.lower() == 'y':\n",
    "        alpha_iw = np.average(np.array(dip_lt_iw), axis = 0)\n",
    "        \n",
    "    if outliers_choice.lower() == 'y':\n",
    "        if (np.abs(np.sort(strength.imag)[0]) >= outliers_cutoff ):\n",
    "            print (\"Outlier: Subsystem number = \", i, \", lowest value = \", np.sort(strength.imag)[0])\n",
    "            outliers.append(i)\n",
    "        else:\n",
    "            print (\"Not an outlier!!\")\n",
    "    print (\"\\n\")\n",
    "    \n",
    "    final_strength.append(strength)\n",
    "    if alpha_iw_choice.lower() == 'y':\n",
    "        final_alpha_iw.append(alpha_iw)\n",
    "\n",
    "final_strength_avg = np.average(np.array(final_strength), axis = 0)\n",
    "\n",
    "if alpha_iw_choice.lower() == 'y': \n",
    "    final_alpha_iw_avg = np.average(np.array(final_alpha_iw), axis = 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Average optical spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def plot_optical(start, finish):\n",
    "    fig, plots = plt.subplots(1, 1)\n",
    "    plots.plot(omega * Eh_IN_eV, final_strength_avg.imag)\n",
    "    plots.set_xlim(start, finish)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "interact(plot_optical, start = (0, 20), finish = (0, 150))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"\\nPath for saving optical_spectra.\\nIf want to save in current directory just type \\\"./\\\"\")\n",
    "if sys.version_info[0] < 3:\n",
    "    strength_file_path = str(raw_input())\n",
    "else:\n",
    "    strength_file_path = str(input())\n",
    "\n",
    "strength_file = open(strength_file_path + \"/\" + \"Optical_spectra.dat\", \"w\")\n",
    "\n",
    "for i in range(len(omega)):\n",
    "    strength_file.write(\"%f\\t%f\\t%f\\n\" % (omega[i] * Eh_IN_eV, final_strength_avg[i].real, final_strength_avg[i].imag))\n",
    "\n",
    "strength_file.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Smoothing out the optical spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print (\"Do you want to smooth the curve? (y/n)\")\n",
    "if sys.version_info[0] < 3:\n",
    "    smooth_choice = str(raw_input())\n",
    "else:\n",
    "    smooth_choice = str(input())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def smooth_plot_optical(windows_size, order, start, finish):\n",
    "    fig, plots = plt.subplots(1, 1)\n",
    "    smooth_S_avg = savitzky_golay(final_strength_avg.imag, window_length = windows_size, polyorder = order)\n",
    "    plots.plot(omega * Eh_IN_eV, smooth_S_avg)\n",
    "    plots.set_xlim(start, finish)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "if smooth_choice.lower() == 'y':\n",
    "    interact(smooth_plot_optical, windows_size = (9, 41, 2), order = (1, 6), start = (0, 20), finish = (0, 150))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "if smooth_choice.lower() == 'y':\n",
    "    print (\"\\nPath for saving smooth optical_spectra.\\nIf want to save in current directory just type \\\"./\\\"\")\n",
    "    if sys.version_info[0] < 3:\n",
    "        smooth_file_path = str(raw_input())\n",
    "    else:\n",
    "        smooth_file_path = str(input())\n",
    "\n",
    "    smooth_file = open(smooth_file_path + \"/\" + \"Smooth_Optical_spectra.dat\", \"w\")\n",
    "\n",
    "    for i in range(len(omega)):\n",
    "        smooth_file.write(\"%f\\t%f\\n\" % (omega[i] * Eh_IN_eV, smooth_S_avg))\n",
    "\n",
    "    smooth_file.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot of $\\alpha(i\\omega)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def plot_alpha_iw(start, finish):\n",
    "    fig, plots = plt.subplots(1, 1)\n",
    "    plots.plot(omega_LT * Eh_IN_eV, final_alpha_iw_avg)\n",
    "    plots.set_xlim(start, finish)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "if alpha_iw_choice.lower() == 'y':\n",
    "    interact(plot_alpha_iw, start = (-1, 20), finish = (0, 150))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "if alpha_iw_choice.lower() == 'y':\n",
    "    print (\"\\nPath for saving Alpha(iw) spectra.\\nIf want to save in current directory just type \\\"./\\\"\")\n",
    "    if sys.version_info[0] < 3:\n",
    "        alpha_file_path = str(raw_input())\n",
    "    else:\n",
    "        alpha_file_path = str(input())\n",
    "\n",
    "    alpha_file = open(alpha_file_path + \"/\" + \"Alpha_iw_spectra.dat\", \"w\")\n",
    "\n",
    "    for i in range(len(omega)):\n",
    "        alpha_file.write(\"%f\\t%f\\n\" % (omega_LT[i] * Eh_IN_eV, final_alpha_iw_avg[i].real))\n",
    "\n",
    "    alpha_file.close()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
