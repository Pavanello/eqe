#!/usr/bin/python
#
## Xiaofeng Qian @ mit, 2010
#
import numpy, sys

#-----------------------------------------------------------#
#
# read the the name of molecule from argument list
if len(sys.argv) > 1:
   molecule = sys.argv[1]
#   prefix = sys.argv[2]
#   dip_filename = 'dip'+molecule 
#   ef_filename = 'ef'+molecule 
   dip_filename = molecule+'.out' 
   molecule_out = molecule
#   molecule_parts = molecule.split('.')
#   molecule_out = molecule_parts[0]
else:
    print 'usage: ./plot.py molecule '
    sys.exit(1)
   
NEgrid2 = 5000
e_strength = 0.02
dt_fs = 2
# Extra parameters for FT
E_begin = 3.5
E_end = 9.5
lfield = False
#print len(sys.argv)
if len(sys.argv) > 2:
    NEgrid2 = int(sys.argv[2])
if len(sys.argv) > 3:
    E_begin = float(sys.argv[3])
if len(sys.argv) > 4:
    E_end = float(sys.argv[4])
# read parameters from the QE TDDFT input file



damping_factor  = 0.010           # damping factor
# Defining output files
dip_output_filename = molecule_out+'.fft'
ef_output_filename = 'ef'+molecule_out+'.fft'
power_output_filename = 'power'+molecule_out+'.fft'
#ef_output_filename = prefix+'-ef'+molecule_out+'.fft'
#power_output_filename = prefix+'-power'+molecule_out+'.fft'
#band_output_filename = prefix+'-band'+molecule_out


#-----------------------------------------------------------#
## constants in SI unit
EV_IN_J = 1.60217733e-19
HBAR    = 1.054572669125102e-34
ATTOSECOND = 1.0e-18
ELECTRON_REST_MASS_IN_KG = 9.1093897e-31
ELEMENTARY_CHARGE_IN_C   = 1.60217733e-19
A_IN_M = 1.0e-10
BOHR_TO_A = 0.529177249 

#-----------------------------------------------------------#
## read dipole from the output file
dp_file = file(dip_filename, "r")
data=[]
edata=[]
bdata=[]
read_band = 0
lines=dp_file.readlines()
for i in range(0,len(lines)):
#for line in dp_file.readlines():
     line = lines[i]
     if read_band == 1:
         nline = line.split()
#        print nline 
         bdata.append(map(float,nline))
         read_band = 0
     if (line[0:3] == 'DIP'):
         nline = line.split()
         nline = nline[3:]
#         print nline 
         data.append(map(float,nline))
     if 'Electric field dir.' in line:
         nline = line.split()
         e_direction = int(nline[4])
#         print e_direction
     if 'e_strength=' in line:
         line2 = lines[i+1]
         e_strength = float(line2) / 0.264586043
         if e_strength < 0.0000001:
            e_strength = 0.0052917720859 / 0.264586043
            print 'Careful, fake e_strength, using 0.0052917720859, check manually if correct with the output of other fragment(s)'
     if 'Time step' in line:
         nline = line.split()
         dt_fs = float(nline[3]) / 0.02065
     if (line[1:7] == 'efield'):
         nline = line.split()
         nline = nline[2:]
#         print real_band
         edata.append(map(float,nline))
     if 'band energies (ev):' in line:
         read_band = 1
    
       
dp_file.close()
dp      = numpy.array(data)
#
#print len(edata)
if len(edata) > 0:
      lfield = True
if lfield:
    ef      = numpy.array(edata)
    NEgrid_e = numpy.shape(ef)[0]
#band    = numpy.array(bdata)
NEgrid = numpy.shape(dp)[0]
#print NEgrid
#print NEgrid_e
dp_diff = dp[:,e_direction-1] -  dp[0,e_direction-1] 
#print dp_diff
#print ef
if lfield:
    ef_diff = ef[:,2]


## setup time and energy grid
S       = numpy.zeros(NEgrid2) + 1j * numpy.zeros(NEgrid2)
E_axis  = numpy.zeros(NEgrid2)
dp_fft = numpy.zeros(NEgrid2) + 1j * numpy.zeros(NEgrid2)
if lfield:
    ef_fft = numpy.zeros(NEgrid2) + 1j * numpy.zeros(NEgrid2)
    power = numpy.zeros(NEgrid2) 

## If external field: perform an FT on the field

dt      = dt_fs * ATTOSECOND * EV_IN_J / HBAR 
nt      = numpy.linspace(0, (NEgrid-1), NEgrid) 
t       = nt * dt
dE      = (E_end - E_begin) / numpy.float(NEgrid2-1)
dp_damping = numpy.exp( - t**2 * damping_factor )
if lfield:
   nt_e     = numpy.linspace(0, (NEgrid_e-1), NEgrid_e) 
   t_e       = nt_e * dt
   dp_damping_e = numpy.exp( - t_e * damping_factor )
for it in range(NEgrid2):
           Eit     = E_begin + dE * it
           E_axis[it] = Eit
           dp_fft[it] =  numpy.sum(dp_diff * dp_damping * numpy.exp( -1j * Eit * t))
           S[it]  = dp_fft[it] * ( Eit ) * 2 * dt /e_strength /numpy.pi
#print dp_fft 
if lfield:
   for it in range(NEgrid2):
           Eit     = E_begin + dE * it
           E_axis[it] = Eit
           ef_fft[it] =  numpy.sum(ef_diff * dp_damping_e * numpy.exp( -1j * Eit * t_e))
           power[it] = abs(dp_fft[it]) * abs(dp_fft[it])

S  = S * ELECTRON_REST_MASS_IN_KG * ELEMENTARY_CHARGE_IN_C * HBAR**(-2) * A_IN_M**2
## check sum_rule = total number of valence electrons
SumRule  = numpy.sum( numpy.imag(S) ) * dE
#print "Sum rule = ", SumRule

f = open(dip_output_filename, "wt")
if lfield:
    g = open(ef_output_filename, "wt")
    h = open(power_output_filename, "wt")
#j = open(band_output_filename, "wt")
## print spectrum in output
for i in xrange(len(E_axis)):
       f.write("%f %f %f\n" % (E_axis[i],  numpy.real(S[i]), numpy.imag(S[i])))
if lfield:
   for i in xrange(len(E_axis)):
       g.write("%f %f %f\n" % (E_axis[i],  numpy.real(ef_fft[i]), numpy.imag(ef_fft[i])))
       h.write("%f %f\n" % (E_axis[i],  numpy.real(power[i])))
#for i in xrange(numpy.shape(band)[0]):
#    for ii in xrange(numpy.shape(band)[1]):
#         if (ii == (numpy.shape(band)[1]-1)):
#                j.write("%8.4f\n" %(band[i,ii]))
#         else:
#                j.write("%8.4f" %(band[i,ii]))
 



