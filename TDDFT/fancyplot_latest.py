#!/usr/bin/python
#
## Xiaofeng Qian @ mit, 2010
#
import numpy, sys

#-----------------------------------------------------------#
#
# read the the name of molecule from argument list
#input_filenames =  [
##'dimer-tddft_x.dat','dimer-tddft_y.dat','dimer-tddft_z.dat',
#'dimer-tddft_y.dat','dimer-tddft_z.dat',
# 'fde-tddft_x_0.dat','fde-tddft_y_0.dat','fde-tddft_z_0.dat',
# 'fde-tddft_x_1.dat','fde-tddft_y_1.dat','fde-tddft_z_1.dat']
#b=[]
#for input_filename in input_filenames:
## read parameters from the QE TDDFT input file
#
#    input_file = file(input_filename, "r")
#    data=[]
#    for line in input_file.readlines():
#        nline = line.split()
#        data.append(map(float,nline))
#    b.append(data)

#dimerx = numpy.array(b[0])
#dimery = numpy.array(b[0])
#dimerz = numpy.array(b[1])
#fde0x = numpy.array(b[2])
#fde0y = numpy.array(b[3])
#fde0z = numpy.array(b[4])
#fde1x = numpy.array(b[5])
#fde1y = numpy.array(b[6])
#fde1z = numpy.array(b[7])
#matrix = np.loadtxt('filename.dat')
dimerx = numpy.loadtxt('dimer-tddft_x.fft')
dimery = numpy.loadtxt('dimer-tddft_y.fft')
dimerz = numpy.loadtxt('dimer-tddft_z.fft')
fde0x = numpy.loadtxt('fde-tddft_x_0.fft')
fde1x = numpy.loadtxt('fde-tddft_x_1.fft')
fde0y = numpy.loadtxt('fde-tddft_y_0.fft')
fde1y = numpy.loadtxt('fde-tddft_y_1.fft')
fde0z = numpy.loadtxt('fde-tddft_z_0.fft')
fde1z = numpy.loadtxt('fde-tddft_z_1.fft')
dimer = (dimerx + dimery + dimerz) / 3
fde0 = (fde0x + fde0y + fde0z) / 3
fde1 = (fde1x + fde1y + fde1z) / 3
#print dimer
#print mono1


# Extra parameters for FT
E_begin = 0
#E_end = dimer6[-1,0]
E_end = 10.
# Defining output files
oas_figure_filename1 = 'dimerx_fdex.png'
oas_figure_filename2 = 'dimery_fdey.png'
oas_figure_filename3 = 'dimerz_fdez.png'
oas_figure_filename4 = 'dimer_fde.png'
oas_figure_filename5 = 'dimer_fde_nm.png'

### Integrate the difference
#ddx = dimer6.shape
#dx = (E_end - E_begin) / numpy.float(ddx[1])
#diff = 0.
#diff10 = 0.
#diff8 = 0.
#diff5 = 0.
#diff3 = 0.
#for ix in range(ddx[0]):
#    diff = diff + dx * numpy.absolute(dimer6[ix,2]-fde0[ix,2]-fde1[ix,2])
#    diff10 = diff10 + dx * numpy.absolute(dimer10[ix,2]-fde010[ix,2]-fde110[ix,2])
#    diff8 = diff8 + dx * numpy.absolute(dimer8[ix,2]-fde08[ix,2]-fde18[ix,2])
#    diff5 = diff5 + dx * numpy.absolute(dimer5[ix,2]-fde05[ix,2]-fde15[ix,2])
#    diff3 = diff3 + dx * numpy.absolute(dimer3[ix,2]-fde03[ix,2]-fde13[ix,2])
#diff = diff / (ddx[0])
#diff10 = diff10 / (ddx[0])
#diff8 = diff8 / (ddx[0])
#diff5 = diff5 / (ddx[0])
#diff3 = diff3 / (ddx[0])
#print diff3, diff5, diff, diff8, diff10


## plot out optical absorption spectrum
try:
    import pylab
except:
    print "pylab not available: not plotting now"
    sys.exit(1)

pylab.figure(0)
params = {'axes.labelsize':  16,
          'text.fontsize':   16,
          'legend.fontsize': 16,
          'xtick.labelsize': 16,
          'ytick.labelsize': 16,
          'text.usetex':     True}
pylab.rcParams.update(params)
pylab.rc("axes", linewidth=2.0)
pylab.rc("lines", linewidth=3.0)

pylab.plot(dimerx[:,0], dimerx[:,2], label='rt-TDDFT \AA',        linestyle='solid', alpha = 0.50, color='blue')
pylab.plot(fde0x[:,0], fde0x[:,2]+fde1x[:,2], label='FDE-rt-TDDFT \AA', linestyle='dashed', alpha = 0.50, color='red')
pylab.xlabel('energy (eV)')
pylab.ylabel('dipole strength function ($eV^{-1}$)')
pylab.title('optical absorption spectrum')
pylab.grid(True)
pylab.legend(loc='upper left')
pylab.axis([E_begin, E_end, numpy.min(dimerx[:,2])*1.2, 10])
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig(oas_figure_filename1)

pylab.figure(1)
pylab.plot(dimery[:,0], dimery[:,2], label='rt-TDDFT \AA',        linestyle='solid', alpha = 0.50, color='blue')
pylab.plot(fde0y[:,0], fde0y[:,2]+fde1y[:,2], label='FDE-rt-TDDFT \AA', linestyle='dashed', alpha = 0.50, color='red')
pylab.xlabel('energy (eV)')
pylab.ylabel('dipole strength function ($eV^{-1}$)')
pylab.title('optical absorption spectrum')
pylab.grid(True)
pylab.legend(loc='upper left')
pylab.axis([E_begin, E_end, numpy.min(dimery[:,2])*1.2, 10])
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig(oas_figure_filename2)

pylab.figure(2)
pylab.plot(dimerz[:,0], dimerz[:,2], label='rt-TDDFT \AA',        linestyle='solid', alpha = 0.50, color='blue')
pylab.plot(fde0z[:,0], fde0z[:,2]+fde1z[:,2], label='FDE-rt-TDDFT \AA', linestyle='dashed', alpha = 0.50, color='red')
pylab.xlabel('energy (eV)')
pylab.ylabel('dipole strength function ($eV^{-1}$)')
pylab.title('optical absorption spectrum')
pylab.grid(True)
pylab.legend(loc='upper left')
pylab.axis([E_begin, E_end, numpy.min(dimerz[:,2])*1.2, 10])
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig(oas_figure_filename3)

pylab.figure(3)
pylab.plot(dimer[:,0], dimer[:,2], label='rt-TDDFT \AA',        linestyle='solid', alpha = 0.50, color='blue')
pylab.plot(fde0[:,0], fde0[:,2]+fde1[:,2], label='FDE-rt-TDDFT \AA', linestyle='dashed', alpha = 0.50, color='red')
pylab.xlabel('energy (eV)')
pylab.ylabel('dipole strength function ($eV^{-1}$)')
pylab.title('optical absorption spectrum')
pylab.grid(True)
pylab.legend(loc='upper left')
pylab.axis([E_begin, E_end, numpy.min(dimer[:,2])*1.2, 10])
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig(oas_figure_filename4)

pylab.figure(4)
pylab.plot(1239.84/dimer[1:,0], dimer[1::,2], label='rt-TDDFT \AA',        linestyle='solid', alpha = 0.50, color='blue')
pylab.plot(1239.84/fde0[1:,0], fde0[1:,2]+fde1[1:,2], label='FDE-rt-TDDFT \AA', linestyle='dashed', alpha = 0.50, color='red')
pylab.xlabel('energy (eV)')
pylab.ylabel('dipole strength function ($eV^{-1}$)')
pylab.title('optical absorption spectrum')
pylab.grid(True)
pylab.legend(loc='upper left')
pylab.axis([150, 550, numpy.min(dimer[:,2])*1.2, 10])
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig(oas_figure_filename5)
#pylab.figure(2)
#pylab.plot(dimerz[:,0], dimerz[:,2], label='rt-TDDFT \AA',        linestyle='solid', alpha = 0.50, color='blue')
#pylab.plot(1240./fde0[10:,0], fde0[10:,2]+fde1[10:,2], label='FDE-rt-TDDFT \AA', linestyle='dashed', alpha = 0.50, color='red')
#pylab.xlabel('energy (eV)')
#pylab.ylabel('dipole strength function ($eV^{-1}$)')
#pylab.title('optical absorption spectrum')
#pylab.grid(True)
#pylab.legend(loc='upper left')
#pylab.axis([100, 600, numpy.min(dimerz[:,2])*1.2, 10])
#ax = pylab.gca()
#ax.title.set_fontsize(24)
#pylab.savefig(oas_figure_filename3)

#pylab.show()
