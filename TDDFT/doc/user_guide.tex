\documentclass[12pt,a4paper]{article}
\def\version{5.1.0}
\def\PWscf{\texttt{PWscf\ }}
\def\TDDFT{\texttt{TDDFT\ }}
\def\qe{{\sc Quantum ESPRESSO}}

%\usepackage{html}

% BEWARE: don't revert from graphicx for epsfig, because latex2html
% doesn't handle epsfig commands !!!
%\usepackage{graphicx}

\textwidth = 17cm
\textheight = 24cm
\topmargin =-1 cm
\oddsidemargin = 0 cm



\begin{document} 
\author{}
\date{}


%\begin{htmlonly}
%%\def\qeImage{../../Doc/quantum_espresso.png}
%%\def\democritosImage{../../Doc/democritos.png}
%%\end{htmlonly}

\title{
  \includegraphics[width=5cm]{\qeImage} \hskip 2cm
  \includegraphics[width=6cm]{\democritosImage}\\
  \vskip 1cm
  % title
  \Huge User's Guide for \PWscf\smallskip
  \Large (version \version)
}
%\endhtmlonly

%\latexonly
%\title{
% \epsfig{figure=quantum_espresso.png,width=5cm}\hskip 2cm
% \epsfig{figure=democritos.png,width=6cm}\vskip 1cm
%  % title
%  \Huge User's Guide for \qe \smallskip
%  \Large (version \version)
%}
%\endlatexonly

%\maketitle

\tableofcontents

\section{Introduction}

This guide is meant for both users of the \TDDFT code, as well as future developers. In order to use
the \TDDFT code, one is first required to perform a \PWscf calculation and save all scratch directories.
For running \PWscf calculation, please refer to the official user guide. 
The \PWscf/\TDDFT combination allows to perform real-time TDDFT calculations by running pw.x and subsequently tddft.x, 
or real-time \textbf{subsystem} TDDFT calculations by running fdepw.x and subsequently fdetddft.x. 




%\subsection{Standard TDDFT input file}


\section{Compilation}

\TDDFT depends on the make.sys of the \PWscf code. In order to compile \TDDFT, please first run
\begin{verbatim}
 ./configure --with-qe-source PATH-TO-MAKE.SYS
\end{verbatim}
This will create a make.depend file listing all dependancies of the code on PWSCF, and a Makefile from Makefile.in. 
When performing an svn update, the make.depend, Makefile.in and Makefile will unfortunately contain the absolute path of the person that created the commit and it will
have to be changed manually. Feel free to fix this. When adding subroutines to the code, make sure not only to add them to Makefile but also to the
Makefile.in. If you are configuring the code on a Mac, two line needs to be changed in makedeps.sh, that begin with ``sed -i'', to ``sed -i '' ''  ``.


\section{Using \TDDFT}


The \TDDFT code can be either used for regular TDDFT calculation, or subsystem TDDFT calculations. Regular TDDFT calculations are run by
\begin{verbatim}
 mpirun -np n tddft.x -in input_file
\end{verbatim}
where n is the number of processors and the input file is named input\_file.in.

For subsystem TDDFT calculations, also the number of fragments needs to be specified using the as follows
\begin{verbatim}
% mpirun -np n fdetddft.x -ni N -in input_file
\end{verbatim}
where N is the number of subsystems, or fragments, and the input file for each fragments is input\_file\_x.in with x ranging between 0 and N-1. If you are using 
a symmetrical parallelization, namely each fragment calculation is distributed between an equal number of processors, make sure mod(n,N)=0. Otherwise, you can specify how many
processors should be assigned to each fragment by creating a fragments\_procs.in file, where on each line you specify the number of processors by an integer. Make sure all of them
add up to n. For instance, if you run a calculation on a trimer and you with the first monomer to use 6 processor, and the other monomers to use 5 processors, your
fragments\_procs.in should be:
\begin{verbatim}
 6
 5
 5
\end{verbatim}
and you should run the calculation as
\begin{verbatim}
 mpirun -np 16 fdetddft.x -ni 3 -in input_file
\end{verbatim}

In all cases, the number of processors per fragment needs to be the same as in the \PWscf calculation, since each processor writes its own binary file to save the wave function. 
The twf\_collect=.true. and wf\_collect=.true. options are not to be trusted, it is bugged, probably on the fdepw.x side.

\subsection{Input data}

Unlike \PWscf, the input file for TDDFT contains only one namelist, \& inputtddft. A typical input file will look as follows:
\begin{verbatim}
&inputtddft
     job            = 'optical'
     molecule       = .true.
     prefix         = 'name'
     tmp_dir        = './scratch/'
     l_tddft_restart = .false.
     dt             = 1.0
     nstep          = 10000
     e_strength     = 0.01
     e_direction    = 1
     conv_threshold  = 1.0d-14
     ehrenfest      = .false.
     tdpulse        = 'uniform_efield'
     tefield        = 'wfshift'
     integrator     = 'cn'
     verbosity      = 'low'
     coupled        = .true.
     icalc_ener      = 1
/
\end{verbatim}

An explanation to each line is given below:
\begin{itemize}
 \item job: for now, 'optical' is the only option. This line cannot be ommitted or it will result in error. 
 \item molecule: default value .true. and is also the only acceptable value unless you're using the periodic branch, where it can be set to .false.. In that case, the code
 follows a completely different route by envoking the solid\_optical\_absorption subroutine, instead of the molecule\_optical\_absorption subroutine and calculating the current. 
 More information is given in the comments of gauge\_fields.f90 and runge\_kutta\_coupled.f90.
 \item prefix: must be the same as the prefix specified in the \PWscf input.
 \item tmp\_dir: must be the same as the outdir specified in the \PWscf input. It is essential to remember to save these directories when performing the \PWscf calculation.
 \item l\_tddft\_restart: default is .false. In that case, the starting point for the calculation is the wave function saved in the .wfc files in the directory specified by tmp\_dir.
 Otherwise, the code will restart from the wave function saved in .wfcx. Note that unlike in \PWscf, the rest of the file is still read so caution must be used. The restart option
 doesn't do anything else than changing the files from which the wave functions are read, so if you are telling the program to shift the wave function or apply an explicit field (see below),
 it will still do so on the restart. To avoid this, you can put the e\_strength on 0. Also, the step number is not saved so the count will restart from 0. If you already performed 5000
 steps and you are restarting with ''nstep = 5000`` (see below), the code will perform additional 5000 steps, which will be labeled 1-5000. When restarting, the old output files will 
 be overwritten so you must save them explicitly before submitting the restart calculation. The fact that the step number is not saved also means that the restart will not work 
 correctly if you applied an explicit field and the field was not 0 yet when the calculation stopped. Feel free to fix this.
 \item dt: the time step, in attoseconds. The example above shows 1.0 attoseconds, which is also the default. The time step should not be above 2 and it is a good idea to test the stability of the calculation
 on the time step for each system, as it can vary. As a measure of stability you should use the total energy (or fragment energy+interaction in case of uncoupled calculations). 
 \item e\_direction: the cartesian direction in which the dipole field is applied (1=x, 2=y, 3=z).
 \item q\_direction1, q\_direction2: If tdpulse = 'uniform\_efg', the cartesian directions in which the electric field gradient is applied (1=x, 2=y, 3=z). Default: xy.
 \item conv\_threshold: the convergency threshold for the conjugate gradient squared subroutine in the crank-nicholson method. Default: 1.0d-12.
 \item ehrenfest: default is .false., if set to .true. triggers the calculation of forces and movement of electrons at each time step. Note that this will only be correct when combined
 with normconserving pseudopotentials, as we are missing (arguably unprogrommable) terms for ultrasoft pseudopotentials.
 \item tdpulse: can have the following values:
  \begin{itemize}
   \item uniform\_efield: the default. Means a uniform electric field, in space and time, will be applied. Should only be combined with tefield='wfshift'.
   \item uniform\_efg: Means a uniform electric field gradient, in space and time, will be applied. Should only be combined with tefield='wfshift'. See 'q\_direction1' and 'q\_direction2' above. In order to obtain the quadrupole response, one should also add the line 
\begin{verbatim}
 l_quadrupole = .true. 
\end{verbatim}
   \item 'cosine4': will apply an electric field uniform in space, but damped in time, of the shape: $A\cos(\omega\cdot c)\exp{(-\frac{(t-t_0)^{2}}{2*\sigma^{2}})}$ where A is 
   specified by e\_strength, $\omega$  and $\sigma$ are defined by adding the two following lines:
    \begin{verbatim}
    omega0 = 2.72
    sigma0 = 100.
    t0 = 0.
    \end{verbatim}
     where omega0 should be the value of the excitation energy you wish to trigger in eV, sigma0 defines the width of the damping gaussian and t0 defines the peak of the gaussian. For more
     information, please see the code.
   \item Other values are 'cosine', 'gaussian', 'cosine2' and 'cosine3' but they are not recommended. More information on them can be found in tddft\_routines.f90 and add\_efield\_t.f90.
    If you want to add an extra shape of field, define a name and an itdpulse value for it in tddft\_routines.f90 in 'select case(trim(tdpulse))' and then program a shape in add\_efield\_t.f90
    for the new itdpulse value. Also, there is an fparser.f90 subroutine that would allow to specify a shape in the input file directly, but it's not working yet, feel free to debug it.
  \end{itemize}
  Also important for applying an explicit field is the saw function, which defines where the potential will drop from maximum value to minimum value within the cell (uniform field means
  linearly rising potential). The default values are:
  \begin{verbatim}
   emaxpos = 0.d0
   eopreg = 0.1d0
  \end{verbatim}
   which are defined the same was as in \PWscf code. These values assume your system is situated in the center of the cell and there is vacuum around 0.9-0.1 alat. If this is not the case,
     these values need to be adjusted.
  \item tefield: can be either 'wfshift', which will result in shifting the wave function, equivalent to applying a $\delta$-pulse, or 'external', which means means an explicit 
  electric field will be added to the potential (only tdpulse='cosine4' is stable, see above).
  \item integrator: for now, the code has two integrators, Crank-Nicholson and runge-kutta. for Crank-Nicholson, you can specify 'cn' (no predictor-corrector step), 'cn2' (1 predictor corrector step), 
  'cn3', 'cn4' and 'cnfull', which will iterate the predictor-corrector steps until self-consistency. The convergency is tested against pc\_conv\_threshold, which is set to a default value of 1.0d-8. 
  The default integrator is 'cn2', but for very long and expensive calculations 'cn' is sufficient. You can also use the Runge-Kutta integrator (not recommended) to 2nd order by specifying 'rk2' or
  to 4th order by specifying 'rk4'. Be carefull since runge-kutta is does not guarantee energy conservation. There are keywords programmed for other integrators, but they are not implemented yet, 
  see tddft\_routines.f90. 
  \item verbosity: can be 'high', 'medium' or 'low'. 'high' is only recommended for debugging, for long simulations use 'low', output slows things down drastically.
  \item calc\_ener: triggers calculation of energy, set true by default. Calculation of energy is needed to test the stability of the calculation, but it is also very expensive, so by default
  energy is only calculated every 100 steps. This can be modified by setting icalc\_ener to a value different than 100. For instance, to calculate energy every 10 steps, set
  \begin{verbatim}
   icalc_ener = 10
  \end{verbatim}
   \item update: .true. by default. For any integrator other than 'cn', a total density update is performed between the fragments. To speed things up, this can be avoided by setting 
   update to .false., which introduces a slight error to the integrator but it is still better to do a 'cn2' with update=.false. than 'cn'. For uncoupled calculations, update is 
   set automatically to .false., without introduction of error. For mixed coupled calculations (see below), when update=.true. the density is only updated between the fragments of 
   the same coupled group.
   \item coupled: .true. by default. If set to .false., an uncoupled calculation will be performed. This means, from the view point of each fragment, the other fragments are frozen at t=0. 
   If you are only interested in the uncoupled response of a single fragment, perform a frozen calculation (see below).
   \item icoupled: by default set to 1 for all coupled fragments. This argument allows to perform mixed coupled calculations, where for instance, between 5 fragments, fragment 1 and 2 only coupled
   with each other, fragments 3 and 4 only coupled with each other and fragment 5 couples with nobody. In that case, one specifies icoupled=1 in fragments 1 and 2, icoupled=2 on fragments 3 and 4
   and icoupled=3 or coupled=.false. in fragment 5. 
   \item frozen: by default set to .false. If true, will read a frozen density (or densities) to be part of the calculation. The number of densities is defined by ifrozen (by default set to 0, 
   so if you are setting frozen=.true., you MUST specify ifrozen). Reading of the frozen densities is done in tddft\_setup\_frozen.f90 and at the moment, the code is only set up to read
   densities of fragments that were calculated by \PWscf but not included in the \TDDFT calculation. This kind of calculation is interesting if you want the uncoupled response of a specific fragment, but
   not the rest of the fragments. To do this, first perform a \PWscf calculation by specifying the option ''fde\_print\_density\_frag=.true.`` in the \&SYSTEM namelist, which will produce 
   a .pp file that will be read by the \TDDFT code. If you performed a \PWscf calculation on a trimer, and you are only interested in the uncoupled response of fragment 0, you specify
   frozen=.true, ifrozen=2 and run the calculation as
   \begin{verbatim}
    mpirun -np n fdetddft.x -ni 1 -in input_file
   \end{verbatim}
   making sure ''n`` is the number of processors assigned to that fragment in \PWscf calculation. Also, if you were using fragments\_procs.in file, the lines of the frozen fragments
   need to be deleted (not just commented out). 
   If, however, you are intersted in the uncoupled response of fragments 2, but not 0 and 1, you need to create an image\_labels.in file, which specifies which fragments are frozen
   and which are active as follows:
   \begin{verbatim}
   #active fragments
   2
   #frozen fragments
   0
   1
   \end{verbatim}
   The coupled, uncoupled and frozen keywords can be combined to your liking: you can perform a calculation on 5 fragments where fragments 0 and 3 are frozen, fragments 1 and 4 are coupled
   and fragment 2 is uncoupled, by specifying frozen=.true. and ifrozen=2 on all, coupled=.true. on fragments 1 and 4, coupled=.false. on fragment 2 and making an image\_labels.in as follows
   \begin{verbatim}
   #active fragments
   1
   2
   4
   #frozen fragments
   0
   3
   \end{verbatim}
   and running the calculation as 
   \begin{verbatim}
    mpirun -np n fdetddft.x -ni 3 -in input_file
   \end{verbatim}
   Remembering to adjust n and fragments\_procs.in.
   \item late\_coupling: set to .false. by default. Allows to start an uncoupled calculation, but have the fragments couple after a number of steps, given by ilate\_coupling. ilate\_coupling
   is set to 0 by default so MUST be specified or you will just perform a coupled calculation. This keyword is experimental and results may be unphysical.
   \item print\_bands: set to .false. by default. If .true., will print the bands energies at each step. Note that \TDDFT only works with the bands calculated in \PWscf, so if you want information
   on virtual bands, you need to increase the number of bands calculated in \PWscf.
   \item chain: set by default to .false. If set to .true., this keyword is designed to mimick super-exchange situations, where, for instance, in a trimer, fragment 0 only couples to fragment 1, 
   fragment one couples to fragments 1 and 2 and fragment 2 only couples to fragment 1. Who couples with whom is specified OUTSIDE the namelist (so a line below ''/``) (for this example)
   in input file of fragment 0:
   \begin{verbatim}
   CHAIN
   3
   1
   1 1 0
   \end{verbatim}
   fragment 1:
   \begin{verbatim}
   CHAIN
   3
   2
   1 1 1
   \end{verbatim}
   fragment 2:
   \begin{verbatim}
   CHAIN
   3
   3
   0 1 1
   \end{verbatim}
   First line specifies ''nlink``, so how many links are there, in this case 3. Second line specifies my\_link and third line specifies the ''my\_chain(nlink)``. In the code, when
   the density is updated, the rho\_fde is copied into nlink temporary arrays, in which the density of the fragment is multiplied by my\_chain(my\_link). After an update over all fragments 
   of the temporary arrays, the array of my\_link is copied back into rho\_fde. So for fragment 0, it would get the rho\_fde where rho0 was multiplied by 1, rho1 was multiplied by 1 and rho2 
   was multiplied by 0. This keyword is highly experimental and though seems to work at first, the energy and dipole moments derail after about 1000 steps. It's unclear whether there is 
   a bug in the code, or the chain calculation leads to unphysical effects.
   \item OCCUPATIONS: instead (or additional to) applying an electric field, one can also promote an electron to a virtual band, provided that band was calculated in \PWscf. This is done 
   by specifying occupations, also outside the namelist. If you calculated a molecule with 4 occupied bands and 2 virtuals and you wish to promote one electron from HOMO to LUMO, you specify:
   \begin{verbatim}
    OCCUPATIONS
    2 2 2 1 1 0
   \end{verbatim}
    For open shell systems, occupations are read for each spin separately (lines 630-634 in fde\_routines.f90). Occupations is not combinable with chain.
\end{itemize}



\subsection{Data files}
The wave function is written at each step into the tmp\_dir directories, to .wfcx. The .wfc files from \PWscf are preserved. If the calculation crashes while writing to the .wfcx file,
you are fresh out of luck, so some kind of robust mechanism would be welcome. Also, we are in need of a keyword to write the density after every so many steps (not too often, huge files).




\subsection{Using the interlocking cells}
If you are using the interlocking cells in the reducedgrid branch, several slight modifications are applicable:
\begin{itemize}
 \item If running a coupled calculation as above, note that only the dipole moment and the total energies will be correct, but not the fragment energy or interaction energy (this will be obvious
 from the numbers).
 \item For any other kind of calculation, you need to disable the fancy parallelization which parallizes the calculation depending on rho\_fde in the large cell over all processors. This is done
 by simply specifying -nfp flag:
   \begin{verbatim}
    mpirun -np n fdetddft.x -nfp -ni 3 -in input_file
   \end{verbatim}
   If you forget, you will get an error. 
   \item If you wish to run a frozen calculation, you need to run the \PWscf calculation also with an -nfp flag and specify ''fde\_print\_density\_frag\_large=.true.`` as you will need the fragment 
   density on the large grid.
   \item Watch out when applying explicit electric fields with the emaxpos and eopreg, interlocking cells will always place your system in the center.
   \item Be careful with making the small cells too small, TDDFT calculations are very sensitive for instabilities. Using interlocking cells has been tested for computational lack of bugs,
   not for effect on results and accuracy.
\end{itemize}


\end{document}
