#!/usr/bin/python
#
## Xiaofeng Qian @ mit, 2010
#
import numpy, sys, pylab

#-----------------------------------------------------------#
#
width=0.01e0
sigma=100.0e0
length=10000
omega=2.35e0
e_strength=0.01e0
dt_fs=2.0e0

h_planck_si=6.62606896e-34
tpi=2.0e0*3.14159265358979323846e0
hartree_si=4.35974394e-18
bohr_radius_angs=0.52917720859e0
## constants in SI unit
EV_IN_J = 1.60217733e-19
HBAR    = 1.054572669125102e-34
ATTOSECOND = 1.0e-18
ELECTRON_REST_MASS_IN_KG = 9.1093897e-31
ELEMENTARY_CHARGE_IN_C   = 1.60217733e-19
A_IN_M = 1.0e-10
BOHR_TO_A = 0.529177249 

au_sec=h_planck_si/tpi/hartree_si

omega=omega*0.036749309e0*2.0e0
e_strength=e_strength*bohr_radius_angs
field=numpy.empty(length)
field2=numpy.empty(length)
nt      = numpy.linspace(0, (length-1), length) 
dt=dt_fs*1.e-18/(2.e0*au_sec)
dt_e=dt_fs*ATTOSECOND*EV_IN_J/HBAR
t= nt*dt
t_e= nt*dt_e
dE      = 10.0 / numpy.float(length-1)
damping_factor  = 0.010           # damping factor
dp_damping_e = numpy.exp( - t * damping_factor )
ef_fft = numpy.zeros(length) + 1j * numpy.zeros(length)
ef_fft2 = numpy.zeros(length) + 1j * numpy.zeros(length)
E_axis  = numpy.zeros(length)
for it in range(length):
           field[it] =  e_strength*numpy.cos(omega*t[it])*numpy.exp(-t[it]**2/(2*sigma**2))
           field2[it] =  e_strength*numpy.cos(omega*t[it])*numpy.exp(-width*t[it])
           print omega, t[it], sigma, field[it]
#           field[it] =  e_strength*numpy.cos(2.35*t[it])#*numpy.exp(-width*t[it])
#           print numpy.cos(2.35*it)
#           print numpy.exp(-0.05*it**2)

for it in range(length):
           Eit=dE * it
           E_axis[it] = Eit
           ef_fft[it] = numpy.sum(field * dp_damping_e * numpy.exp( -1j * Eit * t_e))
           ef_fft2[it] = numpy.sum(field2 * dp_damping_e * numpy.exp( -1j * Eit * t_e))

field_output_filename='field'
f = open(field_output_filename, "wt")
field_output_filename2='field_fft'
g = open(field_output_filename2, "wt")
for i in range(length):
       f.write("%f %f %f \n" % (nt[i]  , field[i], field2[i]))
       g.write("%f %f %f \n" % (E_axis[i],  numpy.real(ef_fft[i]), numpy.real(ef_fft2[i])))

pylab.figure(0)
params = {'axes.labelsize':  16,
          'text.fontsize':   16,
          'legend.fontsize': 16,
          'xtick.labelsize': 16,
          'ytick.labelsize': 16,
          'text.usetex':     True}
pylab.rcParams.update(params)
pylab.rc("axes", linewidth=2.0)
pylab.rc("lines", linewidth=3.0)
pylab.plot(t[:], field[:], label='Field', alpha = 0.50, color='blue')
pylab.plot(t[:], field2[:], label='Field 2', alpha = 0.50, color='red')
pylab.xlabel('time (as)')
pylab.ylabel('field strength')
pylab.grid(True)
pylab.legend(loc='upper right')
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig('field_in_time.png')

pylab.figure(1)
pylab.plot(E_axis[:], numpy.real(ef_fft[:]), label='Field FFT', alpha = 0.50, color='blue')
pylab.plot(E_axis[:], numpy.real(ef_fft2[:]), label='Field 2 FFT', alpha = 0.50, color='red')
pylab.xlabel('time (as)')
pylab.ylabel('field strength')
pylab.grid(True)
pylab.legend(loc='upper right')
ax = pylab.gca()
ax.title.set_fontsize(24)
pylab.savefig('field_in_time.png')
pylab.show()
pylab.show()
