from __future__ import print_function
import numpy as np
import sys

# coding: utf-8

# ## In order to run this code properly (and as it is), jupyter should be installed thorough Anaconda package.
# 
# ### This code generates optical spectra from TDDFT output along with other capabilities.
# 
# ### Few concise definitions before proceeding:

# $\textbf{Uncoupled Calculations:}$ These correspond to calculations which employs only the one-body response ($\chi_I^U$) in other words, the embedding potential is computed at each time step only updating the subsystem time-dependent density and leaving the density of the surrounding subsystems frozen at t = 0.

# $\textbf{Coupled Calculations:}$ These correspond to calculations which employes full many-body response ($\chi_I^C$).
# 
# $$\chi_I^C = \chi_I^U + \sum_{J\neq I}^{N_S}\chi_I^U K_{IJ} \chi_I^C$$
# 
# where, $I$ and $J$ are subscripts denoting subsystems.
# 

# $\textbf{NOTE:}$ Optical spectra corresponding to Coupled calculations can have negative peaks whereas the spectra for uncoupled calculations should not. (Or atleast relatively very small negative values).
# 
# For this reason, I have added a section for computing $\textbf{Outliers}$ (to be used for uncoupled) with the choice of customising the threshold/cutoff to determine which subsystem's uncoupled calculations give rise to negative peaks.

# # A bit of theory:

# The definition of frequency-dependent dipole:
# $$
# \text{dip_FFT}=\delta\mu(\omega)=\int \delta\mu(t) e^{-i\frac{\omega t}{\hbar}}dt.
# $$
# The backward transform is:
# $$
# \delta\mu(t) = \frac{1}{2\pi\hbar}\int \delta\mu(\omega) e^{\frac{\omega t}{\hbar}}d\omega,
# $$
# in this way, units are preserved: 
#     (1) $\mu(t)$ in $ea_0$ or $Cm$; 
#     (2) $\mu(\omega)$ in $e a_0 \frac{\hbar}{\text{Eh}}$ or $s C m$.
#     
# Because numpy.fft gives the following Fourier components (norm='None')
# $$
# A_k = \sum_{m=0}^{n-1} a_m \exp\left\{-2\pi i{mk \over n}\right\} \qquad k = 0,\ldots,n-1.
# $$
# We must multiply by $dt$ the output of fft. Also, $dt$ must be scaled to reproduce the $i\omega t$ exponent:
# $$
# \text{numpy.dt} = \frac{dt}{2\pi}.
# $$
# 
# The signal is given by:
# $$
# \text{sigma}=\sigma(\omega)=\frac{2}{\pi}\frac{\delta\mu}{\delta E}
# $$ 
# The strength function is:
# $$
# \text{S} = \omega\sigma(\omega).
# $$ 
# The sum rule (number of active electrons) is given by
# $$
# N_\text{active}=\int S(\omega)d \omega.
# $$
# The $\omega=0$ polarizability becomes:
# $$
# \text{alpha}=\alpha(0)=\int\frac{S(\omega)}{\omega^2}d \omega
# $$
# or it should also be given by: 
# $$
# \alpha(i\omega)=\frac{3}{\pi}\int\delta\mu(t) e^{-\omega t}dt,
# $$
# but because the value of $\alpha(i\omega)$ at $\omega=0$ cannot be computed numerically (random number generation...), we identify it as
# $$
# \alpha(0)\simeq \max_{\omega} \alpha(i\omega)
# $$
# and then check against the one computed from the sum rules.

# ### Workflow:
# 
# 1. The following cell is where the fun computation starts.
# 
# 2. But before that, user must provide details about the TDDFT calculations on which this code is about to be executed. For eg. number of subsystems, number of directions along which the electric field was subjected to, etc.
#     These inputs should be fairly obvious once the code is executed and the user input section starts.
# 
# 3. At the end of the computations, a basic interactive plot of the optical absorption spectra is generated and if given the option similar spectra for $\alpha_{i\omega}$ is also generated.
# 
# 4. Ability to smooth over the rough plots (for asthetic reasons) is also available towards the end of the code.

# # Important user inputs

# In[ ]:

# Parameters needed for optical spectrum computation to be extraceted from TDDFT input file or output (User's choice).
# Either input or output (case insensitive)!!

print ("Parameters from input or output?\n")
if sys.version_info[0] < 3:
    choice1 = str(raw_input())
else:
    choice1 = str(input())


# In[ ]:

# Number of subsystems. Just an integer number.

print ("Number of subsystems?\n")
if sys.version_info[0] < 3:
    subsystems = int(raw_input())
else:
    subsystems = int(input())


# In[ ]:

# Number of directions to be covered in this single execution of code.

print ("Number of directions?\n")
if sys.version_info[0] < 3:
    directions = int(raw_input())
else:
    directions = int(input())


# In[ ]:

# Path leading to directory containing outputs corresponding to each direction given separately.

print ("Path to the outputs (assuming these are in separate folders for each direction)\n# For ex. 1. Output file directory = \"../../x_dir/Input.tddft_0.out\" then path = \"../../x_dir/\"\n# 2. If output file and this code are in the same dir. then path = \"./\"\n")
paths = []
for i in range(directions):
    if sys.version_info[0] < 3:
        paths.append(str(raw_input()))
    else:
        paths.append(str(input()))


# In[ ]:

# Prefix of the output file name succeeding by _<subsystem number>.out
# For ex. Output file name = "Input.tddft_0.out" then prefix = "Input.tddft"

print ("Prefix of outputs (assuming prefix follows with _<subsytem number>.out)\n# For ex. Output file name = \"Input.tddft_0.out\" then prefix = \"Input.tddft\"\n")
if sys.version_info[0] < 3:
    prefix = str(raw_input())
else:
    prefix = str(input())


# In[ ]:

print ("Want to compute alpha_iw (y/n)?\n")
if sys.version_info[0] < 3:
    alpha_iw_choice = str(raw_input())
else:
    alpha_iw_choice = str(input())


# In[ ]:

print ("Want to compute sumrule (y/n)?\n")
if sys.version_info[0] < 3:
    sumrule_choice = str(raw_input())
else:
    sumrule_choice = str(input())


# In[ ]:

print ("Want to compute polarizabilities (alpha(0)) (y/n)?\n")
if sys.version_info[0] < 3:
    polarizability_choice = str(raw_input())
else:
    polarizability_choice = str(input())


# In[ ]:

outliers_cutoff = 0.1


# In[ ]:

print ("Want to compute outliers for uncoupled calculations (y/n)?\n")
if sys.version_info[0] < 3:
    outliers_choice = str(raw_input())
else:
    outliers_choice = str(input())

if (outliers_choice.lower() == 'y'):
    print ("\nDefault value for outliers is %f, do you want to change this value? (y/n)\n" % (outliers_cutoff))
    if sys.version_info[0] < 3:
        outliers_cutoff_choice = str(raw_input())
    else:
        outliers_cutoff_choice = str(input())
    if outliers_cutoff_choice.lower() == 'y':
        print ("\nCutoff for outliers (absolute value):\n")
        if sys.version_info[0] < 3:
            outliers_cutoff = float(raw_input())
        else:
            outliers_cutoff = float(input())


# In[ ]:

damping_factor = 0.05 # in eV


# In[ ]:

print ("Damping factor = %f eV. Do you want to change this value? (y/n)\n" % (damping_factor))
if sys.version_info[0] < 3:
    damping_factor_choice = str(raw_input())
else:
    damping_factor_choice = str(input())

if damping_factor_choice.lower() == 'y':
    print ("Damping factor = ")
    if sys.version_info[0] < 3:
        damping_factor = float(raw_input())
    else:
        damping_factor = float(input())


# ## Constants in SI unit

# In[ ]:

eV_IN_J                  = 1.60217733e-19                  # eV/J 
HBAR                     = 1.054572669125102e-34           # J*s/(2*pi)
H_CONST                  = 6.62607004081e-34               # J*s
BOHR_TO_A                = 0.529177249                     # Ang / Bohr 
A_IN_M                   = 1.0e-10                         # meters / Ang  
ELECTRON_REST_MASS_IN_KG = 9.1093897e-31                   # Kg / m_e  
ELEMENTARY_CHARGE_IN_C   = 1.60217733e-19                  # Coul / e  
S_IN_AU                  = 2.41888432650516e-17            # s / a.u.
ATTOSECOND_IN_S          = 1.0e-18                         # s / as 
ATTOSECOND_IN_AU         = ATTOSECOND_IN_S / S_IN_AU       # 0.0413413733366
ATTOSECOND_IN_RY_AU      = ATTOSECOND_IN_AU / 2            # 0.0206706866683
Eh_IN_eV                 = 27.211385                       # eV / Eh
RY_IN_Eh                 = 0.5                             # Eh / Ry
RY_IN_eV                 = Eh_IN_eV * RY_IN_Eh             # eV / Ry
Eh_IN_J                  = eV_IN_J / Eh_IN_eV              # J / Eh
C_IN_AU                  = 137.0359996287515               # au of speed


# ## Damping function.;

# In[ ]:

def damp_funct(t0, daping_factor):
    return np.exp(-(t0) * 2.0 * np.pi * daping_factor/Eh_IN_eV )


# In[ ]:

def lt_funct(t0, f0):
    return np.exp( - t0 * 2.0 * np.pi * f0 )


# ## Function to read dipole moment and other parameters from the output file.;

# In[ ]:

def dipole_from_file(outfile, choice2):
    with open(outfile+'.out','r') as outfil:
        lines = outfil.readlines()
        data = []
        outfil.close()
        for i in range(0,len(lines)):
            line = lines[i]
            if (line[0:3] == 'DIP'):
                nline = line.split()
                nline = nline[3:]
                data.append(nline)               #  data = Dipole Moment
                
            if choice2.lower() == 'output':
                if 'Along direction' in line:
                    nline = line.split()[-1]
                    e_dir = int(nline)            #  e_dir = Direction along which field is applied
                if 'e_strength' in line:
                    line2 = lines[i].split()
                    e_str = 2*float(line2[-1])            # e_str = Electric field strength in Eh / Bohr -- This need to be figured out
                if 'Time step' in line:
                    nline = line.split()
                    dt_fs = float(nline[3])/0.0207    # dt_fs = Time step in a.s.
    
    if choice2.lower() == 'input':     
        print ("Input file name taken from prefix provided, by\n replacing _<subsystem number>.out with _<subsystem number>.in\n")
        e_dir, e_str, dt_fs = from_input_file(outfile+'.in')
        
    return data, e_dir, e_str, dt_fs


# In[ ]:

def from_input_file(inputfile):
    with open(inputfile,'r') as infil:
        lines = infil.readlines()
        infil.close()
        for i in range(0,len(lines)):
            line = lines[i]
            if 'e_direction' in line:
                e_dir = int(line.split()[-1])      #  e_dir = Direction along which field is applied
            if 'e_strength' in line:
                e_str = float(line.split()[-1])    # e_str = Electric field strength in Eh / Bohr -- This need to be figured out
            if ' dt ' in line:
                #dt_fs = float(nline[3])/0.0207    # dt_fs = Time step in a.s.
                dt_fs = float(line.split()[-1])
        return e_dir, e_str, dt_fs


# ## Optical Absorption Spectra calculation;

# ### Fourier Transform

# In[ ]:

def fft_transform (dipole_data, e_direction, dt_au):
    
    e_dir = e_direction - 1
    
    dipole_data = np.array(dipole_data, dtype=np.float)
    dip = dipole_data[:,e_dir] - dipole_data[0,e_dir]     # dip in e*bohr
    
    x = np.linspace(0,len(dip),len(dip),endpoint=False)
    d = dt_au / (2*np.pi)
    t = x * d             
    damp = damp_funct(t, damping_factor)                   # Damping function with unitless argument
    
    # Multiply dipole difference by damping function
    signal = dip * damp
    
    # Get the frequencies of the FFT and convert to eV
    
    omega = np.fft.rfftfreq(len(signal),d=d)    # E = 2 pi / dt
    d_omega = (omega[2] - omega[1])
    
    # Calculate the FFT
    dip_FFT = np.fft.rfft(signal) * dt_au
    return dip_FFT, omega, d_omega


# ### Laplace Transform

# In[ ]:

def laplace_transform (dipole_data, e_direction, dt_au):
    
    e_dir = e_direction - 1
    
    dipole_data = np.array(dipole_data, dtype=np.float)
    dip = dipole_data[:,e_dir] - dipole_data[0,e_dir]     # dip in e*bohr
    
    x = np.linspace(0,len(dip),len(dip),endpoint=False)
    d = dt_au / (2*np.pi)
    t = x * d             
    
    damp = damp_funct(t, damping_factor)                   # Damping function with unitless argument
    
    # Multiply dipole difference by damping function
    
    signal = dip * damp
    
    # Get the frequencies of the FFT and convert to eV
    omega = np.fft.rfftfreq(len(signal),d=d)    # E = 2 pi / dt

    dip_LT = np.zeros(len(omega)) + 1j * np.zeros(len(omega))
    for i in range(0,len(omega)):
        dip_LT[i]  = 3.0 * np.sum(lt_funct(t[:], omega[i]) * signal[:]) * dt_au / np.pi
    
    return dip_LT, omega


# ## TDDFT Output file;

# ### Optical Spectra

# In[ ]:

final_strength = []
final_alpha_iw = []
outliers = []

for i in range(subsystems):
    
    S = []
    sumrule = []
    polarizability = []
    dip_lt_iw = []
    print ("%i" % i, end="\r")
    for j in range(directions):
        outputfile = paths[j] + "/" + prefix + "_" + str(i)
        print (outputfile)
        dipole_data, e_direction, e_strength, dt = dipole_from_file(outputfile, choice1)
        
        dt_au = dt * ATTOSECOND_IN_AU
    
        dip_FFT, omega, d_omega = fft_transform(dipole_data, e_direction, dt_au)
    
        sigma = dip_FFT * 2. / e_strength / np.pi
        
        S.append(sigma * omega)
        
        if sumrule_choice.lower() == 'y':
            sumrule.append(np.sum(np.imag(sigma * omega)) * d_omega)
        
        if polarizability_choice.lower() == 'y':
            polarizability.append(np.sum(np.imag(sigma[1:] * omega[1:])/omega[1:]**2) * d_omega)
            
        if alpha_iw_choice.lower() == 'y':
            dip_LT, omega_LT = laplace_transform(dipole_data, e_direction, dt_au)
            dip_lt_iw.append(- dip_LT.real / e_strength)

    strength = np.mean(np.array(S), axis = 0)
    
    if sumrule_choice.lower() == 'y':
        print ("Sumrule = ", np.average(sumrule))
    
    if polarizability_choice.lower() == 'y':
        print('alpha(0) = ', np.average(polarizability), 'au (Bohr cube)')
        print('alpha(0) = ', np.average(polarizability) * BOHR_TO_A**3, 'Angstroms cube')
        
    if polarizability_choice.lower() == 'y' and directions == 3:
        polarizability_anisotropy = polarizability[2] - 0.5 * (polarizability[0] + polarizability[1])
        
        print('Anisotropic Polarizability = ', polarizability_anisotropy, 'au (Bohr cube)')
        print('Anisotropic Polarizability = ', polarizability_anisotropy * BOHR_TO_A**3, 'Angstroms cube')
    
    
    if alpha_iw_choice.lower() == 'y':
        alpha_iw = np.average(np.array(dip_lt_iw), axis = 0)
        
    if outliers_choice.lower() == 'y':
        if (np.abs(np.sort(strength.imag)[0]) >= outliers_cutoff ):
            print ("Outlier: Subsystem number = ", i, ", lowest value = ", np.sort(strength.imag)[0])
            outliers.append(i)
        else:
            print ("Not an outlier!!")
    print ("\n")
    
    final_strength.append(strength)
    if alpha_iw_choice.lower() == 'y':
        final_alpha_iw.append(alpha_iw)

final_strength_avg = np.average(np.array(final_strength), axis = 0)

if alpha_iw_choice.lower() == 'y': 
    final_alpha_iw_avg = np.average(np.array(final_alpha_iw), axis = 0)


# ### Average optical spectrum
# In[ ]:

print ("\nPath for saving optical_spectra.\nIf want to save in current directory just type \"./\"")
if sys.version_info[0] < 3:
    strength_file_path = str(raw_input())
else:
    strength_file_path = str(input())

strength_file = open(strength_file_path + "/" + "Optical_spectra.dat", "w")

for i in range(len(omega)):
    strength_file.write("%f\t%f\t%f\n" % (omega[i] * Eh_IN_eV, final_strength_avg[i].real, final_strength_avg[i].imag))

strength_file.close()


# ## Plot of $\alpha(i\omega)$

# In[ ]:
if alpha_iw_choice.lower() == 'y':
    print ("\nPath for saving Alpha(iw) spectra.\nIf want to save in current directory just type \"./\"")
    if sys.version_info[0] < 3:
        alpha_file_path = str(raw_input())
    else:
        alpha_file_path = str(input())

    alpha_file = open(alpha_file_path + "/" + "Alpha_iw_spectra.dat", "w")

    for i in range(len(omega)):
        alpha_file.write("%f\t%f\n" % (omega_LT[i] * Eh_IN_eV, final_alpha_iw_avg[i].real))

    alpha_file.close()

