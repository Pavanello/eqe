#! /bin/bash

# just a simple script to cleanup the stuff that shouldn't be included in the release tarball

rm -rf .git
rm .gitignore
rm -rf manual_template
rm -rf TODO
rm -rf TDDFT
rm -rf TDDFT-no-FDE
rm -rf Test_Different_Compilers
rm interlocking_newvariablesandroutines.txt

