Modules/griddim.f90:
	subroutine realspace_supergrid_init(dfftl, dfftp, split)
		! Given dfftp%nrx , set dfftl%nrx


Modules/fft_base.f90
	TYPE ( fft_dlay_descriptor ) :: dfftl
		! The descriptor of the supersystem cell


Modules/input_parameter.f90
	INTEGER :: fde_cell_split(3) = 1
		! How the fragment cell is split with respect to the supercell ( 1 means it is the same)


Modules/recvec.f90
	MODULE gvecl
		! it's the equivalent of gvect but for the supersystem cell


PW/src/fde_module.f90
	LOGICAL linterlock = .false.
		! are we usign interlocking cells?
	INTEGER frag_cell_split(3) = 1
		! just a copy of fde_cell_split from the input file


PW/src/setup.f90
	CALL realspace_grids_init ( dfftp, dffts, at, bg, gcutm, gcutms )
	CALL realspace_supergrid_init ( dfftl, dfftp, frag_cell_split )


PW/src/fde_routines.f90
	SUBROUTINE get_local_offset(offset)
		! Calculate the best origin of the fragment cell with respect to the supersystem cell, so that the fragment is found in the middle of it.
	
	FUNCTION grididx_f2g ( fragidx , dfftp, dfftl, offset )
	FUNCTION grididx_g2f ( globidx , dfftp, dfftl, offset )
		! for each point in space, given its index in the fragment (global) array, calculate corresponding index the global (fragment) array.


#############################
NEW FILES Because of slightly duplicated modules for large cell
#############################

Modules/large_cell_base.f90 :
	MODULE large_cell_base

Modules/recvecl_subs.f90 :
	SUBROUTINE ggen_large (and others)

Modules/wavefunctions_large.f90 :
	extra fft memory for the large cell (i.e. psic aux array)

PW/src/allocate_fft_large.f90 :
	allocate the version of rho, v, vrs, etc... in the supersystem grid

PW/src/scf_mod_large.f90 :
	copy of scf_mod.f90 but for the large grid

