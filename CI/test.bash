#!/bin bash
work=$( pwd )
err=0
t2s=1000000
declare -i Diff
for system in AIMD nonlocal_NAKE thiophene-dimer beginner mGGA-L_xc nonlocal_xc use_gaussians
do
  cd ${work}/CI/${system}
  res=$( grep 'FDE: total energy' Input_Benchmark.out | tail -n 1 | awk '{printf "%.8f", $5}' )
  rest=$( grep 'FDE: total energy' Input_0.out | tail -n 1 | awk '{printf "%.8f", $5}' )
  diff=$(echo "scale=1; $res - $rest" | bc) 
  Diff=$(echo ${diff%.*})
  if [ $Diff -gt 1 -o $Diff -lt -1 ]; then
		echo -e "\033[31m ${system}: testing failed !\033[0m"
                echo -e "\033[31m ${res} Ry Vs  ${rest} Ry \033[0m"
                err=1
  else
	echo -e "\033[32m ${system}: testing passed. \033[0m"
        echo -e "\033[32m ${res} Ry Vs  ${rest} Ry \033[0m"
  fi
done
if [ $err == 1 ]; then
	echo -e "\033[31m Testing failed !!! \033[0m"
        exit 1
fi
