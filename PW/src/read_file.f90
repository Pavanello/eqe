!
! Copyright (C) 2001-2012 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE read_file()
  !----------------------------------------------------------------------------
  !
  ! Wrapper routine, for compatibility
  !
  USE io_files,             ONLY : nwordwfc, iunwfc, tmp_dir, wfc_dir
  USE io_global,            ONLY : stdout
  USE buffers,              ONLY : open_buffer, close_buffer
  USE wvfct,                ONLY : nbnd, npwx
  USE noncollin_module,     ONLY : npol
  USE klist,                ONLY : nks
  USE paw_variables,        ONLY : okpaw, ddd_PAW
  USE paw_onecenter,        ONLY : paw_potential
  USE uspp,                 ONLY : becsum
  USE scf,                  ONLY : rho
  USE realus,               ONLY : betapointlist, &
                                   init_realspace_vars,real_space
  USE dfunct,               ONLY : newd
  USE ldaU,                 ONLY : lda_plus_u, U_projection
  USE pw_restart,           ONLY : pw_readfile
  USE control_flags,        ONLY : io_level
  USE input_parameters,     ONLY : fde_xc_funct
! for debugging
!  USE mp_images,       ONLY : nimage, my_image_id
!  USE mp_bands,        ONLY : inter_bgrp_comm, nbgrp
!  USE mp_pools,        ONLY : nproc_pool, me_pool
  !
  IMPLICIT NONE 
  INTEGER :: ierr
  LOGICAL :: exst
  !
  ! ... Read the contents of the xml data file
  !
  CALL read_xml_file ( )
  !
  ! ... Open unit iunwfc, for Kohn-Sham orbitals - we assume that wfcs
  ! ... have been written to tmp_dir, not to a different directory!
  ! ... io_level = 1 so that a real file is opened
  !
  wfc_dir = tmp_dir
  nwordwfc = nbnd*npwx*npol
  io_level = 1
  CALL open_buffer ( iunwfc, 'wfc', nwordwfc, io_level, exst )
  !
  ! ... Read orbitals, write them in 'distributed' form to iunwfc
  !
  CALL pw_readfile( 'wave', ierr )
  !
  ! ... Assorted initialization: pseudopotentials, PAW
  ! ... Not sure which ones (if any) should be done here
  !
  CALL init_us_1()
  !
  IF (lda_plus_u .AND. (U_projection == 'pseudo')) CALL init_q_aeps()
  !
  IF (okpaw) THEN
     becsum = rho%bec
     CALL PAW_potential(rho%bec, ddd_PAW)
  ENDIF 
  !
  IF ( real_space ) THEN
    CALL betapointlist()
    CALL init_realspace_vars()
    WRITE(stdout,'(5X,"Real space initialisation completed")')
  ENDIF
  CALL newd()
  !
  CALL close_buffer  ( iunwfc, 'KEEP' )
  !
END SUBROUTINE read_file
!
!----------------------------------------------------------------------------
SUBROUTINE read_xml_file()
  !----------------------------------------------------------------------------
  !
  ! ... This routine allocates space for all quantities already computed
  ! ... in the pwscf program and reads them from the data file.
  ! ... All quantities that are initialized in subroutine "setup" when
  ! ... starting from scratch should be initialized here when restarting
  !
  USE kinds,                ONLY : DP
  USE ions_base,            ONLY : nat, nsp, ityp, tau, if_pos, extfor
  USE cell_base,            ONLY : tpiba2, alat,omega, at, bg, ibrav
  USE large_cell_base,      ONLY : alatl => alat, omegal => omega, atl => at, bgl => bg, ibravl => ibrav!, tpiba2 => tpiba2
  USE force_mod,            ONLY : force
  USE klist,                ONLY : nkstot, nks, xk, wk
  USE lsda_mod,             ONLY : lsda, nspin, current_spin, isk
  USE wvfct,                ONLY : nbnd, nbndx, et, wg, ecutwfc
  USE symm_base,            ONLY : irt, d1, d2, d3, checkallsym
  USE ktetra,               ONLY : tetra, ntetra 
  USE extfield,             ONLY : forcefield, tefield
  USE cellmd,               ONLY : cell_factor, lmovecell
  USE fft_base,             ONLY : dfftp, dfftl, dffts
  USE fft_interfaces,       ONLY : fwfft
  USE grid_subroutines,     ONLY : realspace_grids_init, realspace_supergrid_init
  USE recvec_subs,          ONLY : ggen
  USE recvecl_subs,          ONLY : ggen_large
  USE gvect,                ONLY : gg, ngm, g, gcutm, &
                                   eigts1, eigts2, eigts3, nl, gstart
  USE gvecl,                ONLY : ggl => gg, ngml => ngm, gl => g, gcutml => gcutm, &
                                   eigts1l => eigts1, eigts2l => eigts2, eigts3l => eigts3, nll => nl, gstartl => gstart
  USE gvecs,                ONLY : ngms, nls, gcutms 
  USE spin_orb,             ONLY : lspinorb, domag
  USE scf,                  ONLY : rho, rho_core, rhog_core, v, vltot
  use scf_large,            only : vltot_large => vltot
  USE wavefunctions_module, ONLY : psic
  USE vlocal,               ONLY : strf
  USE io_files,             ONLY : tmp_dir, prefix, iunpun, nwordwfc, iunwfc
  USE noncollin_module,     ONLY : noncolin, npol, nspin_lsda, nspin_mag, nspin_gga
  USE pw_restart,           ONLY : pw_readfile
  USE read_pseudo_mod,      ONLY : readpp
  USE xml_io_base,          ONLY : pp_check_file
  USE uspp,                 ONLY : becsum
  USE uspp_param,           ONLY : upf
  USE paw_variables,        ONLY : okpaw, ddd_PAW
  USE paw_init,             ONLY : paw_init_onecenter, allocate_paw_internals
  USE ldaU,                 ONLY : lda_plus_u, eth, oatwfc, init_lda_plus_u
  USE control_flags,        ONLY : gamma_only
  USE funct,                ONLY : get_inlc, get_dft_name,dft_is_nonlocc
  USE kernel_table,         ONLY : initialize_kernel_table
  USE esm,                  ONLY : do_comp_esm, esm_ggen_2d
  USE fde
  USE fde_routines
  USE input_parameters,     ONLY : fde_xc_funct
! for debugging
   USE mp_images,       ONLY : nimage, my_image_id, intra_image_comm, inter_fragment_comm
!  USE mp_bands,        ONLY : inter_bgrp_comm, nbgrp
!  USE mp_pools,        ONLY : nproc_pool, me_pool
   use mp,                       only : mp_sum, mp_barrier, mp_bcast
!  use mp_world,                 only : world_comm
   use io_global, only : stdout, ionode, ionode_id
  !
  IMPLICIT NONE
  INTEGER  :: i, is, ik, ibnd, nb, nt, ios, isym, ierr, inlc
  REAL(DP) :: rdum(1,1), ehart, etxc, vtxc, etotefield, charge
  REAL(DP) :: sr(3,3,48)
  CHARACTER(LEN=20) dft_name
  !
  ! ... first we get the version of the qexml file
  !     if not already read
  !
  CALL pw_readfile( 'header', ierr )
  CALL errore( 'read_xml_file ', 'unable to determine qexml version', ABS(ierr) )
  !
  ! ... then we check if the file can be used for post-processing
  !
  IF ( .NOT. pp_check_file() ) CALL infomsg( 'read_xml_file', &
               & 'file ' // TRIM( tmp_dir ) // TRIM( prefix ) &
               & // '.save not guaranteed to be safe for post-processing' )
  !
  ! ... a reset of the internal flags is necessary because some codes call
  ! ... read_xml_file() more than once
  !
  CALL pw_readfile( 'reset', ierr )
  !
  ! ... here we read the variables that dimension the system
  ! ... in parallel execution, only root proc reads the file
  ! ... and then broadcasts the values to all other procs
  !
  CALL pw_readfile( 'dim',   ierr )
  CALL errore( 'read_xml_file ', 'problem reading file ' // &
             & TRIM( tmp_dir ) // TRIM( prefix ) // '.save', ierr )
   call flush_unit(stdout)
  !
  ! ... allocate space for atomic positions, symmetries, forces, tetrahedra
  !
  IF ( nat < 0 ) CALL errore( 'read_xml_file', 'wrong number of atoms', 1 )
  !
  ! ... allocation
  !
  ALLOCATE( ityp( nat ) )
  ALLOCATE( tau(    3, nat ) )
  !if ( do_fde .and. linterlock ) allocate( tau_large( 3, nat) )
  if (do_fde)  allocate(tau_large(3, nat))
  ALLOCATE( if_pos( 3, nat ) )
  ALLOCATE( force(  3, nat ) )
  ALLOCATE( extfor(  3, nat ) )
  !
  IF ( tefield ) ALLOCATE( forcefield( 3, nat ) )
  !
  ALLOCATE( irt( 48, nat ) )
  ALLOCATE( tetra( 4, MAX( ntetra, 1 ) ) )
  !
! call this once, so you can read nowave, repeat later a second time with the info read for interlock. It's an ugly mess: read_fde to read interlock needs to have read_dim first, read_nowaveto trigger read_fde requires realspace_grids_init, realspace_supergrid_init requires interlock information. 
  CALL realspace_grids_init ( dfftp, dffts, at, bg, gcutm, gcutms )

  !
  ! ... check whether LSDA
  !
  IF ( lsda ) THEN
     !
     nspin = 2
     npol  = 1
     !
  ELSE IF ( noncolin ) THEN
     !
     nspin        = 4
     npol         = 2
     current_spin = 1
     !
  ELSE
     !
     nspin        = 1
     npol         = 1
     current_spin = 1
     !
  END IF
  !
  if (cell_factor == 0.d0) cell_factor = 1.D0
  !
  ! ... allocate memory for eigenvalues and weights (read from file)
  !
  nbndx = nbnd
  ALLOCATE( et( nbnd, nkstot ) , wg( nbnd, nkstot ) )
  !
  ! ... here we read all the variables defining the system
  !
  CALL pw_readfile( 'nowave', ierr )
  !
  ! ... distribute across pools k-points and related variables.
  ! ... nks is defined by the following routine as the number 
  ! ... of k-points in the current pool
  !
   if (do_fde.and..not.linterlock) deallocate(tau_large)
  CALL set_dimensions()

  if (do_fde .and. linterlock) then
      call realspace_supergrid_init&
        ( dfftl, dfftp, atl, bgl, gcutml, frag_cell_split, fde_frag_split_type, fde_max_divisor, fde_split_types )
  endif


! repeat second time just in case
  CALL realspace_grids_init ( dfftp, dffts, at, bg, gcutm, gcutms )
  !
  CALL divide_et_impera( xk, wk, isk, lsda, nkstot, nks )
  !
  CALL poolscatter( nbnd, nkstot, et, nks, et )
  CALL poolscatter( nbnd, nkstot, wg, nks, wg )
  !
  ! ... check on symmetry
  !
  IF (nat > 0) CALL checkallsym( nat, tau, ityp, dfftp%nr1, dfftp%nr2, dfftp%nr3 )
  !
  !  Set the different spin indices
  !
  nspin_mag  = nspin
  nspin_lsda = nspin
  nspin_gga  = nspin
  IF (nspin==4) THEN
     nspin_lsda=1
     IF (domag) THEN
        nspin_gga=2
     ELSE
        nspin_gga=1
        nspin_mag=1
     ENDIF
  ENDIF
  !
  ! ... read pseudopotentials
  !
  CALL pw_readfile( 'pseudo', ierr )
  dft_name = get_dft_name () ! already set, should not be set again
  CALL readpp ( dft_name )
  !
  ! ... read the vdw kernel table if needed
  !
  if (dft_is_nonlocc()) then
  if (do_fde) then
        if (trim(fde_xc_funct) == 'SAME' ) then
           inlc = get_inlc()
        elseif (trim(fde_xc_funct) == 'RVV10' ) then
           inlc = 3
        elseif (trim(fde_xc_funct) == 'REV-VDW-DF2' .or. &
                trim(fde_xc_funct) == 'VDW-DF2-C09' .or. &
                trim(fde_xc_funct) == 'VDW-DF2' ) then
           inlc = 2
        elseif (trim(fde_xc_funct) == 'VDW-DF4'    .or. &
                trim(fde_xc_funct) == 'VDW-DF3'    .or. &
                trim(fde_xc_funct) == 'VDW-DF-C09' .or. &
                trim(fde_xc_funct) == 'VDW-DF' ) then
           inlc = 1
        else
           inlc = 0
        endif
        if (inlc > 0) call initialize_kernel_table(inlc)
  else 
        inlc = get_inlc()
        if (inlc > 0) call initialize_kernel_table(inlc)
  endif

  !if (trim(fde_xc_funct) /= 'SAME' .and. inlc .ne. 0) inlc = 0

  endif

  !
  okpaw = ANY ( upf(1:nsp)%tpawp )
  !
  IF ( .NOT. lspinorb ) CALL average_pp ( nsp )
  !
  ! ... allocate memory for G- and R-space fft arrays
  !
  CALL pre_init()

  CALL allocate_fft()

  if ( do_fde .and. linterlock ) call allocate_fft_large()

 ! moved allocate_fde earlier, because set_rhoc needs some "large". cannot be earlier than allocate_fft because allocate_fde needs ngm.
  if (do_fde)  then
       call allocate_fde
       !
       ! fde_cell_offset , tau , tau_large are all read from xml, no need to call local_offset again
       !if ( linterlock) call get_local_offset(fde_cell_offset,fde_cell_shift,frag_cell_split)
  endif
  allocate( fde_si_vec(nfragments) )
  fde_si_vec=0
  if (fde_si) fde_si_vec(currfrag) = 1
  if (ionode) call mp_sum( fde_si_vec, inter_fragment_comm )
  call mp_bcast( fde_si_vec, ionode_id, intra_image_comm )

  CALL ggen ( gamma_only, at, bg ) 
  if ( do_fde .and. linterlock ) call ggen_large( gamma_only, atl, bgl )

  IF (do_comp_esm) CALL esm_ggen_2d ()

  CALL gshells ( lmovecell ) 
  if ( do_fde .and. linterlock ) call gshells_large( lmovecell )

  ! Not sure here is the best place to initialize the datatype, but let's se...
  if (do_fde) call gen_simulation_cells()
  !
  ! ... allocate the potential and wavefunctions
  !
  CALL allocate_nlpot()
  IF (okpaw) THEN
     CALL allocate_paw_internals()
     CALL paw_init_onecenter()
     CALL d_matrix(d1,d2,d3)
  ENDIF

  CALL allocate_locpot()
  if (do_fde .and. linterlock) call allocate_locpot_large()
  !
  IF ( lda_plus_u ) THEN
     CALL init_lda_plus_u ( upf(1:nsp)%psd, noncolin )
  ENDIF
  !
  CALL allocate_wfc()
  !
  ! ... read the charge density
  !
  CALL pw_readfile( 'rho', ierr )
  !
  ! ... re-calculate the local part of the pseudopotential vltot
  ! ... and the core correction charge (if any) - This is done here
  ! ... for compatibility with the previous version of read_file
  !
!  call mp_barrier(world_comm)
  if (do_fde) call fde_summary
  CALL init_vloc()
  if (do_fde .and. linterlock) then
!     tau_large = tau
     !call get_local_offset(fde_cell_offset,fde_cell_shift,frag_cell_split)
     call init_vloc_large()
  endif
  CALL struc_fact( nat, tau, nsp, ityp, ngm, g, bg, dfftp%nr1, dfftp%nr2, &
                   dfftp%nr3, strf, eigts1, eigts2, eigts3 )
! this part copies from hinit0, where it's between struc_fact and setlocal
  if (do_fde ) then 
     strf_fde(:,:) = strf(:,:)
     !
     if (linterlock) then
        CALL struc_fact( nat_fde, tau_fde, nsp, ityp_fde, ngml, gl, bgl, &
                   dfftl%nr1, dfftl%nr2, dfftl%nr3, strf_fde_large, eigts1l, eigts2l, eigts3l )
        call calc_f2l(f2l, dfftp, dfftl, fde_cell_shift, fde_cell_offset)
        call setlocal_fde_large(vltot_large, strf_fde_large)
     else 
!        CALL struc_fact( nat_fde, tau_fde, nsp, ityp_fde, ngm, g, bg, &
!              dfftp%nr1, dfftp%nr2, dfftp%nr3, strf_fde, eigts1, eigts2, eigts3 )
        do nt = 1, nsp
            call c_grid_gather_sum_scatter( strf_fde(:,nt) )
        end do
     endif
  endif
! END: this part copies from hinit0, where it's between struc_fact and setlocal
  CALL setlocal()
  if (do_fde .and. linterlock) call copy_pot_l2f(vltot_large, vltot)
  CALL set_rhoc()
  !
  ! ... bring rho to G-space
  !
  DO is = 1, nspin
     !
     psic(:) = rho%of_r(:,is)
     CALL fwfft ('Dense', psic, dfftp)
     rho%of_g(:,is) = psic(nl(:))
     !
  END DO
  if (do_fde) call update_rho_fde(rho, .true.)
  !
  ! ... read info needed for hybrid functionals
  !
  call flush_unit(stdout)
  CALL pw_readfile('exx', ierr)
  !
  ! ... recalculate the potential
  !
  CALL v_of_rho( rho, rho_core, rhog_core, &
                 ehart, etxc, vtxc, eth, etotefield, charge, v )
  !
  RETURN
  !
  CONTAINS
    !
    !------------------------------------------------------------------------
    SUBROUTINE set_dimensions()
      !------------------------------------------------------------------------
      !
      USE constants, ONLY : pi
      USE cell_base, ONLY : alat, tpiba, tpiba2
      USE gvect,     ONLY : ecutrho, gcutm
      USE wvfct,     ONLY : ecutwfc
      USE gvecs,     ONLY : gcutms, dual, doublegrid
      use gvecl,     ONLY : gcutml => gcutm
      !
      !
      ! ... Set the units in real and reciprocal space
      !
      tpiba  = 2.D0 * pi / alat
      tpiba2 = tpiba**2
      !
      ! ... Compute the cut-off of the G vectors
      !
      gcutm = dual * ecutwfc / tpiba2
      ecutrho=dual * ecutwfc
      !
      doublegrid = ( dual > 4.D0 )
      IF ( doublegrid ) THEN
         gcutms = 4.D0 * ecutwfc / tpiba2
      ELSE
         gcutms = gcutm
      END IF
      if (do_fde .and. linterlock) gcutml = gcutm
      !
    END SUBROUTINE set_dimensions
    !
END SUBROUTINE read_xml_file
