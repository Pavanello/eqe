!
! Copyright (C) 2001-2010 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
SUBROUTINE data_structure( gamma_only )
  !-----------------------------------------------------------------------
  ! this routine sets the data structure for the fft arrays
  ! (both the smooth and the dense grid)
  ! In the parallel case, it distributes columns to processes, too
  !
  USE kinds,      ONLY : DP
  USE mp,         ONLY : mp_max
  USE mp_bands,   ONLY : me_bgrp, nproc_bgrp, root_bgrp, intra_bgrp_comm, &
                         ntask_groups
  USE mp_large,   ONLY : me_lgrp, nproc_lgrp, root_lgrp, intra_lgrp_comm
  USE mp_pools,   ONLY : inter_pool_comm
  USE fft_base,   ONLY : dfftp, dffts, dfftl
  USE cell_base,  ONLY : bg, tpiba
  USE large_cell_base,  ONLY : bgl => bg, tpibal => tpiba
  USE klist,      ONLY : xk, nks
  USE gvect,      ONLY : gcutm, gvect_init 
  USE gvecs,      ONLY : gcutms, gvecs_init
  USE gvecl,      ONLY : gcutml => gcutm, gvecl_init
  USE stick_set,  ONLY : pstickset
  USE stick_set_large,  ONLY : pstickset_large
  USE wvfct,      ONLY : ecutwfc
  use fde,        only : do_fde, linterlock
  use io_global, only : stdout!, flush_unit
  !
  IMPLICIT NONE
  LOGICAL, INTENT(in) :: gamma_only
  REAL (DP) :: gkcut
  INTEGER :: ik, ngm_, ngs_, ngw_, ngml_, ngsl_, ngwl_
  !
  ! ... calculate gkcut = max |k+G|^2, in (2pi/a)^2 units
  !
  IF (nks == 0) THEN
     !
     ! if k-points are automatically generated (which happens later)
     ! use max(bg)/2 as an estimate of the largest k-point
     !
     gkcut = 0.5d0 * max ( &
        sqrt (sum(bg (1:3, 1)**2) ), &
        sqrt (sum(bg (1:3, 2)**2) ), &
        sqrt (sum(bg (1:3, 3)**2) ) )
  ELSE
     gkcut = 0.0d0
     DO ik = 1, nks
        gkcut = max (gkcut, sqrt ( sum(xk (1:3, ik)**2) ) )
     ENDDO
  ENDIF
  gkcut = (sqrt (ecutwfc) / tpiba + gkcut)**2
  !
  ! ... find maximum value among all the processors
  !
  CALL mp_max (gkcut, inter_pool_comm )
  !
  ! ... set up fft descriptors, including parallel stuff: sticks, planes, etc.
  !
  CALL pstickset( gamma_only, bg, gcutm, gkcut, gcutms, &
                  dfftp, dffts, ngw_ , ngm_ , ngs_ , me_bgrp, &
                  root_bgrp, nproc_bgrp, intra_bgrp_comm, ntask_groups )
  !
  if (do_fde .and. linterlock) then
     gkcut = 0.d0
     CALL pstickset_large( gamma_only, bgl, gcutml, gkcut, gcutml, &
                  dfftl, dfftl, ngwl_ , ngml_ , ngsl_ , me_lgrp, &
                  root_lgrp, nproc_lgrp, intra_lgrp_comm, ntask_groups )
     call gvecl_init( ngml_ , intra_lgrp_comm )
  endif
  !     on output, ngm_ and ngs_ contain the local number of G-vectors
  !     for the two grids. Initialize local and global number of G-vectors
  !
  call gvect_init ( ngm_ , intra_bgrp_comm )
  call gvecs_init ( ngs_ , intra_bgrp_comm );
  !

END SUBROUTINE data_structure

