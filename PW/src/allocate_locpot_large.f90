
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
subroutine allocate_locpot_large
  !-----------------------------------------------------------------------
  !
  ! dynamical allocation of arrays:
  ! local potential for each kind of atom, structure factor
  !
  USE ions_base, ONLY : nat, ntyp => nsp
  USE vlocal_large,    ONLY : vloc => vloc_large, strf => strf_large
  USE gvecl,     ONLY : eigts1, eigts2, eigts3, ngm, ngl
  USE fft_base , ONLY : dfftp => dfftl
  use fde,       only : do_fde, nat_fde
  !
  implicit none
  integer :: nat0
  !
  nat0 = nat
  if (do_fde) nat0 = nat_fde 
  !
  allocate (vloc( ngl, ntyp))    
  allocate (strf( ngm, ntyp))    

  allocate( eigts1(-dfftp%nr1:dfftp%nr1,nat0) )
  allocate( eigts2(-dfftp%nr2:dfftp%nr2,nat0) )
  allocate( eigts3(-dfftp%nr3:dfftp%nr3,nat0) )

  return
end subroutine allocate_locpot_large

