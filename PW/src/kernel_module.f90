module kernel_module
!
! Module to build nonlocal Ts kernels
!
! M.P., Sept. 2015 
! W.M., Jul. 2019

  use kinds,                    only : dp
  use constants,                only : pi

  implicit none

!================================================================
!           KINETIC LUMP TYPE
!================================================================

  Type KineticLumpType

   ! is kinetic lump?
   logical :: isLump

   ! the type of lump
   character :: TheLump 

   ! fraction of lump to be included (a parameter in input)
   real(dp) :: LumpFraction, LumpAlpha

   ! Lump in G-space
   real(dp), allocatable :: LumpFunction(:)

  End Type KineticLumpType

!=================================================================
! Kinetic Electron Type 
! === Created by Wenhui Mi Apr. 11, 2018 
!================================================================
  Type KinEleType

     logical     :: isKinEle

     ! The type (name) of kinetic electron shape
     character   :: NameKinEle

     ! Parameters for Kinetic electron （Parameters from input ）
     !real(dp)    :: MGPFactor  ! the prefactor for MGP KE functional
     !real(dp)    :: MGPExp  ! the prefactor of Exp for MGP KE functional       
     real(dp)    :: LMGPA  ! the parameter for modeling the KE for LMGP functional

     ! Constant used in MGP and WT
     real(dp)    :: C_MGP = 3.1902600002091024 ! pi**2/((3._DP *pi**2)**(1._DP/3._DP)) 
     real(dp)    :: C_WT
     ! KinEle in G-space
     real(dp),allocatable :: KinEleFunction(:)

  End type KinEleType

!================================================================
!               KERNEL TYPE
!================================================================
  Type KernelType

   ! is already generated?
   logical :: isAllocated = .false.

   ! Maximum of functional integration points
   ! integer :: MaxPoints

   ! number of different LOCAL rho0 (LMGP only)
   integer :: nkf

   ! the charge of the (sub) system
   real(dp) :: charge

   ! the space of rho fpr spline of LMGP 
   real(dp) :: drho

   ! the maxrho for spline of LMGP 
   real(dp) :: RhoMax

   ! the rho_0 parameter
   real(dp) :: rho_star

   ! type of nonlocal Ts
   !real(dp) :: ThomasFermiFraction

   ! type of nonlocal Ts
   character(len=5) :: TheFunctional

   ! The Kernel in G-space
   !
   ! Must be used in conjunction with 
   ! c_grid_gather_sum_scatter( w(:,kf) )
   !
   real(dp), allocatable :: KernelFunction(:,:)

   ! if LMGP need a series of rho0 values
   real(dp), allocatable :: rhos(:)

   ! this is for handling the Kinetic Lump
   type (KineticLumpType) :: KL

   ! if LMGP need a series of values
   real(dp), allocatable :: kfs(:)

   ! for handling the Kinetic Electron 
   type (KinEleType)      :: KET

   contains

        procedure, pass :: get_rhomax

  End Type KernelType

  contains

           subroutine get_rhomax(this,cell)
                   use uspp_param, only : upf
                   use atom,       only : rgrid
                   use fde_types,  only : simulation_cell
                   use constants,  only : fpi
                   use io_global,  only : ionode, stdout
                   class(KernelType) :: this
                   type(simulation_cell),intent(in) :: cell
                   real(DP) :: rmax,rhomax
                   integer  :: nt
                   rhomax = 0.0_dp
                   do nt = 1, cell%nsp
                   rmax = maxval((upf(nt)%rho_at (:)/rgrid(nt)%r(:)**2) / fpi )
                   if (rhomax .lt. rmax) then
                           rhomax = rmax
                   endif
                   enddo
                   this%RhoMax = rhomax + 0.14
                   if (ionode) write(stdout,*) "FDE: fde_rhomax set to ", this%RhoMax
           end

!================================================================
end module kernel_module
