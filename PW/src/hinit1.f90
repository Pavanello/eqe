!
! Copyright (C) 2001-2011 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE hinit1()
  !----------------------------------------------------------------------------
  !
  ! ... Atomic configuration dependent hamiltonian initialization
  !
  USE ions_base,     ONLY : nat, nsp, ityp, tau
  USE cell_base,     ONLY : at, bg, omega, tpiba2
  USE fft_base,      ONLY : dfftp
  USE gvect,         ONLY : ngm, g
  USE gvecs,         ONLY : doublegrid
  USE ldaU,          ONLY : lda_plus_u
  USE lsda_mod,      ONLY : nspin
  USE scf,           ONLY : vrs, vltot, v, kedtau
  use scf_large,     only : vltot_large => vltot
  USE control_flags, ONLY : tqr
  USE realus,        ONLY : generate_qpointlist
  USE wannier_new,   ONLY : use_wannier
  USE martyna_tuckerman, ONLY : tag_wg_corr_as_obsolete
  USE scf,           ONLY : rho
  USE paw_variables, ONLY : okpaw, ddd_paw
  USE paw_onecenter, ONLY : paw_potential
  USE paw_init,      ONLY : paw_atomic_becsum
  USE paw_symmetry,  ONLY : paw_symmetrize_ddd
  USE dfunct,        ONLY : newd
  USE fde,           ONLY : do_fde, tau_fde, linterlock
  use fde_routines
  use input_parameters, only : pot_extrapolation , wfc_extrapolation
  !
  IMPLICIT NONE
  !
  ! ... update the atomic positions of the full system
  !
  if (do_fde .and. .not. linterlock) call gather_coordinates(tau, tau_fde)
  !
  ! ... update the wavefunctions, charge density, potential
  ! ... update_pot initializes structure factor array as well
  !
  if ( do_fde .and. &
        ( trim(pot_extrapolation) == 'scratch' .or. &
          trim(wfc_extrapolation) == 'scratch' ) )  &
  then
     CALL update_pot_scratch()
  else
     CALL update_pot()
  endif
  !
  ! ... calculate the total local potential
  !
  CALL setlocal()
  !
  if (do_fde .and. linterlock) call copy_pot_l2f(vltot_large, vltot)
  !
  ! ... define the total local potential (external+scf)
  !
  CALL set_vrs( vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid )
  !
  IF ( tqr ) CALL generate_qpointlist()
  !
  ! ... update the D matrix and the PAW coefficients
  !
  IF (okpaw) THEN
     CALL compute_becsum(1)
     CALL PAW_potential(rho%bec, ddd_paw)
     CALL PAW_symmetrize_ddd(ddd_paw)
  ENDIF
  ! 
  CALL newd()
  !
  ! ... and recalculate the products of the S with the atomic wfcs used 
  ! ... in LDA+U calculations
  !
  IF ( lda_plus_u ) CALL orthoUwfc () 
  IF ( use_wannier ) CALL orthoatwfc( .true. )
  !
  call tag_wg_corr_as_obsolete
  !
  RETURN
  !
END SUBROUTINE hinit1

