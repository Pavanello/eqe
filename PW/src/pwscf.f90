!
! Copyright (C) 2001-2013 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
PROGRAM pwscf
  !----------------------------------------------------------------------------
  !
  ! ... Main program calling one instance of Plane Wave Self-Consistent Field code
  !
  USE environment,       ONLY : environment_start
  USE mp_global,         ONLY : mp_startup
  USE io_global,         ONLY : ionode, ionode_id, stdout
  USE read_input,        ONLY : read_input_file
  USE command_line_options, ONLY: input_file_
  !
  IMPLICIT NONE
  INTEGER :: exit_status
  logical :: opnd
  character(len=256) :: filin, filout
  !
  !
  CALL mp_startup ( )
  CALL environment_start ( 'PWSCF' )
  ! open input files
  if ( trim (input_file_) == ' ') then
     filin = 'pw' //  '.in'
  else
     filin = trim(input_file_) // '.in'
  end if
  ! open output file
  if (ionode) then
     inquire(unit=stdout, opened=opnd)
     if (opnd) close(unit=stdout)
     filout = 'pw' //  '.out'
     if (trim(input_file_) /= ' ') &
        filout = trim(input_file_) //  '.out'
     open(unit=stdout, file=trim(filout), status='unknown')
  end if
  !
  CALL read_input_file ('PW', input_file_=filin )
  !
  ! ... Perform actual calculation
  !
  CALL run_pwscf  ( exit_status )
  !
  CALL stop_run( exit_status )
  CALL do_stop( exit_status )
  !
  STOP
  !
END PROGRAM pwscf
