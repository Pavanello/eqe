!
! Copyright (C) 2001-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!

! This files contains routines to evaluate the non-additive kinetice
! energy semilocal functionals for the Frozen Density Embedding method [1,2].
!
! Coded by D. Ceresoli and A. Genova, during the April 2013 PRG hackathon
! in Rutgers-Newark (http://www.michelepavanello.com/pdf/hackathon.pdf)
!
! [1] T. A. Wesolowksi and A. Warshel, J. Phys. Chem. 97, 8050 (1993) 
! [2] see: $QEDIR/FDE-Notes/fde-equations.tex

Module fde_functionals
implicit none
contains
! Thomas-Fermi
SUBROUTINE fde_kin_tf (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  Fs = 1.d0
  dF_ds = 0.d0
END SUBROUTINE fde_kin_tf


! von Weizsacker
SUBROUTINE fde_kin_vw (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  Fs = 5.d0/3.d0 * s*s
  dF_ds = 10.d0/3.d0 * s
END SUBROUTINE fde_kin_vw

! Damped von Weizsacker
SUBROUTINE fde_kin_dvw (nr, s, Fs, dF_ds)
  use kinds, only : dp
  use io_global, only : stdout
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter  :: B = 5.d0
  real(dp), parameter  :: C = 3.d0
  write(stdout,*) 'Damped VW!!'
  Fs = (5.d0*s**2*(1.d0 - Tanh(B*(-C + s))))/6.d0 
  dF_ds = (-5.d0*B*s**2*(1.d0/Cosh(B*(-C + s)))**2)/6.d0 - (5.d0*s*(1.d0 - Tanh(B*(-C + s))))/3.d0
END SUBROUTINE fde_kin_dvw


! 2nd order DGE
SUBROUTINE fde_kin_dge2 (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  Fs = 1.d0 + 5.d0/27.d0 * s**2
  dF_ds = 10.d0/27.d0 * s
END SUBROUTINE fde_kin_dge2


! Lee, Lee, Parr
SUBROUTINE fde_kin_llp (nr, s, Fs, dF_ds)
  use kinds, only : dp
  use constants, only : pi
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter :: beta = 2.d0*(6.d0*pi*pi)**(1.d0/3.d0)
  real(dp), parameter :: b1 = 0.0044188d0
  real(dp), parameter :: b2 = 0.0253d0
  Fs = 1.d0 + (b1 * (beta*s)**2) / (1.d0 + b2*beta*s*asinh(beta*s))
  dF_ds = (2*beta*beta*b1*2)/(1.d0+beta*b2*s*asinh(beta*s)) - &
     (b1*(beta*s)**2 * (beta*beta*b2*s/sqrt(1.d0+(beta*s)**2) + beta*b2*asinh(beta*s))) / &
     (1.d0 + b2*beta*s*asinh(beta*s))**2
#ifdef __XLF
CONTAINS
FUNCTION asinh ( x )
  use kinds, only : dp
  implicit none
  real(dp), intent(in) :: x(:)
  real(dp), allocatable :: asinh(:)
  allocate( asinh(size(x)) )
  asinh = log( x + sqrt(x**2 + 1) )
  return
END FUNCTION asinh
#endif
END SUBROUTINE fde_kin_llp


! cojoint Perdew, Wang
SUBROUTINE fde_kin_pw86 (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter :: b1 = 1.296d0
  real(dp), parameter :: b2 = 14.d0
  real(dp), parameter :: b3 = 0.2d0
  Fs = (1.d0 + b1*s**2 + b2*s**4 + b3*s**6)**(1.d0/15.d0)
  dF_ds = (2*b1*s + 4*b2*s**3 + 6*b3*s**5) / (15.*(1 + b1*s**2 + b2*s**4 + &
           b3*s**6)**(14.d0/15.d0))
END SUBROUTINE fde_kin_pw86


! Lembarki, Chermette
SUBROUTINE fde_kin_lc94 (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter :: A0 = 76.32d0
  real(dp), parameter :: A1 = 0.093907d0
  real(dp), parameter :: A2 = 0.26608d0
  real(dp), parameter :: A3 = 0.0809615d0
  real(dp), parameter :: A4 = 100.d0
  real(dp), parameter :: A5 = 0.57767d-4
  Fs = (1.d0 + (A2 - A3 * exp(-A4*s**2))*s**2 + A1*s*asinh(A0*s)) / &
       (1 + A5*s**4 + A1*s*asinh(A0*s))
  dF_ds = -(((4.d0*A5*s**3 + (A0*A1*s)/sqrt(1.d0 + A0**2*s**2) + A1*asinh(A0*s))* &
         (1.d0 + (A2 - A3*exp(-A4*s**2))*s**2 + A1*s*asinh(A0*s)))/(1.d0 + A5*s**4 + &
         A1*s*asinh(A0*s))**2) + &
         (2.d0*(A2 - A3*exp(-A4*s**2))*s + 2.d0*A3*A4*s**3*exp(-A4*s**2) + &
         (A0*A1*s)/sqrt(1.d0 + A0**2*s**2) + A1*asinh(A0*s)) / &
         (1.d0 + A5*s**4 + A1*s*asinh(A0*s))
#ifdef __XLF
CONTAINS
FUNCTION asinh ( x )
  use kinds, only : dp
  implicit none
  real(dp), intent(in) :: x(:)
  real(dp), allocatable :: asinh(:)
  allocate( asinh(size(x)) )
  asinh = log( x + sqrt(x**2 + 1) )
  return
END FUNCTION asinh
#endif
END SUBROUTINE fde_kin_lc94


! asymptotic PBE (APBEK)
SUBROUTINE fde_kin_apbek (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter :: a1 = 0.23899d0
  real(dp), parameter :: a2 = 0.804d0
  Fs = 1.d0 + a1*s*s/(1.d0 + (a1/a2)*s*s)
  dF_ds = (2.d0 * a1*a2*a2*s) / (a1*s*s + a2)**2
END SUBROUTINE fde_kin_apbek

! Rev asymptotic PBE (revAPBEK)
SUBROUTINE fde_kin_rapbek (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter :: a1 = 0.23899d0
  real(dp), parameter :: a2 = 1.245d0
  Fs = 1.d0 + a1*s*s/(1.d0 + (a1/a2)*s*s)
  dF_ds = (2.d0 * a1*a2*a2*s) / (a1*s*s + a2)**2
END SUBROUTINE fde_kin_rapbek


! TFP
SUBROUTINE fde_kin_tfp (nr, s, Fs, dF_ds)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  real(dp), intent(in) :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  real(dp), parameter :: a1 = 5.d0/27.d0
  !real(dp), parameter :: a1 = 0.23899d0
  real(dp), parameter :: a2 = 5.0d0
  !real(dp), parameter :: a2 = 1.245d0
  Fs = 1.d0 + a1*s*s/(1.d0 + (a1/a2)*s*s)
  dF_ds = (2.d0 * a1*a2*a2*s) / (a1*s*s + a2)**2
END SUBROUTINE fde_kin_tfp

SUBROUTINE fde_kin_tfvW (nr, s, Fs, dF_ds)
  use kinds, only : dp
  use fde, only : fde_tvpbea

  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr)
  real(dp), intent(out) :: Fs(nr), dF_ds(nr)
  !real(dp), parameter   :: fde_tvpbea= 0.1d0

  Fs = 1.d0 + 5.d0/3.d0*s*s/(1.0 + fde_tvpbea*s*s)
  dF_ds = 10.d0/3.d0*s/(1.0+fde_tvpbea*s*s)**2.0

END SUBROUTINE fde_kin_tfvw

SUBROUTINE fde_kin_tfl (nr, s, q, Fs, dF_dp, dF_dq)
  use kinds, only : dp
  !use fde

  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr), q(nr)
  real(dp), intent(out) :: Fs(nr),dF_dp(nr),dF_dq(nr)
  real(dp)              :: p(nr)

  p=s*s
  Fs = 1.d0 + 20.0/9.0*q
  dF_dp =0.0
  dF_dq = 20.0/9.0
  
END SUBROUTINE fde_kin_tfl

SUBROUTINE fde_kin_L04 (nr, s, q, Fs, dF_dp, dF_dq)
  use kinds, only : dp
  !use fde

  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr), q(nr)
  real(dp), intent(out) :: Fs(nr),dF_dp(nr),dF_dq(nr)
  real(dp)              :: p(nr)
  real(dp)              :: dFdx1(nr),dFdx2(nr),x1(nr),x2(nr)
  real(dp)              :: p5(nr),dedp(nr),dedq(nr),delta(nr)
  real(dp)              :: dx1dp(nr),dx1dq(nr),dx2dp(nr),dx2dq(nr)
  real(dp), parameter   :: k = 0.402d0

  p=s*s
  p5 = 5.0*p/27.0
  delta = (8.0/81.0)*q*q -(1.0/9.0)*p*q + (8.0/243.0)*p**2.0
  dedp = -(1.0/9.0)*q + (16.0/243.0)*p
  dedq = (16.0/81.0)*q -(1.0/9.0)*p
  x1 = p5 + delta + p5*p5/k
  x2 =2.0*p5*delta/k + p5**3.0/k**2.0
  Fs = 1.0 + 2.0*k - k*(1.0/(1.0 + x1/k) + 1.0/(1.0 + x2/k) )

  dfdx1 = 1.0/(1.0 + x1/k)**2.0
  dfdx2 = 1.0/(1.0 + x2/k)**2.0

  dx1dp = dedp + 5.0/27.0 + 2.0*p5*5.0/27.0/k
  dx1dq = dedq

  dx2dp = 10.0/27.0*delta/k + 2.0*p5*dedp/k
  dx2dp = dx2dp + 5.0/9.0*p5**2.0/k**2
  dx2dq = 2.0*p5*dedq/k

  dF_dp = dfdx1*dx1dp + dfdx2*dx2dp
  dF_dq = dfdx1*dx1dq + dfdx2*dx2dq
  
END SUBROUTINE fde_kin_L04

SUBROUTINE fde_kin_GEA2L (nr, s, q, Fs, dF_dp, dF_dq)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr), q(nr)
  real(dp), intent(out) :: Fs(nr),dF_dp(nr),dF_dq(nr)
  real(dp)              :: p(nr),fw(nr)

 p=s*s 
 fw =5.0/3.0 * p
 Fs = 1.0 + 1.0/9.0*fw + 20.0/9.0*q
 dF_dp = 5.0/27.0
 DF_dq = 20.0/9.0
 do i =1,nr
    if(Fs(i) < fw(i)) then
      Fs(i) =Fw(i)
      dF_dP(i) = 5.0/3.0
      dF_dq(i) = 0.0
   endif
 enddo

END subroutine fde_kin_GEA2L 

SUBROUTINE fde_kin_TFLopt(nr, s, q, Fs, dF_dp, dF_dq)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr), q(nr)
  real(dp), intent(out) :: Fs(nr),dF_dp(nr),dF_dq(nr)
  real(dp)              :: p(nr),fw(nr)
  
  p=s*s
  fw= 5.0d0/3.0d0*s*s
  Fs = 1.0d0 -0.203519d0*s*s + 2.513880d0*q
  dF_dP =  -0.203519
  DF_dq = 2.513880
  do i=1,nr
    if(fw(i) > Fs(i)) then
      Fs(i)=fw(i)
      dF_dP(i) = 5.0/3.0
      Df_dq(i) =0.0
    endif
  enddo
END SUBROUTINE fde_kin_TFLopt

SUBROUTINE fde_kin_PCopt(nr, s, q, Fs, dF_dp, dF_dq)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr), q(nr)
  real(dp), intent(out) :: Fs(nr),dF_dp(nr),dF_dq(nr)
  real(dp)              :: Fge4(nr),delta(nr),z(nr),fab(nr)
  real(dp)              :: dfabdz(nr),dzdp(nr),dzdq(nr)
  real(dp)              :: tmp(nr),tmp2(nr),dtmd(nr)
  real(dp)              :: p(nr),fw(nr)
  real(dp), parameter  :: a = 1.784720, b=0.258304 ! For PC_opt

  p = s*s
  fw =5.0/3.0*p
  delta = (8.0/81.0)*q*q -(1.0/9.0)*p*q + (8.0/243.0)*p**2.0
  Fge4 = 1.0 + (5.0/27.0)*p +(20.0/9.0)*q + delta

  tmp = 1.0 + (delta/(1.0 + fw))**2.0
  tmp2 = 2.0*delta/(1+fw)**3.0
  dtmd =( (-1.0/9.0)*q+ (16.0/243.0)*p )*(1+fw)
  dtmd = dtmd - (5.0/3.0)*delta
  dtmd = dtmd*tmp2
  DF_dp =(5.0/27.0 + 16.0/243.0*p-1.0/9.0*q) /sqrt(tmp)
  DF_DP = DF_DP-0.5*Fge4*dtmd/tmp**(1.5)

! For dq

  dtmd = ((16.0/81.0)*q - (1.0/9.0)*p)*(1+fw)
  dtmd = dtmd*tmp2
  DF_Dq = (20.0/9.0 + 16.0/81.0*q -1.0/9.0*p)/sqrt(tmp)
  DF_Dq = DF_Dq - 0.5*Fge4*dtmd/tmp**(1.5)

  Fge4 = Fge4/sqrt(tmp)
!!!!
  z=Fge4 - fw
  dzdp = Df_DP -5.0/3.0
  dzdq = Df_Dq
!!!!!
  dfabdz=0
  fab=1.0
  do i=1,nr
    if (z(i) <= 0.1) then
       fab(i) = 0.0
       dfabdz(i) = 0.0
    elseif (z(i) >= (a - 0.1) ) then
       fab(i) = 1.0
       dfabdz(i) =0.0
    else
       tmp(i) = exp(a/(a-z(i)))
       tmp2(i) = exp(a/z(i))
       fab(i) =(1.0 + tmp(i))/(tmp2(i) + tmp(i))
       dfabdz(i) = b*fab(i)**(b-1.0)
       fab(i) =fab(i)**b
       dfabdz(i) = dfabdz(i)*   (a/(a-z(i))**2 *tmp(i)*(tmp(i)+tmp2(i)) &
              - (1.0 + tmp(i))*  a/(a-z(i))**2 *tmp(i)-a/z(i)**2*tmp2(i) )
       dfabdz(i) = dfabdz(i)/(tmp(i)+tmp2(i))**2
    endif
  enddo
  !fab=1.0
  Fs = fw + z*fab
  !dfabdz =0
  dF_dp = 5.0/3.0 + dzdp*fab + z*dfabdz*dzdp
  dF_dq = dzdq*fab + z*dfabdz*dzdq
!
END SUBROUTINE fde_kin_PCopt

SUBROUTINE fde_kin_PC(nr, s, q, Fs, dF_dp, dF_dq)
  use kinds, only : dp
  implicit none
  integer, intent(in) :: nr
  integer   :: i
  real(dp), intent(in)  :: s(nr), q(nr)
  real(dp), intent(out) :: Fs(nr),dF_dp(nr),dF_dq(nr)
  real(dp)              :: Fge4(nr),delta(nr),z(nr),fab(nr)
  real(dp)              :: dfabdz(nr),dzdp(nr),dzdq(nr)
  real(dp)              :: tmp(nr),tmp2(nr),dtmd(nr)
  real(dp)              :: p(nr),fw(nr)
  real(dp), parameter  :: a = 0.5389, b=3.0 ! For PC

  p = s*s
  fw =5.0/3.0*p
  delta = (8.0/81.0)*q*q -(1.0/9.0)*p*q + (8.0/243.0)*p**2.0
  Fge4 = 1.0 + (5.0/27.0)*p +(20.0/9.0)*q + delta

  tmp = 1.0 + (delta/(1.0 + fw))**2.0
  tmp2 = 2.0*delta/(1+fw)**3.0
  dtmd =( (-1.0/9.0)*q+ (16.0/243.0)*p )*(1+fw)
  dtmd = dtmd - (5.0/3.0)*delta
  dtmd = dtmd*tmp2
  DF_dp =(5.0/27.0 + 16.0/243.0*p-1.0/9.0*q) /sqrt(tmp)
  DF_DP = DF_DP-0.5*Fge4*dtmd/tmp**(1.5)

! For dq

  dtmd = ((16.0/81.0)*q - (1.0/9.0)*p)*(1+fw)
  dtmd = dtmd*tmp2
  DF_Dq = (20.0/9.0 + 16.0/81.0*q -1.0/9.0*p)/sqrt(tmp)
  DF_Dq = DF_Dq - 0.5*Fge4*dtmd/tmp**(1.5)

  Fge4 = Fge4/sqrt(tmp)
!!!!
  z=Fge4 - fw
  dzdp = Df_DP -5.0/3.0
  dzdq = Df_Dq
!!!!!
  dfabdz=0
  fab=1.0
  do i=1,nr
    if (z(i) <= 0.02) then
       fab(i) = 0.0
       dfabdz(i) = 0.0
    elseif ((z(i) >= a - 0.02) ) then
       fab(i) = 1.0
       dfabdz(i) =0.0
    else
       tmp(i) = exp(a/(a-z(i)))
       tmp2(i) = exp(a/z(i))
       fab(i) =(1.0 + tmp(i))/(tmp2(i) + tmp(i))
       dfabdz(i) = b*fab(i)**(b-1.0)
       fab(i) =fab(i)**b
       dfabdz(i) = dfabdz(i)*( a/(a-z(i))**2 *tmp(i)*(tmp(i)+tmp2(i)) &
              - (1.0 + tmp(i))* (a/(a-z(i)**2)*tmp(i)-a/(z(i)**2)*tmp2(i))  )
       dfabdz(i) = dfabdz(i)/(tmp(i)+tmp2(i))**2
    endif
  enddo
  !fab=1.0
  Fs = fw + z*fab
  !dfabdz =0
  dF_dp = 5.0/3.0 + dzdp*fab + z*dfabdz*dzdp
  dF_dq = dzdq*fab + z*dfabdz*dzdq
!
END SUBROUTINE fde_kin_PC

END Module fde_functionals
