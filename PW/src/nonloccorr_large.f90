!
! Copyright (C) 2001-2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!----------------------------------------------------------------------------
SUBROUTINE nonloccorr_large( rho, rho_core, enl, vnl, v )
  !----------------------------------------------------------------------------
  !
  !USE constants,            ONLY : e2
  USE kinds,                ONLY : DP
  !USE gvect,                ONLY : nl, ngm, g
  USE lsda_mod,             ONLY : nspin
  !USE cell_base,            ONLY : omega, alat
  USE funct,                ONLY : dft_is_nonlocc, get_inlc, nlc_large
  !USE spin_orb,             ONLY : domag
  !USE noncollin_module,     ONLY : ux
  USE fft_base,             ONLY : dfftp => dfftl
  !USE fft_interfaces,       ONLY : fwfft
  !USE fde,                  ONLY: do_fde
  USE input_parameters,     ONLY : fde_xc_funct

  !
  IMPLICIT NONE
  !
  REAL(DP),    INTENT(IN)    :: rho(dfftp%nnr,nspin), rho_core(dfftp%nnr)
  REAL(DP),    INTENT(INOUT) :: v(dfftp%nnr,nspin)
  REAL(DP),    INTENT(INOUT) :: vnl, enl
  !
  INTEGER :: k, ipol, is, nspin0, ir, jpol
  INTEGER :: inlc
  !
  !
  REAL(DP), PARAMETER :: epsr = 1.D-6, epsg = 1.D-10
  !
  !
        if (trim(fde_xc_funct) == 'SAME' ) then
           inlc = get_inlc()
        elseif (trim(fde_xc_funct) == 'RVV10' ) then
           inlc = 3
        elseif (trim(fde_xc_funct) == 'REV-VDW-DF2' .or. &
                trim(fde_xc_funct) == 'VDW-DF2-C09' .or. &
                trim(fde_xc_funct) == 'VDW-DF2' ) then
           inlc = 2
        elseif (trim(fde_xc_funct) == 'VDW-DF4'    .or. &
                trim(fde_xc_funct) == 'VDW-DF3'    .or. &
                trim(fde_xc_funct) == 'VDW-DF-C09' .or. &
                trim(fde_xc_funct) == 'VDW-DF' ) then
           inlc = 1
        else
           inlc = get_inlc()
        endif

  !
  ! Everything is summed inside the proc
  !

  if (dft_is_nonlocc() .and. (inlc .ne. 0) .and. &
       fde_xc_funct /= 'SAME' ) then
     call errore('nonloccorr_large','Nonlocal XC must be specified as input_dft',1)
  endif

  if (dft_is_nonlocc() .or. (inlc .ne. 0)) CALL nlc_large( rho, rho_core, nspin, enl, vnl, v , inlc)
  !
  RETURN
  !
END SUBROUTINE nonloccorr_large 





