subroutine test_sum_band_selective()

  use scf , only : scf_type, create_scf_type, rho
  use io_global, only : stdout, ionode
  use kinds, only : dp
  use fft_base, only : dfftp
  use mp, only : mp_sum
  use mp_bands, only : intra_bgrp_comm
  use cell_base, only : omega
  use fde_routines, only : plot_dense
  
  implicit none

  type(scf_type) :: homo
  integer, allocatable :: band_vec(:)
  real(dp) :: chg_homo, chg_rho, domega
  integer :: nbnd, ir
  

  call create_scf_type(homo)
  
  nbnd = 1
  allocate( band_vec(nbnd) )
  band_vec = (/ 4 /)

  call sum_band_selective(nbnd, band_vec, homo)

  chg_homo = 0.d0
  chg_rho = 0.d0
  do ir = 1, dfftp%nnr
     chg_homo = chg_homo + homo%of_r(ir,1)
     chg_rho = chg_rho + rho%of_r(ir,1)
  enddo

  call mp_sum( chg_homo, intra_bgrp_comm )
  call mp_sum( chg_rho, intra_bgrp_comm )

  domega = omega / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3)

  chg_homo = chg_homo*domega
  chg_rho = chg_rho*domega

  write( stdout, * ) 'Selected bands charge:  ', chg_homo, chg_rho

  call plot_dense( homo%of_r(:,1), 'homo.pp         ' )


end subroutine test_sum_band_selective
