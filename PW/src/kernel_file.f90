module kernel_File_module
   !!###################################################!!
   !!************* author: Wenhui Mi          **********!!
   !!************* date  : 2019-02-17          *********!!
   !!************* kernel file INF.   *********!!
   !!###################################################!!
!   use constants
use kinds,        ONLY: DP
!use fde_routines, ONLY: polynom
   implicit none
   INTEGER,  PARAMETER      :: I4B = SELECTED_INT_KIND(9)
!   INTEGER,  PARAMETER      :: DP = KIND(1.0D0)
   type kernel_File
      INTEGER(I4B)     :: numpoints
      INTEGER(I4B)     :: num_range
      character(30)    :: FileName
      real(dp)         :: gmax_eta
      real(dp),dimension (:), pointer   :: lmax_eta
      real(dp),dimension (:), pointer   :: d_eta
      real(dp),dimension (:), pointer   :: d_points
      real(dp),dimension (:), pointer   :: KFpotvalue
      real(dp),dimension (:), pointer   :: Eta
   end type kernel_File
contains
   
   subroutine destroy_KTfile(KF)
      implicit none
      type(kernel_File), intent (inout) :: KF
      if (associated (KF%lmax_eta)) deallocate (KF%lmax_eta)
      if (associated (KF%d_eta))     deallocate (KF%d_eta)
      if (associated (KF%d_points))  deallocate (KF%d_points)
      if (associated (KF%KFpotvalue))   deallocate (KF%KFpotvalue)
      if (associated (KF%eta)) deallocate (KF%eta)
   end subroutine destroy_KTfile
   subroutine create_KTfile(KF,ND,np)
      implicit none
      integer(I4B),intent(in)           :: ND,np
      type(kernel_File), intent (inout) :: KF
      allocate(KF%lmax_eta(ND))
      allocate(KF%d_eta(ND))
      allocate(KF%d_points(ND))
      allocate(KF%KFpotvalue(np))
      allocate (KF%eta(np))
   end subroutine create_KTfile
   
     subroutine readkernel(KF)!{{{
      !!#################################################!!
      !!*********  author: Wenhui Mi  *******************!!
      !!*********  date  : 2019-02-17 *******************!!
      !!*********  Read Kernel_Table.dat files  **!!
      !!#################################################!!
      use constants
      use io_global,                only : stdout
   !   use MathSplines
      implicit none
      type(kernel_File),intent (inout)      :: KF
      character(30)                         :: filename
      INTEGER(I4B)                          :: i,j,k,np,filestatu,positions,allocatestatus,readstatu
      character(30)              :: KFline,KFhead
      INTEGER(8)                 :: max_points
      INTEGER(I4B)               :: ii
      INTEGER(I4B)               :: KFunit
      INTEGER(I4B),allocatable   :: d(:)
      logical                    :: lopen
      
      
       
      KFunit = 408
!      Filename="Kernel_Table.dat"
      Filename=trim(KF%FileName)
      open(UNIT=KFunit,FILE=FileName,IOSTAT=filestatu)
      if (filestatu/=0) then
!         write(stdout,*) "My guess: The Kernel file does not exit "
         call errore("Could not open ", trim(FileName),".")
      end if
        !read until we find an END COMMENT
      KFline=""
      do while(index(KFline,"END COMMENT")==0)
         read(KFunit,'(A)')KFline
      end do
      read(KFunit,*)KF%numpoints,KF%num_range,KF%gmax_eta
      call create_KTfile(KF,KF%num_range,KF%numpoints)
      backspace(KFunit )
      read(KFunit,*)KF%numpoints,KF%num_range,KF%gmax_eta,KF%d_eta(:)
      read(KFunit,*)KF%d_points(:),KF%lmax_eta(:)
      
      ! Check the number of kernel points if equal to the number in header!!
      ii=0
      do
         read(KFunit,*,IOSTAT=readstatu)KFline
         if(trim(KFline)== "EOF") exit
         ii=ii+5
      end do
      if(ii .ne. KF%numpoints) then
         call errore("The number of points in kernel file is not same as header.")
      endif
      close(KFunit)
         !Check if terminator
      if (KFline(1:3) /="EOF") then
         call errore("Kernel file lacks the proper terminator")
      endif

      KF%FileName=filename
      open(UNIT=KFunit,FILE=KF%FileName,IOSTAT=filestatu)
      KFline=""
      do while(index(KFline,"END COMMENT")==0)
         read(KFunit,'(a)')KFline
      end do
      !skip the already read date
      read(KFunit,'(a)') KFline
      read(KFunit,'(a)') KFline
      ! Now read the rest of pseudopotential
      read(KFunit,*)KF%KFpotvalue(1:KF%numpoints)
      close(KFunit)
      
      ! Calculate the eta
      allocate(d(KF%num_range))
      do i=1,KF%num_range
         d(i)=0
         do j=1,i
            d(i)=d(i)+KF%d_points(j)
         enddo
      enddo

      do i=1,KF%numpoints
         do j =1,KF%num_range
            if (i <= d(j)) exit
         enddo
         if (j==1) then
            KF%eta(i)= dble(i)*KF%d_eta(1)
         else
            np=0
            do k=1,j-1
               np=np+KF%d_points(k)
            enddo
            KF%eta(i)= dble(i-np)*KF%d_eta(j)+KF%lmax_eta(j-1)
         endif
      end do           
      deallocate(d)
   end subroutine readkernel!}}}    
   
 real(DP) function polynom(m,np,xa,ya,c,x)
! !INPUT/OUTPUT PARAMETERS:
!   m  : order of derivative (in,integer)
!   np : number of points to fit (in,integer)
!   xa : abscissa array (in,real(np))
!   ya : ordinate array (in,real(np))
!   c  : work array (out,real(np))
!   x  : evaluation abscissa (in,real)
! !DESCRIPTION:
!   Fits a polynomial of order $n_p-1$ to a set of $n_p$ points. If $m\ge 0$ the
!   function returns the $m$th derviative of the polynomial at $x$, while for
!   $m<0$ the integral of the polynomial from the first point in the array to
!   $x$ is returned.
!
! !REVISION HISTORY:
!   Created October 2002 (JKD)
!EOP
!BOC
use kinds,     ONLY: DP
implicit none
! argmuments
integer, intent(in) :: m
integer, intent(in) :: np
real(DP), intent(in) :: xa(np)
real(DP), intent(in) :: ya(np)
real(DP), intent(out) :: c(np)
real(DP), intent(in) :: x
! local variables
integer i,j,k
real(DP) x0,x1,x2,x3,y0,y1,y2,y3
real(DP) t0,t1,t2,t3,t4,t5,t6
real(DP) c1,c2,c3,sum
! fast evaluations for small np
select case(np)
case(1)
  select case(m)
  case(:-1)
    polynom=ya(1)*(x-xa(1))
  case(0)
    polynom=ya(1)
  case default
    polynom=0.d0
  end select
  return
case(2)
  c1=(ya(2)-ya(1))/(xa(2)-xa(1))
  t1=x-xa(1)
  select case(m)
  case(:-1)
    polynom=t1*(ya(1)+0.5d0*c1*t1)
  case(0)
    polynom=c1*t1+ya(1)
  case(1)
    polynom=c1
  case default
    polynom=0.d0
  end select
  return
case(3)
  x0=xa(1)
  x1=xa(2)-x0
  x2=xa(3)-x0
  y0=ya(1)
  y1=ya(2)-y0
  y2=ya(3)-y0
  t0=1.d0/(x1*x2*(x2-x1))
  t1=x1*y2
  t2=x2*y1
  c1=x2*t2-x1*t1
  c2=t1-t2
  t1=x-x0
  select case(m)
  case(:-1)
    polynom=t1*(y0+t0*t1*(0.5d0*c1+0.3333333333333333333d0*c2*t1))
  case(0)
    polynom=y0+t0*t1*(c1+c2*t1)
  case(1)
    polynom=t0*(2.d0*c2*t1+c1)
  case(2)
    polynom=t0*2.d0*c2
  case default
    polynom=0.d0
  end select
  return
case(4)
  x0=xa(1)
  x1=xa(2)-x0
  x2=xa(3)-x0
  x3=xa(4)-x0
  y0=ya(1)
  y1=ya(2)-y0
  y2=ya(3)-y0
  y3=ya(4)-y0
  t0=1.d0/(x1*x2*x3*(x1-x2)*(x1-x3)*(x2-x3))
  t1=x1*x2*y3
  t2=x2*x3*y1
  t3=x3*x1*y2
  c3=t1*(x1-x2)+t2*(x2-x3)+t3*(x3-x1)
  t6=x3**2
  t5=x2**2
  t4=x1**2
  c2=t1*(t5-t4)+t2*(t6-t5)+t3*(t4-t6)
  c1=t1*(x2*t4-x1*t5)+t2*(x3*t5-x2*t6)+t3*(x1*t6-x3*t4)
  t1=x-x0
  select case(m)
  case(:-1)
    polynom=t1*(y0+t0*t1*(0.5d0*c1+t1*(0.3333333333333333333d0*c2 &
     +0.25d0*c3*t1)))
  case(0)
    polynom=y0+t0*t1*(c1+t1*(c2+c3*t1))
  case(1)
    polynom=t0*(c1+t1*(2.d0*c2+3.d0*c3*t1))
  case(2)
    polynom=t0*(6.d0*c3*t1+2.d0*c2)
  case(3)
    polynom=t0*6.d0*c3
  case default
    polynom=0.d0
  end select
  return
end select
if (np.le.0) then
  write(*,'("Error(polynom): np <= 0 : ",I8)') np
  call errore('Wrong spline.')
end if
if (m.ge.np) then
  polynom=0.d0
  return
end if
! find the polynomial coefficients in divided differences form
c(:)=ya(:)
do i=2,np
  do j=np,i,-1
    c(j)=(c(j)-c(j-1))/(xa(j)-xa(j+1-i))
  end do
end do
! special case m=0
if (m.eq.0) then
  sum=c(1)
  t1=1.d0
  do i=2,np
    t1=t1*(x-xa(i-1))
    sum=sum+c(i)*t1
  end do
  polynom=sum
  return
end if
x0=xa(1)
! convert to standard form
do j=1,np-1
  do i=1,np-j
    k=np-i
    c(k)=c(k)+(x0-xa(k-j+1))*c(k+1)
  end do
end do
if (m.gt.0) then
! take the m'th derivative
  do j=1,m
    do i=m+1,np
      c(i)=c(i)*dble(i-j)
    end do
  end do
  t1=c(np)
  t2=x-x0
  do i=np-1,m+1,-1
    t1=t1*t2+c(i)
  end do
  polynom=t1
else
! find the integral
  t1=c(np)/dble(np)
  t2=x-x0
  do i=np-1,1,-1
    t1=t1*t2+c(i)/dble(i)
  end do
  polynom=t1*t2
end if
return
end function
!EOC
!===========================================================================================
   Function OmegaOfEta (eta,KF)
     
    !use  kernel_File_module 
     implicit none
   
     type(kernel_File),intent(in)        ::  KF    ! Kernel file information
     real(DP), intent(IN)                 ::  eta   ! the point at which the function getsevaluated.

     real(DP)                             ::  OmegaOfEta
     integer (I4B)                        ::  i,j,ir,np
   
     ! Local
     real(DP)                             :: c3(3)
!     real(DP),external                    :: polynom    
        
     if (eta >=KF%lmax_eta(KF%num_range)) then
         OmegaofEta=-6.125308117
         return
     endif
    ! Determine which range that Eta is located 
     do i=1,KF%num_range
        if(eta <= KF%lmax_eta(i)) then
          ir=i
          exit
        end if
     end do
 
     ! Ecaluation the nearst of eata
      np=0
      if(ir==1) then
         np=floor(eta/KF%d_eta(1))
      else 
         do j=1,ir-1
             np=np+KF%d_points(j)
         enddo
         np=np + floor((eta-KF%lmax_eta(ir-1))/KF%d_eta(ir))
       end if 

     ! Spline with the nearst
       if (np < 1) then
         OmegaOfEta = 1._DP/(0.5_DP + 0.25_DP *(1._DP-eta**2)*LOG((1._DP +eta)/&
                      abs(1._DP-eta))/eta)-1._dp-3._DP*eta**2
         OmegaOfEta = 1.2_DP*3.1902600002091024_DP*OmegaOfEta
         if (abs(eta) < 1.0E-10) OmegaOfEta =0.0_dp
       else if (np >= KF%numpoints) then
         OmegaOfEta=-6.125308117
       else
         OmegaOfEta = polynom(0,2,KF%eta(np:np+1),KF%KFpotvalue(np:np+1),c3,eta)
       endif
 
       ! return OmegaOfEta
   End function  
    
end module kernel_File_module 
