#! /usr/bin/env python3

import QEPP
import numpy as np

#import scipy as sp
#import string
import argparse


parser = argparse.ArgumentParser(description='Interpolate a QE PP file from a regular grid to another')
parser.add_argument('-i', '--input', nargs=2, help = 'Name of the two input PP files.', required = True)
args = parser.parse_args()

filespp = args.input

pps=[]
intrho=[]
deltarho = 0.
print(filespp)

for ipp, ppin in enumerate(filespp):
    print(ipp)
    pps.append(QEPP.PP(ppin))
    pps[ipp].readpp()
    intrho.append(np.sum(pps[ipp].plot.values))
    print('SUMRHO of PP',ipp,' = ',intrho[ipp])

deltarho = np.sum(np.abs(pps[0].plot.values - pps[1].plot.values))/2.
print('DELTARHO = ',deltarho)
print('DELTARHO% = ',deltarho/intrho[0])

    




