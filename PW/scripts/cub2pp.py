#! /usr/bin/env python

import numpy as np
#import scipy as sp
#import string
import argparse


parser = argparse.ArgumentParser(description='Convert Gaussian cub into Quantum Espresso pp')
parser.add_argument('-i', '--input', help = 'Name of the input file containing the cub grid data', required = True)
parser.add_argument('-o', '--output', help = 'Name of the output file containing the pp grid data', required = True)
args = parser.parse_args()

z2lab = ['NA', 'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 
        'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 
        'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 
        'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 
        'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 
        'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 
        'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 
        'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 
        'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 
        'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 
        'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 
        'Rg', 'Cn', 'Uut', 'Fl', 'Uup', 'Lv', 'Uus', 'Uuo']

# READ THE CUB FILE (Z is the fastest changing index)

with open(args.input, 'r') as cub :
    title = cub.readline()
    line = cub.readline()
    line = cub.readline().split()
    nat = int(line[0])
    r0 = np.asarray(line[1:4], dtype=float)
    nr = np.zeros(3,dtype=int)
    dr = []
    for iline in range(3):
        line = cub.readline().split()
        nr[iline] = int(line[0])
        dr.append(np.asarray(line[1:4], dtype=float))
    nnr = nr[0]*nr[1]*nr[2]
    atom_z = np.zeros(nat,dtype=int)
    ityp = np.zeros(nat,dtype=int)
    tau = []
    for iat in range(nat):
        line = cub.readline().split()
        atom_z[iat] = int(line[0])
        tau.append(np.asarray(line[2:5], dtype=float))
    species = np.nonzero(np.bincount(atom_z))[0]
    ntyp = len(species)
    for iat in range(nat):
        ityp[iat] = np.where(species==atom_z[iat])[0][0]
    cubgrid = np.zeros(nnr,dtype=float)
    igrid = 0
    for line in cub:
        line = line.split()
        npts = len(line)
        cubgrid[igrid:igrid+npts] = np.asarray(line, dtype=float)
        igrid += npts

# CALCULATE CELL PARAMETERS
at = np.zeros((3,3),dtype=float)
for ipol in range(3):
    at[ipol,:] = dr[ipol]*nr[ipol]
alat = np.sqrt(np.dot(at[0,:],at[0,:]))
omega = np.dot( at[0,:], np.cross(at[1,:],at[2,:]) )
at = at/alat
tau = tau/alat

# WRITE THE PP FILE (X is the fastest changing index)
grid3d = np.reshape(cubgrid,nr,order='C')
ppgrid = np.reshape(grid3d,nnr,order='F')

def mywrite(fileobj, iterable, newline):
    if newline :
        fileobj.write('\n  ')
    if len(iterable) > 1 :
        for ele in iterable:
            fileobj.write(str(ele)+'  ')

with open('./conv.pp', 'w') as pp :
    val_per_line = 5
    pp.write(title)
    mywrite(pp,nr,False)
    mywrite(pp,nr,False)
    mywrite(pp,(nat,ntyp),False)
    mywrite(pp,(0,alat,0.0,0.0,0.0,0.0,0.0),True)
    for ilat in range(3):
        mywrite(pp,at[ilat,:],True)
    mywrite(pp,(omega,10.0,40.0,0),True)
    for it in range(ntyp):
        mywrite(pp,(it+1,z2lab[species[it]]),True)
    for iat in range(nat):
        mywrite(pp,(iat+1,tau[iat][0],tau[iat][1],tau[iat][2],ityp[iat]+1),True)
    nlines = nnr//val_per_line
    for iline in range(nlines):
        igrid = iline * val_per_line
        mywrite(pp,ppgrid[igrid:igrid+val_per_line],True)
    igrid = (iline+1) * val_per_line
    mywrite(pp,ppgrid[igrid:nnr],True)


