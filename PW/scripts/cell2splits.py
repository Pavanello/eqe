#!/usr/bin/env python3
"""
Most of functions are come from our eDFTpy project.
"""

import numpy as np
from ase.neighborlist import neighbor_list
from ase import io
from functools import reduce
import itertools

def union_mlist(arrs, keys = None, array = False):
    if keys is None :
        keys = set(itertools.chain.from_iterable(arrs))
    for key in keys:
        comp = [(i, item) for i, item in enumerate(arrs) if key in item]
        ind, comp = zip(*comp)
        ind = set(ind)
        if len(comp) > 0 :
            arrs = [arrs[i] for i, item in enumerate(arrs) if i not in ind]
            if len(comp) > 1 :
                if array :
                    arrs.append(reduce(np.union1d, comp))
                else :
                    arrs.append(set(itertools.chain.from_iterable(comp)))
            else :
                arrs.append(comp[0])
    return arrs

def from_distance_to_sub(atoms, cutoff = 3, max_nbins=1e6, **kwargs):
    nat = atoms.get_global_number_of_atoms()
    inda, indb, dists = neighbor_list('ijd', atoms, cutoff, self_interaction=True, max_nbins=max_nbins)
    subcells = []
    index = []
    for i in range(nat):
        index.append(i)
        firsts = np.where(inda == i)[0]
        neibors = indb[firsts]
        subcells.append(neibors)
    keys = np.arange(nat)
    subcells = union_mlist(subcells, keys = keys, array = True)
    return subcells

def write2qe(outf, atoms, basefile = None):
    if basefile is None :
        io.write(outf, atoms, format = 'espresso-in')
    else :
        ntyp = len(set(atoms.symbols))
        nat = atoms.get_global_number_of_atoms()
        with open(basefile, 'r') as fr :
            with open(outf, 'w') as fw :
                for line in fr :
                    if 'ntyp' in line :
                        pass
                        # To be compatible with older versions of eQE
                        # x = line.index("=") + 1
                        # line = line[:x] + ' ' + str(ntyp) + '\n'
                    elif 'nat' in line :
                        nat_old = int(line.split('=')[1])
                        x = line.index("=") + 1
                        line = line[:x] + ' ' + str(nat) + '\n'
                    elif 'cell_parameters' in line.lower() :
                        for i in range(3):
                            fr.readline()
                            line += '{0[0]:.14f} {0[1]:.14f} {0[2]:.14f}\n'.format(atoms.cell[i])
                    elif 'atomic_positions' in line.lower():
                        line = 'ATOMIC_POSITIONS angstrom\n'
                        for i in range(nat_old):
                            fr.readline()
                        for s, p in zip(atoms.symbols, atoms.positions):
                            line += '{0:4s} {1[0]:.14f} {1[1]:.14f} {1[2]:.14f}\n'.format(s, p)
                    fw.write(line)


if __name__ == "__main__":
    import argparse
    import os

    class ARGDICT(argparse.Action) :
        def __call__(self , parser, namespace, values, option_string = None):
            setattr(namespace, self.dest, {})

            for item in values.split(',') :
                key, value = item.split('=')
                key = key.strip()
                value = value.strip()
                getattr(namespace, self.dest)[key] = value

    parser = argparse.ArgumentParser(description='Split cell')
    parser = argparse.ArgumentParser(description='details',
        usage='use "%(prog)s --help" for more information',
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('cells', nargs = '*')
    parser.add_argument('-i', '--ini', '--input', dest='input', type=str, action='store',
            default='qe.in', help='The structure of large cell')
    parser.add_argument("-r", "--rcut", dest="rcut", type=float,
            default=2.0, help='The cutoff of two subsystems')
    parser.add_argument('-e', '--elements', '--radius', dest = "radius", action = ARGDICT,
            default={}, help='The radii of each element. e.g.\n    -e "C=1, O = 1"')
    parser.add_argument('-f', '--format', dest='format', type=str, action='store',
            default=None, help='The format of input and output')
    parser.add_argument('--format_in', dest='format_in', type=str, action='store',
            default=None, help='The format of input')
    parser.add_argument('--format_out', dest='format_out', type=str, action='store',
            default=None, help='The format of output')
    parser.add_argument('-b', '--basefile', dest='basefile', type=str, action='store',
            default=None, help='The additional formation of output.\n   If format_in and format_out are QE input file, input file alse can be basefile')

    args = parser.parse_args()
    if len(args.cells) == 0 :
        args.cells.append(args.input)

    if len(args.radius) > 0 :
        cutoff = {}
        keys = list(args.radius.keys())
        for i, k in enumerate(keys):
            for k1 in keys[i:] :
                cutoff[(k.capitalize(), k1.capitalize())] = float(args.radius[k]) + float(args.radius[k1])
    else :
        cutoff = args.rcut

    if args.format_in is None :
        args.format_in = args.format
    if args.format_out is None :
        args.format_out = args.format

    for fname in args.cells:
        prefix, ext = os.path.splitext(fname)
        if args.format_in is not None and args.format_in.lower() in ['eqe', 'qe', 'pwscf', 'espresso-in'] :
            format = 'espresso-in'
            if args.basefile is None :
                args.basefile = fname
        else :
            format = args.format_in
        atoms = io.read(fname, format=format)
        indices = from_distance_to_sub(atoms, cutoff)
        for i, index in enumerate(indices) :
            outf = prefix + '_' + str(i) + ext
            subcell = atoms[index]
            if args.format_out is not None and args.format_out.lower() in ['eqe', 'qe', 'pwscf', 'espresso-in'] :
                outf = prefix + '_' + str(i) + '.in'
                write2qe(outf, subcell, args.basefile)
            else :
                io.write(outf, subcell, format = args.format_out)
