#! /usr/bin/env python3

import QEPP

#import scipy as sp
#import string
import argparse


parser = argparse.ArgumentParser(description='Interpolate a QE PP file from a regular grid to another')
parser.add_argument('-i', '--input', help = 'Name of the input PP file.', required = True)
parser.add_argument('-o', '--output', help = 'Name of the output PP file.', required = True)
parser.add_argument('-g', '--grid', nargs=3, 
                    type=int,
                    help = 'Number of points in the new grid (three integers).', required = True)
args = parser.parse_args()

fileppin = args.input
fileppout = args.output

pp_in = QEPP.PP(fileppin)
pp_in.readpp()

newgrid = args.grid

pp_out = QEPP.PP(fileppout)

pp_out.title = pp_in.title
pp_out.plot = pp_in.plot.get_3dinterpolation(newgrid)
pp_out.cell = pp_out.plot.cell
pp_out.atoms = pp_in.atoms
pp_out.cutoffvars = pp_in.cutoffvars

pp_out.writepp()


