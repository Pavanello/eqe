import numpy as np
from scipy import ndimage

class PP(object):
    
    def __init__(self,filepp):
        self.filepp = filepp
        self.title = ''
        self.cutoffvars = {}
        #self.readpp()
        
        
    def readpp(self):
        
        with open(self.filepp) as filepp:
            # title
            self.title = filepp.readline()
            
            # nr1x, nr2x, nr3x, nr1, nr2, nr3, nat, ntyp
            nrx = np.zeros(3,dtype=int)
            nr = np.zeros(3,dtype=int)
            nrx[0], nrx[1], nrx[2] , nr[0], nr[1], nr[2], nat, ntyp = (int(x) for x in filepp.readline().split())
            
            # ibrav, celldm
            celldm = np.zeros(6,dtype=float)
            linesplt = filepp.readline().split()
            ibrav = int(linesplt[0])
            celldm = np.asarray(linesplt[1:],dtype=float)

            # at(:,i) three times
            at = np.zeros((3,3), dtype=float)
            if ibrav == 0 :
                for ilat in range(3):
                    linesplt = filepp.readline().split()
                    at[:,ilat] = np.asarray(linesplt,dtype=float)
                    #at[:,i] = (float(x) for x in filepp.readline().split())
            self.cell = Cell(ibrav, at, celldm, nrx, nr)
                    
            # gcutm, dual, ecut, plot_num 
            gcutm, dual, ecut, plot_num = (float(x) for x in filepp.readline().split())
            plot_num = int(plot_num)
            self.cutoffvars['gcutm'] = gcutm
            self.cutoffvars['dual'] = dual
            self.cutoffvars['ecut'] = ecut
            
            # ntyp
            atm = np.empty(ntyp,dtype=str)
            zv = np.empty(ntyp,dtype=float)
            for ity in range(ntyp):
                linesplt = filepp.readline().split()
                atm[ity] = linesplt[1]
                zv[ity] = float(linesplt[2])
            # tau
            tau = np.zeros((3,nat), dtype=float)
            ityp = np.zeros(nat, dtype=int)
            for iat in range(nat):
                linesplt = filepp.readline().split()
                tau[:,iat] = np.asarray(linesplt[1:4],dtype=float)
                ityp[iat] = int(linesplt[4]) -1
            
            self.atoms = Ions( nat, ntyp, atm, zv, tau, ityp, self.cell)

            
            # plot
            igrid = 0
            nnr = nrx[0]*nrx[1]*nrx[2]
            ppgrid = np.zeros(nnr,dtype=float)
            for line in filepp:
                line = line.split()
                npts = len(line)
                ppgrid[igrid:igrid+npts] = np.asarray(line, dtype=float)
                igrid += npts
            self.plot = Plot(self.cell, plot_num, grid_pp=ppgrid)
            
            
    def writepp(self):
        
        with open(self.filepp, 'w') as filepp:
            val_per_line = 5
            
            # title
            filepp.write(self.title)
            
            # nr1x, nr2x, nr3x, nr1, nr2, nr3, nat, ntyp
            mywrite(filepp, self.cell.nrx,False)
            mywrite(filepp, self.cell.nr,False)
            mywrite(filepp, [len(self.atoms.ions), len(self.atoms.species)],False)
            
            # ibrav, celldm
            mywrite(filepp, self.cell.ibrav,True)
            mywrite(filepp, self.cell.celldm,False)

            # at(:,i) three times
            if self.cell.ibrav == 0 :
                for ilat in range(3):
                    mywrite(filepp,self.cell.at[:,ilat],True)
                    
            # gcutm, dual, ecut, plot_num 
            mywrite(filepp, self.cutoffvars['gcutm'],True)
            mywrite(filepp, self.cutoffvars['dual'],False)
            mywrite(filepp, self.cutoffvars['ecut'],False)
            mywrite(filepp, self.plot.plot_num,False)

            # ntyp
            for ity, spc in enumerate(self.atoms.species):
                mywrite(filepp,[ity+1,spc[0],spc[1]],True)
                
            
            # tau
            for iat, ion in enumerate(self.atoms.ions):
                mywrite(filepp,iat+1,True)
                mywrite(filepp,ion.pos,False)
                mywrite(filepp,ion.typ+1,False)

            
            # plot
            nlines = self.cell.nnrx//val_per_line
            grid_pp = self.plot.get_values_1darray(order='F')
            for iline in range(nlines):
                igrid = iline * val_per_line
                mywrite(filepp,grid_pp[igrid:igrid+val_per_line],True)
            igrid = (iline+1) * val_per_line
            mywrite(filepp,grid_pp[igrid:self.cell.nnrx],True)

            
            
            
class Cell(object):
    
    def __init__(self, ibrav, at, celldm, nrx, nr, units='Bohr'):
        self.at = at
        self.ibrav = ibrav
        self.celldm = celldm
        self.nrx = nrx
        self.nr = nr
        self.nnrx = nrx[0]*nrx[1]*nrx[2]
        self.nnr = nr[0]*nr[1]*nr[2]

    
class Atom(object):
    z2lab = ['NA', 'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 
            'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 
            'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 
            'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 
            'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 
            'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 
            'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 
            'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 
            'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', 
            'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 
            'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 
            'Rg', 'Cn', 'Uut', 'Fl', 'Uup', 'Lv', 'Uus', 'Uuo']
    
    def __init__(self, Z=None, Zval=None, label=None, pos=None, typ=None):

        self.Z = Z
        self.Zval = Zval
        if Zval == None :
            self.Zval = Z
        if label == None :
            self.label = z2lab[self.Z]
        else :
            self.label = label
        
        self.pos = pos
        self.typ = typ
        

class Ions(object):
    
    def __init__(self, nat, ntyp, atm, zv, tau, ityp, cell):
        self.species = []
        self.ions = []
        for ity in range(ntyp):
            self.species.append([atm[ity],zv[ity]])
        
        for iat in range(nat):
            self.ions.append(Atom(Zval=zv[ityp[iat]],pos=tau[:,iat],typ=ityp[iat],label=atm[ityp[iat]]))
            
            
        
class Plot(object):
    
    spl_order = 3
    
    def __init__(self, cell, plot_num, grid_pp=None, grid_3d=None ):
        self.cell = cell
        self.plot_num = plot_num
        self.spl_coeffs = None
        if grid_pp is None and grid_3d is None :
            pass
        elif grid_pp is not None :
            self.values = np.reshape(grid_pp,cell.nrx,order='F')
        elif grid_3d is not None :
            self.values = grid_3d
    
    def calc_spline(self) :
        padded_values = np.pad(self.values,((self.spl_order,)),mode='wrap')
        self.spl_coeffs = ndimage.spline_filter(padded_values, order=self.spl_order)
        return

    
    def get_3dinterpolation(self, nr_new) :
        '''Interpolates the values of the plot on a cell with a different number of points, and returns a new plot object.'''
        if self.spl_coeffs is None :
            self.calc_spline()
        x = np.linspace(0,1,nr_new[0],endpoint=False)*self.cell.nr[0] + self.spl_order
        y = np.linspace(0,1,nr_new[1],endpoint=False)*self.cell.nr[1] + self.spl_order
        z = np.linspace(0,1,nr_new[2],endpoint=False)*self.cell.nr[2] + self.spl_order
        X, Y, Z = np.meshgrid(x, y, z, indexing='ij')
        new_values = ndimage.map_coordinates(self.spl_coeffs, [X, Y, Z],mode='wrap')
        new_cell = Cell(self.cell.ibrav, self.cell.at, self.cell.celldm, nr_new, nr_new)
        return Plot(new_cell, self.plot_num, grid_3d=new_values)

         
    def get_value_at_points(self, points) :
        '''points is in crystal coordinates'''
        if self.spl_coeffs is None :
            self.calc_spline()
        for ipol in range(3):
            # restrict crystal coordinates to [0,1)
            points[:,ipol] = (points[:,ipol]%1)*self.cell.nr[ipol] + self.spl_order
        values = ndimage.map_coordinates(self.spl_coeffs, [points[:,0],points[:,1],points[:,2]],mode='wrap')
        return values
    
    
    def get_values_1darray(self, order='F'):
        return np.reshape(self.values,self.cell.nnr,order=order)
        
    
    def get_plotcut(self, x0, r0, r1=None, r2=None, nr=10):
        '''general routine to get the arbitrary cuts of a Plot object in 1,2 or 3 dimensions. spline interpolation will be used.
            x0 = origin of the cut
            r0 = first vector (always required)
            r1 = second vector (required for 2D and 3D cuts)
            r2 = third vector (required for 3D cuts)
            nr[i] = number points to discretize each direction ; i = 0,1,2
            x0, r0, r1, r2 are all in crystal coordinates'''

        ndim =1

        x0 = np.asarray(x0)
        r0 = np.asarray(r0)
        if r1 is not None:
            r1 = np.asarray(r1)
            ndim += 1
            if r2 is not None:
                r1 = np.asarray(r1)
                ndim += 1
        nrx = np.ones(3,dtype=int)
        if isinstance(nr,(int,float)):
            nrx[0:ndim] = nr
        elif isinstance(nr,(np.ndarray,list,tuple)):
            nrx[0:ndim] = np.asarray(nr,dtype=int)

        dr = np.zeros((3,3),dtype=float)
        dr[0,:] = (r0-x0)/nrx[0]
        if ndim > 1 :
            dr[1,:] = (r1-x0)/nrx[1]
            if ndim == 3 :
                dr[2,:] = (r2-x0)/nrx[2]
        points = np.zeros((nrx[0],nrx[1],nrx[2],3))
        axis = []
        for ipol in range(3): 
            axis.append(np.zeros((nrx[ipol],3)))
            for ir in range(nrx[ipol]):
                axis[ipol][ir,:] = x0 + ir*dr[ipol]

        for i in range(nrx[0]):
            for j in range(nrx[1]):
                for k in range(nrx[2]):
                    points[i,j,k,:] = axis[0][i,:] + axis[1][j,:] + axis[2][k,:]

        a,b,c,d = points.shape
        points = points.reshape((a*b*c,3))

        values = self.get_value_at_points(points)
        
        if ndim ==1 :
            values = values.reshape((a))
        elif ndim == 2 :
            values = values.reshape((a,b))
        elif ndim == 3 :
            values = values.reshape((a,b,c))
            

        return values

    
    
def mywrite(fileobj, iterable, newline):
    if newline :
        fileobj.write('\n  ')
    #if len(iterable) > 1 :
    try:
        for ele in iterable:
            fileobj.write(str(ele)+'    ')
    except:
        fileobj.write(str(iterable)+'    ')