#! /usr/bin/env python

import numpy as np
import scipy as sp
import string
import argparse
import sys

parser = argparse.ArgumentParser(description='PP density file integrator')
parser.add_argument('-k', '--ks', help = 'Name of the file containing the total charge', required = True)
parser.add_argument('-f', '--fde', help = 'Path to the directory containing the isolated fragments', required = True)
parser.add_argument('-e', '--eff', help = '# of electrons actually involved in bonds', default = '0.0', required = False )

args = parser.parse_args()

InputA = open(args.ks, 'r')
EffElec = float(args.eff)
InputB = open(args.fde, 'r')

DELTA = 0.
N = 0.
N1 = 0.
nelec = 0.
atomtype = []

cnt = 0
initlines = 1000
additional = 100

while True:
   cnt = cnt + 1
   line_a = string.split(InputA.readline())
   
   if (cnt == 2):
      nat = int(line_a[-2])
      ntyp = int(line_a[-1])
   elif (cnt == 3) :
      ibrav = int(line_a[0])
      if (ibrav == 0):
         additional = 4
      elif (ibrav == 1):
         additional = 1
   
      initlines = 3 + additional + ntyp + nat

   if (cnt == 3 + additional):
      for j in range(ntyp):
         cnt = cnt + 1
         line_a = string.split(InputA.readline())
         atomtype.append(float(line_a[-1]))

      for j in range(nat):
         cnt = cnt + 1
         line_a = string.split(InputA.readline())
         nelec = nelec + atomtype[int(line_a[-1])-1]

   if (cnt >= initlines):
      break


cnt = 0
initlines = 100
while True:
   cnt = cnt + 1
   line_b = string.split(InputB.readline())
   if (cnt == 2):
      natB = int(line_b[-2])
      ntypB = int(line_b[-1])
   elif (cnt == 3) :
      ibrav = int(line_b[0])
      if (ibrav == 0):
         additional = 4
      elif (ibrav == 1):
         additional = 1

      initlines = 3 + additional + natB + ntypB

   if (cnt >= initlines):
      break


for line in InputA:
   line_a = string.split(line)
   line_b = string.split(InputB.readline())
#   print line_a
#   print line_b
#   sys.exit()
   for i in range(len(line_a)):
#            FDE_KS = FDE_KS + ( float(line_fde[i]) * float(line_ks[i]) ) 
#            KS_KS = KS_KS + ( float(line_ks[i]) * float(line_ks[i]) )
#            FDE_FDE = FDE_FDE + ( float(line_fde[i]) * float(line_fde[i]) )
      DELTA = DELTA + abs(float(line_a[i]) - float(line_b[i]) )
      N = N + float(line_a[i])
      N1 = N1 + float(line_b[i])
#            FDE_KS = FDE_KS + np.sqrt( abs(float(line_fde[i]) * float(line_ks[i])) ) 
#            KS_KS = KS_KS + abs( float(line_ks[i]))
#            FDE_FDE = FDE_FDE + abs( float(line_fde[i]))

if EffElec == 0.0 :
   EffElec = nelec

print 'EffElec    = ',EffElec
print 'Nelec      = ',nelec
print 'N          = ',N
print 'N1         = ',N1
print 'DELTA      = ', 0.5*nelec*DELTA/N

