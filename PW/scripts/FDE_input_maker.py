#! /usr/bin/env python

import os
import sys
import argparse
import re

class Inputs(object):
  def __init__(self, n, fileprefix):
    self.n = n
    filename = fileprefix+'_'+str(n)+'.in'
    self.Input = open(filename, 'w')

  #def write(self, line):
    #self.Input.write(line)
  
  def close(self):
    self.Input.close()

def GlobalWrite(string, n=-1):
  if n == -1:
    for item in InputList:
      item.Input.write(string)
  else:
    InputList[n].Input.write(string)
  return

# Check the arguments given while launching the program and set filenames and other variables.

parser = argparse.ArgumentParser(description='Input files creator for PW FDE')
parser.add_argument('-i', '--input', help = 'Name of the file containing the global input file', required = True)
args = parser.parse_args()

prefix, trash = os.path.splitext(os.path.basename(args.input))

MainInput = open(args.input, 'r')

line = MainInput.readline()
nfragments = int(re.findall("FRAGMENTS = (\d+)", line)[0])

#nfragments = 2

#InputList = []
#for i in range(nfragments):
#  InputList.append(Inputs(i,prefix))

for ifrag in range(nfragments):
   ncount = -1
   MainInput.seek(0)
   line = MainInput.readline()
#   f.close()
#   f = open(prefix+'_'+str(ifrag)+'.in', 'w')
#   if True :
   with open(prefix+'_'+str(ifrag)+'.in', 'w') as f:
      for line in MainInput:

         if ncount == ifrag :
            if line.strip() != 'FRAGMENT':
               f.write(line)

         if line.strip() == 'FRAGMENT':
            ncount += 1
            if ncount > ifrag :
               break

         if ncount == -1 :
            f.write(line)
         
         
#  if line.strip() == '&system':
#    GlobalWrite(line, ncount)
    #GlobalWrite('    nfragments = '+str(nfragments)+'\n', ncount)
#  elif line.strip() == 'FRAGMENT':
#    ncount += 1
#  else:
#    GlobalWrite(line, ncount)
