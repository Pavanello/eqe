#!/bin/bash
#export TESTCODE_NPROCS=3
#export PARA_PREFIX="mpirun -np ${TESTCODE_NPROCS}"

#export OMPI_MCA_btl_vader_single_copy_mechanism=none

mpirun  --allow-run-as-root -np 3 ../../bin/fdepw.x -ni 3 -in trimer.scf  
echo "Well done"
res="$( grep 'FDE: total energy' trimer.scf_0.out | awk '{printf "%.10f", $5}' )"
echo "${res}"
 

