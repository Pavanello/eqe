<a id="keys"></a>
## Keyword List
<b>&system</b>

[fde_kin_funct](#fde_kin_funct) | [fde_xc_funct](#fde_xc_funct) | [fde_nspin](#fde_nspin) | [fde_print_density](#fde_print_density) | [fde_print_density_frag](#fde_print_density_frag) | [fde_print_embedpot](#fde_print_embedpot) | [fde_split_mix](#fde_split_mix) | [fde_cell_split](#fde_cell_split) | [fde_overlap](#fde_overlap) | [fde_overlap_c](#fde_overlap_c) | [fde_fractional](#fde_fractional) | [fde_nspline](#fde_nspline) | [fde_tvpbea](#fde_tvpbea) | [fde_rhomax](#fde_RhoMax) | [fde_mgga_kin](#fde_mgga_kin) | [input_dft](#input_dft)

<b>&electrons</b>

[use_gaussians](#use_gaussians) | [fde_frag_charge](#fde_frag_charge)

<a id="fde_kin_funct"></a>

### fde_kin_funct

<b>Namelist: </b>&system

<b>Type: </b>character

<b>Default: </b>'LC94'


The kinetic energy density functional to be used to evaluate the non-additive kinetic energy and potential.

Selected GGA functionals are implemented:

    - 'TF' : Thomas-Fermi LDA
    - 'VW' : Von Weizsacker (unstable)
    - 'DGE2'
    - 'LLP'
    - 'PW86'
    - 'LC94' : Default
    - 'APBEK'
    - 'revAPBEK' : Recommended
    
<b>Example: </b>

	 eQE_input_template/beginner/

Selected Nonlocal functionals are implemented: 

    - 'LMGPA' : Recommended 
    - 'LMGPG'
    - 'LMGP0'
    - 'LMGP' 
    - 'LWT'  
<b> NOTE : </b> An additional kernel table file named `Kernel_'fde_kin_funct'.TABLE` (e.g. `Kernel_LMGPA.TABLE` for LMGPA functional) is needed when using Nonlocal non-additive kinetic energy functional. All the kernel table files are in `PW/Kernel/`.

Keywords ONLY related to nonlocal kinetic energy functionals are:

[fde_nspline](#fde_nspline) | [fde_tvpbea](#fde_tvpbea) | [fde_rhomax](#fde_RhoMax)

<b>Example: </b>

	 eQE_input_template/nonlocal_NAKE/

[^ Back to top](#keys)
<a id="fde_xc_funct"></a>

### fde_xc_funct

<b>Namelist: </b>&system

<b>Type: </b>character

<b>Default: </b>'SAME'

The xc density functional to be used to evaluate the non-additive xc energy and potential.

By default the same functional is used within and across the fragments.

<b>Example: </b> 

	eQE_input_template/beginner/

[^ Back to top](#keys)
<a id="fde_nspin"></a>

### fde_nspin

<b>Namelist: </b>&system

<b>Type: </b>integer

<b>Default: </b>1

Determines wheter the supersystem density will be spin polarized:
    
	1 : Spin unpolarized
    2 : Spin polarized

If the nspin variable of at least one of fragment is equal to 2, then fde_nspin NEEDS to be eqaual to 2 for all the fragments.

This allows to have one or more fragments treated openshell while the others are close shell.

[^ Back to top](#keys)
<a id="fde_print_density"></a>

### fde_print_density

<b>Namelist: </b>&system

<b>Type: </b>logical

<b>Default: </b>.false.

Print the total density to a .pp file at the end of the calculation.

[^ Back to top](#keys)
<a id="fde_print_density_frag"></a>

### fde_print_density_frag

<b>Namelist: </b>&system

<b>Type: </b>logical

<b>Default: </b>.false.

Print the fragment density to a .pp file at the end of the calculation.

[^ Back to top](#keys)
<a id="fde_print_embedpot"></a>

### fde_print_embedpot

<b>Namelist: </b>&system

<b>Type: </b>logical

<b>Default: </b>.false.

Explicitly calculate the embedding potential of the fragment and print it to a .pp file at the end of the calculation.

Make sure this keyword is `.true.` only for a single fragment, or eQE will arbitrarely print it only for the fragment with the lowest index.

The embedding potential is represented into the smaller electronic cell.

[^ Back to top](#keys)
<a id="fde_split_mix"></a>

### fde_split_mix

<b>Namelist: </b>&system

<b>Type: </b>logical

<b>Default: </b>.true.

Dynamically adjust the mixing_beta parameter of the fragment along the scf cycle.

Helps speeding up convergence and stability.

[^ Back to top](#keys)
<a id="fde_cell_split"></a>

### fde_cell_split(i), i=1,3

<b>Namelist: </b>&system

<b>Type: </b>integer

<b>Default: </b>1.d0

Flag to define the dimension of each fragment electronic cell.

By default the electronic cell and the supersystem cell are the same, but a huge increase in efficiency can be achieved if the fragment is small enough compared to the total system so that its electronic structure can be confined within a cell which is a fraction of the supersystem cell using this variable.

Each direction can be scaled independently.

Although any value between 0. and 1. can be set, the code will round it to be one of the following allowed fractions:

    1/5
    1/4
    1/3
    2/5
    1/2
    3/5
    2/3
    3/4
    4/5
    1/1

And it might be further changed so that the grid points match across all the subsystems.

<b>Example: </b> 

	eQE_input_template/thiophene-dimer/


[^ Back to top](#keys)
<a id="fde_overlap"></a>

### fde_overlap

<b>Namelist: </b>&system

<b>Type: </b>logical

<b>Default: </b>.false.

Adds a potential proportional to the electron density of the surrounding fragments, $v\_\text{overlap}^I(\mathbf{r}) = C \cdot \sum\_{J \neq I} \rho\_J(\mathbf{r})$.

It can be useful to confine (localize) the electronic structure of subsystems. In the examples, the excess electron in a thiophene radical anion is localized onto the thiophene.

See also fde_overlap_c. Nota Bene: This is a <i>last resort</i> solution. Consider using `use_gaussians` first.

<b>Example: </b>

	 eQE_input_template/fde_overlap/

[^ Back to top](#keys)
<a id="fde_overlap_c"></a>

### fde_overlap_c

<b>Namelist: </b>&system

<b>Type: </b>real

<b>Default: </b>0.0e0

The proportionality constant, $C$, to be applied to the density of surrounding fragments for fde_overlap.

Mind of the following specifications: 

- 0.000 < `fde_overlap_c` < 10000: Only the core density of surrounding subsystems is used.
- 10000 < `fde_overlap_c` < 20000: The total density of surrounding subsystems is used with $C = ($`fde_overlap_c` $- 10000)$

<b>Example: </b> 

	eQE_input_template/fde_overlap/

[^ Back to top](#keys)
<a id="fde_fractional"></a>

### fde_fractional

<b>Namelist: </b>&system

<b>Type: </b>logical

<b>Default: </b>.false.

WARNING: fde_fractional is relatively untested. Use with a grain of salt and at your own risk!

fde_fractional Allows the subsystem electron occupations to be determined self-consistently. Subsystems will exchange electrons through the alpha channel (`fde_fractional_onlyalpha`=.true.), or the beta channel (`fde_fractional_onlybeta`=.true.), or both (setting both alpha and beta channels to true).

The transfer rate of electrons in/out of a subsystem is set by `fde_fractional_mixing` (\(0 < \) `fde_fractional_mixing` \( < 1 \)) and the total amount of trasferred electrons is capped by `fde_fractional_maxEtransfer` (typically set to 0.5 electrons).

Transfer goes in the direction of Fermi energy equalization for all subsystems. Set `fde_fractional_cycle`=N to start electron transfer at the N-th SCF cycle.

[^ Back to top](#keys)
<a id="fde_nspline"></a>

### fde_nspline

<b>Namelist: </b>&system

<b>Type: </b>integer

<b>Default: </b>80

The number of $\rho_0$ to be used to spline the kernel and potential of nonlocal non-additive kinetic energy functional.

<b>Example: </b>

	eQE_input_template/beginner/nonlocal_NAKE/

[^ Back to top](#keys)
<a id="fde_tvpbea"></a>

### fde_tvpbea

<b>Namelist: </b>&system

<b>Type: </b>real

<b>Default: </b>0.01

The parameter to be used in evaluation of the TF+vW functional with PBE-like formalism. It is  only used in Nonlocal NAKEs (LMGP(X) and LWT).

The benchmarks of fde_tvpbea=0.01 and 0.1 for LMGP(X) and LWT functionals can be found in the paper: [JPCL 11 (1), 272–279 (2020).](https://doi.org/10.1021/acs.jpclett.9b03281)

<b>Example: </b>

	eQE_input_template/nonlocal_NAKE/

[^ Back to top](#keys)
<a id="fde_rhomax"></a>

### fde_rhomax

<b>Namelist: </b>&system

<b>Type: </b>real

<b>Default: </b> Evaluated from the pseudo-atom density of pseudopotentials

The maximum value of electron density, $\rho\_\text{max}$ for the considered system. There is no needed to be exact number (can be little larger than the real $\rho_{max}$). It is only used in Nonlocal NAKEs

DO NOT USE this keyword unless you know what you are doing

<b>Example: </b> 

	eQE_input_template/nonlocal_NAKE/

[^ Back to top](#keys)


### fde_mgga_kin

<b>Namelist: </b>&system

<b>Type: </b>character

<b>Default: </b>'TFL'

The kinetic energy density functional to be used to evaluate the kinetic energy density in deorbitalized meta-GGA xc functional.

Selected functionals are implemented:

    - 'TFL' : Default
    - 'TFLopt' 
    - 'GEA2L'
    - 'PC'
    - 'PCopt' 
    - 'L04' : Recommended

<b>Example: </b>

	 eQE_input_template/mGGA-L_x/

[^ Back to top](#keys)
<a id="use_gaussians"></a>

### use_gaussians

<b>Namelist: </b>&electrons

<b>Type: </b>logical

<b>Default: </b>.false.

If .true., it replaces the core densities of the surrounding fragments to a gaussian. This is to avoid problems of electrons leaking in the core region of surrounding fragments when <i>hard pseudopotentials</i> (such as hgh NC-PPs) are employed.

Use use_gaussians only if you encounter convergence problems.

<b>Example: </b>

	 eQE_input_template/use_gaussians/

[^ Back to top](#keys)
<a id="fde_frag_charge"></a>

### fde_frag_charge

<b>Namelist: </b>&electrons

<b>Type: </b>real

<b>Default: </b>0.d0

Change the number of electrons assigned to a fragment. Default = 0.0 neutral fragment.

The sum of the charges across all fragments needs to be 0.0

[^ Back to top](#keys)

### input_dft

<b>Namelist: </b>&system

<b>Type: </b>character

<b>Default: </b>'read from pseudopotential files'

Exchange-correlation functional: eg 'PBE', 'revPBE' etc

See Modules/funct.f90 for allowed values.

Selected deorbitalized meta-GGA xc functionals are implemented:

    - 'SCAL' : Deorbitalized meta-GGA SCAN functional (SCAN-L) 
    - 'r2SL' : Deorbitalized meta-GGA r2SCAN functional (To be released). 

<b>Example: </b>

	eQE_input_template/mGGA-L_x/

Selected nonlocal xc functionals are implemented:

    - 'rVV10'
<b> NOTE : </b> The kernel table (`rVV10_kernel_table`) is the additional inputfile when using rVV10 xc functional and it is in `PW/Kernel/`.

<b>Example: </b>

	eQE_input_template/nonlocal_xc/

[^ Back to top](#keys)

