<a id="deprecated"></a>
## What `eQE` cannot do that QE can

The following simulations/keywords cannot be used in `eQE`. This is <b>not</b> a comprehensive list.

 - hybrid functionals
 - stress and cell optimizations 
 - andersen thermostat (only velocity rescaling available) 

[^ Back to top](#keys)



