<a id="top"></a>

<center> 
<img src="../logo.png" width="35%">  
</center>

# eQE 2021 User's Guide<br>(eQE v2.0)

## Table of Content

* [Introduction](#introduction)
  * [People](#people)
  * [Contact](#contact)
  * [License and Citation](#terms)
* [Setup](#setup)
  * [Download](#download)
  * [Compile](#compile)
* [Running](#run)
* [Embedding Potential](#embedpot)
  * [Atom centered grid](#atomgrid)
* [Keyword List](#keys)
* [What eQE cannot do that QE can](#deprecated)

<a id="introduction"></a>

## Introduction
This is the manual for embedded Quantum ESPRESSO v2.0 (eQE 2017-2021). eQE is a generalization of [Quantum ESPRESSO](http://www.quantum-espresso.org) tailored to perform large-scale subsystem DFT simulations. 

This distribution contains the core packages for `PWscf` (Plane-Wave Self-Consistent Field). These allow for the calculation of electronic structure properties employing Subsystem Density-Functional Theory (DFT), using a Plane Wave (PW) basis set and pseudopotentials.

All trademarks mentioned in this manual belong to their respective owners.

<a id="people"></a>

### People
The `eQE` distribution is promoted, developed and maintained by the Pavanello Research group (PRG) at the Chemistry Department of Rutgers - Newark, NJ and collaborators.

Contributors to `eQE`, include (Alphabetize by Last Name):

- Oliviero Andreussi
- Davide Ceresoli
- Robert DiStasio
- Alessandro Genova 
- Alisa Krishtal
- Wenhui Mi 
- Michele Pavanello (PI)
- Xuecheng Shao
- All the `QE` contributors for providing the Quantum ESPRESSO codebase

<a id="contact"></a>

### Contact
The homepage of `eQE` is [eqe.rutgers.edu](http://eqe.rutgers.edu).

Bleeding edge source code can be downloaded from the GIT repository at [gitlab.com/Pavanello/eqe](https://gitlab.com/Pavanello/eqe) (registration required).

`eQE` developers are always happy to answer questions about the code and to help setting up and running `eQE` simulations. Please, contact us at:

- [wenhui.mi@rutgers.edu](mailto:wenhui.mi@rutgers.edu)
- [xuecheng.shao@rutgers.edu](mailto:xuecheng.shao@rutgers.edu)
- [m.pavanello@rutgers.edu](mailto:m.pavanello@rutgers.edu).
- [a.genova@kitware.com](mailto:a.genova@kitware.com)
- [dceresoli@gmail.com](mailto:dceresoli@gmail.com).

<a id="terms"></a>

### Term of Use
`eQE` is free software, released under the [GNU General Public License](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt).
    
Acknowledgement of our work is appreciated when scientific work is carried out using `eQE`. Please, cite the following reference:
> W. Mi, X. Shao, A. Genova, D. Ceresoli, M. Pavanello, "eQE version 2: Subsystem DFT beyond GGA functionals", To be published (2021).

> A. Genova, D. Ceresoli, A. Krishtal, O. Andreussi, R. A. DiStasio, M. Pavanello, <i>Int. J. Quantum Chem.</i> <b>117</b>, e25401 (2017) [10.1002/qua.25401](https://doi.org/10.1002/qua.25401)

<a id="setup"></a>

## Setup

<a id="download"></a>

### Download
Presently, `eQE` is distributed in source form.

A snapshot of the code can be found at [eqe.rutgers.edu](http://eqe.rutgers.edu).

Uncompress and unpack the base distribution using the command:

```bash
tar -zxvf eqe-XYZ.tar.gz
```

where `XYZ` stands for the version number.

The directory `eqe-XYZ` will be created. 

Alternatively, for the ones who dare access the bleeding edge of the code, it can be found on our GIT repository. Request access to m.pavanello@rutgers.edu, and then clone the repo:

```bash
git clone git@gitlab.com:Pavanello/eqe.git eqe
```

<a id="compile"></a>

### Compilation
Compilation is identical to the regular Quantum ESPRESSO distribution, refer to their user manual for the fine tuning needed to obtain an efficient executable for your machine.

For the impatient:

```bash
cd eqe-XYZ
./configure
make pw
```

<b>NOTA BENE:</b> this will more often than not produce a slow executable. Make sure that high performance compilers and libraries are properly selected by manually modifying `make.sys` after running `configure`.

`eQE` NEEDS a parallel environment to properly run and communicate information across the subsystems. Make sure all the needed parallel compilers are present in your machine.

<a id="run"></a>

## Running `eQE`
You will need a separate input file for each subsystem (fragment that may be an atom, a molecule, or a surface) composing your system. 

The input files need to share the same prefix and be numbered starting from 0.

Each input file is essentially a regular `QE` input file with only the atoms belonging to that specific fragment.

`eQE` introduces a series of new keywords, whose complete list can be found at the end of this manual.

In this example we will simulate a water trimer, so we have the following input files:

```
trimer.scf_0.in
trimer.scf_1.in
trimer.scf_2.in
```

Several example input files for `eQE` can be found in the `eQE_input_template` folder included with the distribution.

Specifically, the water trimer input files are in `eQE_input_template/beginner`.

Although most of the original `QE` keywords can be defined independently in each of the subsystem input files, some need to be consistent and explicitly specified for all the subsystems. These keywords are:

- `prefix` (the name of files in the scratch folders)
- The supersystem simulation cell (i.e., `ibrav` and `CELL_PARAMETERS`).
- `ecutwfc` and `ecutrho` (the cutoff for wavefunction and charge density)
- `conv_thr` (this sets the SCF convergence)

The following keywords can differ among the subsystems:

- `occupations` (whether to use smearing or not)
- `degauss` (smearing parameter)
- `nspin` (1=spin restricted, 2=open-shell)
- the `K_POINT` card (subsystems can have different intrinsic periodicity)
- `fde_cell_split(i)` (defines the electronic simulation cell of each fragment in the direction of the $i$-th lattice vector).

To run an `eQE` simulation, the following command is needed (please, adapt it based on your needs and available resources):

```bash
 mpirun -np 6 fdepw.x -ni 3 -in trimer.scf
```

where `-np` defines the total number of processors used, `-ni` defines the number of fragments in your system, and `-in` defines the prefix of the input and output files (i.e. the filename of the input files up to and excluding `_N.in`)

The calculation will run on 6 processors, assigning two processors to each of the fragment.

By default the code will try to split the number of processors evenly. But it may be the case where the size of the fragments is heterogenous, and an even split of the available processors would be inefficient.

`eQE` allows the user to assign an arbitrary number of processors to each fragment by creating an additional file named `fragments_procs.in`. This file contains as many lines as subsystems, each line containing the number of processors to assign to each subsystem. For example, if `fragments_procs.in` contains:

```
2
4
2
```

it will assign 2 processors to the first and third fragment, and 4 processors to the second. The `-np` parameter will have to be set accordingly.

<a id="embedpot"></a>

## Generating and Exporting the embedding potential
In order for `eQE` to compute the embedding potential of a single fragment explicitly, the keyword

```
fde_print_embedpot = .true. 
```

must be included in the `&system` namelist of the fragment input file.

<b>NOTA BENE:</b> Because of parallelization complexity, currently the embedding potential can be printed only for a single fragment.

If the user specifies the keyword for more than one fragment, the code will only print the embedding potential of the fragment with the lowest id.

The embedding potential will be represented in the <i>small electronic cell</i> of the fragment and stored in a `.pp` file at the end of the calculation.

Afterwards, it is possible to export the embedding potential to several other formats using the standard `pp.x` program provided with the `eQE` distribution.

<a id="atomgrid"></a>

### Atom centered grid
The embedding potential can be interpolated on an arbitrarily defined grid. This can be useful when codes based on atom-centered basis functions are employed. We have noticed that integrals of the kind 

$$
v_\mathrm{emb}^{\mu\nu}=\langle \chi_\mu | v_\mathrm{emb} | \chi_\nu \rangle,
$$

will not be evaluated correctly when $v_\mathrm{emb}$ is expressed on a regular, evenly-spaced grid. Atom-centered grid generated from a molecular code (such as the Becke grid) provide the correct real-space representation in this case.

To achieve this, the `pp.x` program provided with the `eQE` distribution needs to be used. Be sure to include a file containing the arbitrary grid named `grid.dat` in the same directory as the embedding potential `.pp` file.

The format of `grid.txt` should be as follows:

```
  npoints
x1   y1   z1   w1
x2   y2   z2   w2
...
xn   yn   zn   wn
```

where 

- `npoints` is the total number of points in the custom grid;
- `x,y,z` are the cartesian coordinates in Bohr of the gridpoints;
- `w` is the weight or volume element associated to the grid point.

Create a `pp.x` input file based on this template and name it `interpolatepp.in`:

```
&inputpp
/
&plot 
   nfile = 1
   filepp(1) = 'filename_embedpot_0_alpha.pp'
   iflag = 3                           ! 3D plot
   output_format = 6                   ! cube file
   interpolation = 'bspline_custom'    ! interpolate on the points specified in 'grid.dat'.
   fileout = 'filename_embedpot_0_alpha.customgrid'           ! interpolated potential output file
/
```

Finally, execute:

```bash
  /path/to/eQE/bin/pp.x < interpolatepp.in 
```

This will generate `filename_embedpot_0_alpha.customgrid` in Gaussian cube file format containing the embedding potential splined onto the custom grid.




