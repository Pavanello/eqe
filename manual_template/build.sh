#! /bin/bash

# easy
if [ -x "$(command -v markdown)" ]; then
	echo 'use markdown'
	markdown manual.md > content_man.html
	markdown keywords.md > content_key.html
	markdown postkey.md > content_postkey.html

	echo "" > manual.html
	cat preamble.html >> manual.html
	cat content_man.html >> manual.html
	cat content_key.html >> manual.html
	cat content_postkey.html >> manual.html
	cat postamble.html >> manual.html
	rm content_man.html content_key.html content_postkey.html
fi

# final
if [ -x "$(command -v i5ting_toc)" ]; then
	echo 'use i5ting_toc'
	cat manual.md keywords.md postkey.md > tmp.md 
	i5ting_toc -f tmp.md 
	cp ./preview/tmp.html tmp.html
	sed -i '0,/logo.png/d' tmp.html                         
	cat preamble.html tmp.html > manual.html
	sed -i 's/<ul>/<ul class="browser-default">/g' manual.html 
	sed -i '/jquery-1.4.4/,$d' manual.html
	rm -r tmp.md tmp.html preview
fi
