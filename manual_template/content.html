<p><a id="top"></a></p>



<h1 id="embedded-quantum-espresso-users-guide-eqe-v020">embedded Quantum Espresso User’s Guide (eQE v0.2.0)</h1>



<h2 id="table-of-content">Table of Content</h2>

<ul>
<li><a href="#introduction">Introduction</a> <br>
<ul><li><a href="#people">People</a></li>
<li><a href="#contact">Contact</a></li>
<li><a href="#terms">License and Citation</a></li></ul></li>
<li><a href="#setup">Setup</a> <br>
<ul><li><a href="#download">Download</a></li>
<li><a href="#compile">Compile</a></li></ul></li>
<li><a href="#run">Running</a></li>
<li><a href="#embedpot">Embedding Potential</a> <br>
<ul><li><a href="#atomgrid">Atom centered grid</a></li></ul></li>
<li><a href="#keywords">Keyword List</a></li>
</ul>

<p><a id="introduction"></a></p>



<h2 id="introduction">Introduction</h2>

<p>This guide gives a general overview of the contents and of the installation of the <b>Subsystem DFT</b> (<b>FDE</b>) flavor of Quantum ESPRESSO, eQE version 0.2.0.</p>

<p>This distribution contains the core packages <code>PWscf</code> (Plane-Wave Self-Consistent Field) for the calculation of electronic-structure properties within Subsystem Density-Functional Theory (DFT), using a Plane-Wave (PW) basis set and pseudopotentials.</p>

<p>All trademarks mentioned in this guide belong to their respective owners.</p>

\( f(x) = 2x^2 \)
<p>another something</p>

<p><a id="people"></a></p>



<h3 id="people">People</h3>

<p>The maintenance and further development of the <code>eQE</code> and <code>eQE-tddft</code> distribution is promoted by the Pavanello Research group (PRG) at the Chemistry Department of Rutgers - Newark, NJ.</p>

<p>Contributors to <code>eQE</code>, include:</p>

<ul>
<li>Alessandro Genova</li>
<li>Davide Ceresoli</li>
<li>Alisa Krishtal</li>
<li>Sudheer Kumar</li>
<li>Michele Pavanello</li>
<li>Robert DiStasio</li>
<li>Oliviero Andreussi</li>
<li>All the <code>QE</code> contributors for providing a base</li>
</ul>

<p><a id="contact"></a></p>



<h3 id="contact">Contact</h3>

<p>The homepage of <code>eQE</code> and <code>eQE-tddft</code> is at <a href="http://eqe.rutgers.edu">eqe.rutgers.edu</a>.</p>

<p>Bleeding edge source code can be downloaded from the GIT repository at <a href="https://gitlab.com/Pavanello/eqe">gitlab.com/Pavanello/eqe</a> (registration required).</p>

<p>You can contact the developers for questions about the code, proposals, offers to help at <a href="mailto:ales.genova@rutgers.edu">ales.genova@rutgers.edu</a> and <a href="mailto:m.pavanello@rutgers.edu">m.pavanello@rutgers.edu</a>.</p>

<p><a id="terms"></a></p>



<h3 id="term-of-use">Term of Use</h3>

<p><code>eQE</code> is free software, released under the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt">GNU General Public License</a>.</p>

<p>We shall greatly appreciate if scientific work done using <code>eQE</code> distribution will contain an explicit acknowledgment and the following references:</p>

<blockquote>
  <p>A. Genova, D. Ceresoli, A. Krishtal, O. Andreussi, R. A. DiStasio, M. Pavanello, <i>Int. J. Quantum Chem.</i> 117, e25401 (2017) <a href="https://doi.org/10.1002/qua.25401">10.1002/qua.25401</a></p>
  
  <p>A. Krishtal, D. Ceresoli, M. Pavanello, <i>J. Chem. Phys.</i>, 142, 154116 (2015) <a href="https://doi.org/10.1063/1.4918276">10.1063/1.4918276</a></p>
</blockquote>

<p><a id="setup"></a></p>



<h2 id="setup">Setup</h2>

<p><a id="download"></a></p>



<h3 id="download">Download</h3>

<p>Presently, <code>eQE</code> is distributed in source form.</p>

<p>A snapshot of the code can be found at <a href="http://eqe.rutgers.edu">eqe.rutgers.edu</a>.</p>

<p>Uncompress and unpack the base distribution using the command:</p>



<pre class="prettyprint"><code class="language-bash hljs ">tar -zxvf eqe-X.Y.Z.tar.gz</code></pre>

<p>where <code>X.Y.Z</code> stands for the version number.</p>

<p>The directory <code>eqe-X.Y.Z</code> will be created. </p>

<p>Alternatively, for the adventurers who want access to the bleeding edge of the code, it can be found on our GIT repository.</p>



<pre class="prettyprint"><code class="language-bash hljs ">git clone git@gitlab.com:Pavanello/eqe.git eqe</code></pre>

<p>Before cloning the repository is necessary to ask the developers to be added to the project.</p>

<p><a id="compile"></a></p>



<h3 id="compilation">Compilation</h3>

<p>Compilation is identical to the regular quantum espresso distribution, refer to their user manual for the fine tuning needed to obtain an efficient executable for your machine.</p>

<p>For the impatient:</p>



<pre class="prettyprint"><code class="language-bash hljs "><span class="hljs-built_in">cd</span> eqe-X.Y.Z
./configure
make pw</code></pre>

<p><b>NOTA BENE:</b> this will more often than not produce a slow executable. Make sure that high performance compilers and libraries are properly selected by manually modifying <code>make.sys</code> after running <code>configure</code>.</p>

<p><code>eQE</code> NEEDS a parallel environment to properly run and communicate information across the subsystems. Make sure all the needed parallel compilers are present in your machine.</p>

<p><a id="run"></a></p>



<h2 id="running">Running</h2>

<p>You will need a separate input file for each fragment composing your system. </p>

<p>Each input file is essentially a regular <code>QE</code> input file with only the atoms belonging to that specific fragment.</p>

<p><b>NOTA BENE:</b> Pseudopotentials and the <code>ntyp</code> keyword has to be the same across all the fragments. i.e. all the fragments need to be aware of all the atom types in the entire system.</p>

<p><code>eQE</code> introduces a series of new keywords, whose complete list can be found at the end of this manual.</p>

<p>The input files need to share the same prefix and be numbered starting from 0.</p>

<p>In this example we will simulate a water trimer, so we have the following input files:</p>



<pre class="prettyprint"><code class=" hljs avrasm">trimer<span class="hljs-preprocessor">.scf</span>_0<span class="hljs-preprocessor">.in</span>
trimer<span class="hljs-preprocessor">.scf</span>_1<span class="hljs-preprocessor">.in</span>
trimer<span class="hljs-preprocessor">.scf</span>_2<span class="hljs-preprocessor">.in</span></code></pre>

<p>Various example input files for <code>eQE</code> can be found in the <code>eQE_input_template</code> folder included with the distribution.</p>

<p>Specifically, the water trimer input files are in <code>eQE_input_template/beginner</code>.</p>

<p>Although most of the original <code>QE</code> variables can be defined independently in each of the fragments input files, some of them need to be consistent across them.</p>

<p>Most notably the following keywords <b>have to be the same</b>: <br>
- <code>prefix</code> <br>
- The supersystem simulation cell. <br>
- <code>ecutwfc</code> and <code>ecutrho</code> <br>
- <code>ntyp</code> and the <code>ATOMIC_SPECIES</code> card (You’ll have to include here all the elements in the supersystem, even if they are not present in the specific fragment) <br>
- <code>conv_thr</code> and <code>mixing_beta</code></p>

<p>The following keywords can be safely chosen to be different: <br>
- <code>occupations</code> and <code>degauss</code> <br>
- <code>nspin</code> <br>
- the <code>K_POINT</code> card <br>
- <code>fde_cell_split(i)</code> to independently define the electronic simulation cell of each fragment in the <script type="math/tex" id="MathJax-Element-1">i</script> direction.</p>

<p>To run the calculation we need to run the following command (adapt it based on your needs and available resources):</p>



<pre class="prettyprint"><code class="language-bash hljs "> mpirun -np <span class="hljs-number">6</span> fdepw.x -ni <span class="hljs-number">3</span> -in trimer.scf</code></pre>

<p>where <code>-np</code> defines the total number of processors used, <code>-ni</code> defines the number of fragments in your system, and <code>-in</code> defines the prefix of the input and output files (i.e. the filename of the input files up to and excluding <code>_N.in</code>)</p>

<p>The calculation will run on 6 processors, assigning two processors to each of the fragment.</p>

<p>By default the code will try to split the number of processors evenly. But it may be the case where the size of the fragments is eterogenous, and an even split of the processors would be highly inefficient.</p>

<p>It is possible to assign an arbitrary number of processors to each fragment, by creating an additional input file named <code>fragments_procs.in</code>.</p>

<p>In this file you just need to write the number of processors you want assigned to each fragment, one per line. <br>
For example, if <code>fragments_procs.in</code> contains:</p>



<pre class="prettyprint"><code class=" hljs "> 2
 4
 2</code></pre>

<p>it will assign 2 processors to the first and third fragment, and 4 processors to the second.</p>

<p>Obviously the <code>-np</code> parameter will have to be set accordingly.</p>

<p><a id="embedpot"></a></p>



<h2 id="exporting-the-embedding-potential">Exporting the embedding potential</h2>

<p>In order for <code>eQE</code> to compute the embedding potential of a single fragment explicitly, the keyword</p>



<pre class="prettyprint"><code class=" hljs fix"><span class="hljs-attribute"> fde_print_embedpot </span>=<span class="hljs-string"> .true. </span></code></pre>

<p>must be included in the <code>&amp;system</code> namelist of the fragment’s input file.</p>

<p>Because of parallelization complexity, currently the embedding potential can be printed only for a single fragment.</p>

<p>If the user specifies the keyword for more than a fragment, the code will only print the embedding potential of the fragment with the lowest id.</p>

<p>The embedding potential will be represented in the small electronic cell of the fragment and stored in a <code>.pp</code> file at the end of the calculation.</p>

<p>Afterwards, it is possible to export the embedding potential to several other formats using the standard <code>pp.x</code> program.</p>

<p><a id="atomgrid"></a></p>



<h3 id="atom-centered-grid">Atom centered grid</h3>

<p>The embedding potential can be interpolated on an arbitrarily defined grid (e.g. an atom centered grid generated from a molecular code).</p>

<p>To do so, the <code>pp.x</code> program that ships with the <code>eQE</code> distribution needs to be used.</p>

<p>A file containing the arbitrary grid named <code>grid.dat</code> needs to be in the same directory.</p>

<p>The format of the file is the following:</p>



<pre class="prettyprint"><code class=" hljs r">  npoints
x1   y1   z1   w1
x2   y2   z2   w2
<span class="hljs-keyword">...</span>
xn   yn   zn   wn</code></pre>

<p>where <code>x,y,z</code> are the cartesian coordinates in Bohr of the gridpoints and <code>w</code> is the weight of the point.</p>

<p>Create a modified <code>pp.x</code> input file based on this template and name it <code>interpolatepp.in</code>:</p>



<pre class="prettyprint"><code class=" hljs livecodeserver">&amp;inputpp
/
&amp;plot
   nfile = <span class="hljs-number">1</span>
   filepp(<span class="hljs-number">1</span>) = <span class="hljs-string">'filename_embedpot_0_alpha.pp'</span>
   iflag = <span class="hljs-number">3</span>                           ! <span class="hljs-number">3</span>D plot
   output_format = <span class="hljs-number">6</span>                   ! cube <span class="hljs-built_in">file</span>
   interpolation = <span class="hljs-string">'bspline_custom'</span>    ! interpolate <span class="hljs-command"><span class="hljs-keyword">on</span> <span class="hljs-title">the</span> <span class="hljs-title">points</span> <span class="hljs-title">specified</span> <span class="hljs-title">in</span> <span class="hljs-string">'grid.dat'</span>.</span>
   fileout = <span class="hljs-string">'filename_embedpot_0_alpha.customgrid'</span>           ! interpolated potential output <span class="hljs-built_in">file</span>
/</code></pre>

<p>Finally, execute:</p>



<pre class="prettyprint"><code class="language-bash hljs ">  /path/to/eQE/bin/pp.x &lt; interpolatepp.in </code></pre>
