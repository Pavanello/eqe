![logo here](logo.png) 
# [http://eqe.rutgers.edu](http://eqe.rutgers.edu)
# Embedded Quantum ESPRESSO -- `eQE`

eQE is a subsystem DFT generalization of Quantum ESPRESSO 

## `Reference`
*eQE: An open-source density functional embedding theory code for the condensed phase* <br />
[International Journal of Quantum Chemistry, **117**, e25401 (2017)](http://dx.doi.org/10.1002/qua.25401)

## `Installation`
Quick installation instructions for the impatient:
 - ./configure [options]
 - make pw

## `Nota Bene`
### Compilation tips
Options can be something like
```
F77=gfortran MPIF90=mpif90 --with-scalapack=no
```

Make sure that the code is compiled with appropriate BLAS, LAPACK and (most 
importantly) MPI libraries. Check the configure-generated make.sys file
to make sure the appropriate libraries are linked.

<!---
### SAOP xc functional needs LibXC
If you wish to use SAOP functional in eQE, you must install libxc and follow these instructions:

 - Build libxc from source
 - Add path to your libxc install to the IFLAG and LDLIBS variables in make.sys

```
PATHTOLIBXC= **changes depending on installation** 
IFLAGS         = -I../include -I$PATHTOLIBXC/include/
LD_LIBS        = -L$PATHTOLIBXC/lib -lxcf90 -lxc
```
--->
## `Manual`
More information, user manual, and keyword list available at [http://eqe.rutgers.edu](http://eqe.rutgers.edu)

## `Additional References`
 - [Subsystem DFT review with focus on eQE](http://iopscience.iop.org/0953-8984/27/18/183202)
 - [Nonlocal subsystem DFT](https://doi.org/10.1021/acs.jpclett.9b03281)
 - [Initial implementation](https://doi.org/10.1063/1.4897559)
 - [Implementation of k-point load balancing](https://doi.org/10.1088/0953-8984/27/49/495501)
 - [Implementation of forces](https://doi.org/10.1063/1.4953363)
 - [Implementation of real-time TDDFT](http://scitation.aip.org/content/aip/journal/jcp/142/15/10.1063/1.4918276)
 - [Application of real-time TDDFT to molecular systems](https://doi.org/10.1063/1.4944526)
 - [Application of real-time TDDFT to liquids](https://arxiv.org/abs/1706.03260)


## `Legal`
All the material included in this distribution is free software;
you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation;
either version 2 of the License, or (at your option) any later version.

These programs are distributed in the hope that they will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
675 Mass Ave, Cambridge, MA 02139, USA.


